import { Socket } from 'socket.io';
import { INextFn } from '../../../types/socket';
import { getUserRoom } from '../../utils/socketRooms';

export const auth = (socket: Socket, next: INextFn) => {
    console.log('socket:auth', socket.handshake.query.userId);
    if (socket.handshake.query.userId) {
        socket.join(getUserRoom(socket.handshake.query.userId as string))
    }

    next();
};
