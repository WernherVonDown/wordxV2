import { Server, Socket } from "socket.io";
import { io } from "../config/socket";

class SocketEmitter {
    private io: Server;

    constructor(io:Server) {
        this.io = io;
    }

    sendToUser(socketId: string, event: string, data?: any) {
        this.io.to(socketId).emit(event, data);
    }

    sendToRoomButUser(socket: Socket, roomId: string, event: string, data?: any) {
        socket.broadcast.to(roomId).emit(event, data);
    }

    sendToRoom(roomId: string, event: string, data?: any) {
        this.io.to(roomId).emit(event, data);
    }
}

export const emitter = new SocketEmitter(io);
