import { logger } from '../config/logger';
import { initSocketExchange } from './socket/consumer';

export const initConsumers = async () => {
    logger.info('broker:consumers:init:start');
    await initSocketExchange();
    logger.info('broker:consumers:init:complete');
};
