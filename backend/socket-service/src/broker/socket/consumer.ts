import { SOCKET_EXCHANGE } from '../../config/brokerExchanges';
import { createConsumer } from '../../utils/broker/createConsumer';
import * as controller from './controller';

export const initSocketExchange = () => Promise.all([
    createConsumer(
        {
            queueName: SOCKET_EXCHANGE.queues.SEND_NOTIFICATION.name,
            prefetch: 50,
        },
        controller.sendNotification,
    ),
]);
