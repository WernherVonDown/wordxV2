import { IBrokerHandler } from '../../../types/broker';
import { UserEvents } from '../../const/user/USER_EVENTS';
import { world } from '../../services/hello/world';
import { emitter } from '../../services/socketEmitter';
import { getUserRoom } from '../../utils/socketRooms';

export const sendNotification: IBrokerHandler = async ({ payload }) => {
    console.log("SEND NOTIFICATION", payload)
    const {
        userId,
        data,
    } = payload;
    if (userId) {
        emitter.sendToRoom(getUserRoom(userId), UserEvents.NOTIFICATION, data)
    }

    return {};
    // await world({ name: payload.name });
};
