import Transport from 'winston-transport';
import { sendMessage } from './index';

export class TelegramTransport extends Transport {
    botToken: string;

    chatId: string;

    constructor({ botToken, chatId }: { botToken: string; chatId: string }) {
        super();

        this.botToken = botToken;
        this.chatId = chatId;
    }

    log(info: any, next: () => void) {
        if (info.level === 'info') {
            next();
            return;
        }
        sendMessage({
            chatId: this.chatId,
            botToken: this.botToken,
            message: info.message,
        })
            .then(() => {
                next();
            })
            .catch((e) => {
                // eslint-disable-next-line no-console
                console.error(`!!! UNABLE SEND TELEGRAM ${info.level} MESSAGE !!!:\n${info.message}`);
                // eslint-disable-next-line no-console
                console.error(e);
                next();
            });
    }
}
