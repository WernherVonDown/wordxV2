import { sendHttpRequest } from '../../utils/http/sendHttpRequest';

const MAX_MESSAGE_LENGTH = 4096;

interface IArgs {
    chatId: string;
    botToken: string;
    message: string;
}

export const sendMessage = async ({ chatId, botToken, message }: IArgs) => {
    await sendHttpRequest({
        url: `https://api.telegram.org/bot${botToken}/sendMessage`,
        method: 'post',
        data: {
            chat_id: chatId,
            text: message.substring(0, MAX_MESSAGE_LENGTH),
        },
    });
};
