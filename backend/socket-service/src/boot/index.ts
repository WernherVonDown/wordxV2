import { logger } from '../config/logger';
import { startBroker } from './startBroker';
import { startSocket } from './startSocket';

export const runBootTasks = async () => {
    logger.info('BootTasks:running:start');
    await startBroker();
    await startSocket();
    logger.info('BootTasks:running:complete');
};
