import { getConnection } from '../utils/broker/getConnection';
import { initExchanges } from '../utils/broker/initExchanges';
import { logger } from '../config/logger';
import { initConsumers } from '../broker';

export const startBroker = async () => {
    logger.info('broker:running:start');
    const connection = await getConnection();
    await initExchanges({ connection });
    await initConsumers();
    logger.info('broker:running:complete');
};
