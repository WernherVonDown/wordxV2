import dotenv from 'dotenv';
import { parseBoolean } from '../utils/parsers/parseBoolean';
import { parseNumber } from '../utils/parsers/parseNumber';
import { parseString } from '../utils/parsers/parseString';

dotenv.config();

export const vars = Object.freeze({
    env: parseString(process.env.NODE_ENV, 'develop'),
    logs: process.env.NODE_ENV === 'production' ? 'combined' : 'dev',
    isLocal: parseBoolean(process.env.IS_LOCAL, false),
    port: parseNumber(process.env.PORT, 3000),
    telegram: {
        botToken: parseString(process.env.TELEGRAM_BOT_TOKEN, ''),
        chatId: parseString(process.env.TELEGRAM_CHAT_ID, ''),
    },
    rabbit: {
        user: parseString(process.env.RABBIT_USER, 'rabbitmq'),
        pass: parseString(process.env.RABBIT_PASS, 'rabbitmq'),
        host: parseString(process.env.RABBIT_HOST, 'localhost:5672'),
    },
});
