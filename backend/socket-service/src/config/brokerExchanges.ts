import { IExchange } from '../../types/broker';

export const SOCKET_EXCHANGE = {
    name: 'socket',
    type: 'direct',
    options: {
        durable: true,
    },
    queues: {
        SEND_NOTIFICATION: {
            name: 'socket.sendNotification',
            binding: 'socket.sendNotification',
            options: {
                durable: true,
            }
        },
    }
} as IExchange;
