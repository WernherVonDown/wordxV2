import winston from 'winston';

import { TelegramTransport } from '../externalServices/telegram/transport';
import { vars } from './vars';

const { telegram: { chatId, botToken } } = vars;

const transports = [];

if (chatId && botToken) {
    transports.push(new TelegramTransport({
        botToken,
        chatId,
    }));
}

const logger = winston.createLogger({
    level: 'info',
    format: winston.format.combine(
        winston.format.timestamp(),
        winston.format((info) => {
            // eslint-disable-next-line no-param-reassign
            info.message = `Log ${info.level} on core: \n${info.message}`;
            return info;
        })(),
    ),
    transports,
});

if (process.env.NODE_ENV !== 'production') {
    logger.add(new winston.transports.Console({
        format: winston.format.simple(),
    }));
}

export { logger };
