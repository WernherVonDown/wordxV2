import { Router } from "express";
import { languageMiddleware } from "../../middlewares/lanugage.middleware";
import { booksController } from "../../controllers/books.service";
import { authMiddleware } from "../../middlewares/auth.middleware";
import { cacheMiddleware } from "../../middlewares/cache.middleware";

const router = Router()

router.get('/:languageKey', authMiddleware, languageMiddleware, cacheMiddleware(), booksController.getBooks);
router.get('/:languageKey/:bookId', authMiddleware, languageMiddleware, booksController.getBookById);

export default router;