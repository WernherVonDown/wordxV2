import { Router } from "express";
import { sourceController } from "../../controllers/source.controller";
import { languageMiddleware } from "../../middlewares/lanugage.middleware";
import { memeController } from "../../controllers/meme.controller";

const router = Router()

router.get('/:languageKey', languageMiddleware, memeController.getRandomByLanguage);

export default router;