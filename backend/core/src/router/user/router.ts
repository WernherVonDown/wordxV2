import { Router } from "express";
import { body } from "express-validator";
import { paymentController } from "../../controllers/payment.controller";
import { userController } from '../../controllers/user.controller';
import { authMiddleware } from "../../middlewares/auth.middleware";
import { cacheMiddleware } from "../../middlewares/cache.middleware";

const router = Router();

router.post('/registration',
    body('email').isEmail(),
    body('password').isLength({ min: 3, max: 32 }),
    body('userName'),
    userController.registration
);
router.post('/googleAuth', userController.googleAuth);
router.post('/login', userController.login);
router.post('/logout', userController.logout);
router.post('/activate', userController.activate);
router.post('/changeSubscriptionPlan', authMiddleware, userController.changeSubscriptionPlan, paymentController.payment);
router.post('/changeLanguage', authMiddleware, userController.changeLanguage);
router.get('/refresh', userController.refresh);
router.get('/checkAuth', authMiddleware, userController.checkAuth);
router.get('/users', authMiddleware, userController.getUsers);
router.get('/paymentsMethods', authMiddleware, userController.getPaymentMethods)
router.post('/reset-password',
    body('email').isEmail(),
    userController.resetPassword
);
router.post('/reset-password-confirm',
    body('email').isEmail(),
    body('password').isLength({ min: 3, max: 32 }),
    userController.resetPasswordConfirm
);

router.post('/addWords', authMiddleware, userController.addWords)

export default router;