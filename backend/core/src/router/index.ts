import { Request, Router } from "express";
import userRoutes from './user/router';
import sourceRoutes from './source/router';
import wordsRoutes from "./words/router";
import dataRoutes from "./data/router";
import paymentRoutes from "./payment/router";
import notificationsRoutes from "./notifications/router";
import memeRoutes from "./meme/router";
import booksRoutes from "./books/router";

const router = Router();

router.use('/user', userRoutes);
router.use('/source', sourceRoutes);
router.use('/words', wordsRoutes);
router.use('/data', dataRoutes);
router.use('/meme', memeRoutes);
router.use('/books', booksRoutes);
router.use('/payment', paymentRoutes);
router.use('/notifications', notificationsRoutes);
router.get('/', (req, res) => res.json({hello: true}))

export default router;