import { Router } from "express";
import { dataController } from "../../controllers/data.controller";
import { notificationsController } from "../../controllers/notifications.controller";
import { authMiddleware } from "../../middlewares/auth.middleware";
import { languageMiddleware } from "../../middlewares/lanugage.middleware";

const router = Router();

router.get('/:languageKey', authMiddleware, languageMiddleware, notificationsController.getNotifications);
router.post('/:notificationId/read', authMiddleware, notificationsController.readNotification);

export default router;