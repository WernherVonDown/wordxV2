import { Router } from "express";
import { dataController } from "../../controllers/data.controller";
import { dataService } from "../../services/data.service";
import { cacheMiddleware } from "../../middlewares/cache.middleware";
import { HOUR, MIN } from "../../utils/convertors/convertedMilliseconds";

const router = Router();

router.get('/subscriptionPlans', cacheMiddleware(), dataController.getSubscriptionPlans);
router.get('/languages', cacheMiddleware(), dataController.getLanguages);
router.get('/languageLevels', cacheMiddleware(), dataController.getLanguageLevels);
router.get('/products', cacheMiddleware(), dataController.getProducts);

export default router;