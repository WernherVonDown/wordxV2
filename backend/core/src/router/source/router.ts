import { Router } from "express";
import { sourceController } from "../../controllers/source.controller";
import multer from "multer";
import { getRandStr } from "../../utils/getRandStr";
import { authMiddleware } from "../../middlewares/auth.middleware";
import { languageMiddleware } from "../../middlewares/lanugage.middleware";
import { sourceMiddleware } from "../../middlewares/source.middleware";
import { wrapWalletMiddleware } from "../../middlewares/wallet.middleware";
import { cacheMiddleware } from "../../middlewares/cache.middleware";

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, '/tmp/')
    },
    filename: (req, file, cb) => {
        console.log("FIle", file)
        cb(null, getRandStr() + '_' + file.originalname)
    }
})

const upload = multer({ storage })

const router = Router()

router.post('/create/:languageKey', authMiddleware, languageMiddleware, upload.fields([{name: 'file', maxCount: 1}, {name: 'poster', maxCount: 1}]), sourceController.create);
router.get('/list/:languageKey', authMiddleware, languageMiddleware, cacheMiddleware("locals.language.key"), sourceController.getSources);
router.get('/words/:sourceId', authMiddleware, sourceMiddleware,sourceController.getWords);
router.get('/statistics/:sourceId', authMiddleware, sourceMiddleware, ...wrapWalletMiddleware(sourceController.getStatistics));
router.post('/subtitles/:languageKey', authMiddleware, languageMiddleware, ...wrapWalletMiddleware(sourceController.getSubtitles));
router.post('/book/:languageKey', authMiddleware, languageMiddleware, ...wrapWalletMiddleware(sourceController.getBookText));
router.get('/music/:songId', authMiddleware, sourceController.searchSongById);
router.get('/music/:songId/lyrics', authMiddleware, sourceController.searchMusicLyricsById);
router.get('/music/:languageKey', authMiddleware, languageMiddleware, sourceController.getMusic);
router.get('/music', authMiddleware, sourceController.searchMusic);


export default router;