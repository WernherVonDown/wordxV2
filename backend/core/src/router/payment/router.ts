import { Router } from "express";
import { paymentController } from "../../controllers/payment.controller";
import { authMiddleware } from "../../middlewares/auth.middleware";

const router = Router();

router.post('/status', paymentController.paymentStatus);
router.post('/', authMiddleware, paymentController.payment);

export default router;