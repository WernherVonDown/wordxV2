import { Router } from "express";
import { wordsController } from "../../controllers/words.controller";
import { authMiddleware } from "../../middlewares/auth.middleware";
import { languageMiddleware } from "../../middlewares/lanugage.middleware";

const router = Router()

router.get('/:languageKey', authMiddleware, languageMiddleware, wordsController.getWords)
router.post('/learnedWords', authMiddleware, wordsController.learnedWords)
router.get('/sources/:languageKey', authMiddleware, languageMiddleware, wordsController.getWordsSources)

export default router;