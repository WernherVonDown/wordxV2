import { addLanguageLevels } from "./addLanguageLevels";
import { addLanguages } from "./addLanguages";
import { addProducts } from "./addProducts";
import { addSourceTypes } from "./addSourceTypes";
import { addSubscriptionPlans } from "./addSubscriptionPlans";

const migrationKeys = {
    languages: 'languages',
    sourceTypes: 'sourceTypes',
    languageLevels: 'languageLevels',
    subscriptionPlans: 'subscriptionPlans',
    products: 'products',
}

export const migrationRegistry = {
    [migrationKeys.languages]: addLanguages,
    [migrationKeys.sourceTypes]: addSourceTypes,
    [migrationKeys.languageLevels]: addLanguageLevels,
    [migrationKeys.subscriptionPlans]: addSubscriptionPlans,
    [migrationKeys.products]: addProducts,
}

export const migrationList = Object.values(migrationKeys);