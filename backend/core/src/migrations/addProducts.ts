import { Products } from "../const/product/Products";
import PoductModel from "../models/Product.model";

export const addProducts = async () => {
    try {
        await Promise.all(Products.map(p => PoductModel.findOneAndUpdate({ key: p.key }, p, { upsert: true })))
    } catch (error) {
        console.log('migrationError addProducts', error);
    }

}
