import LanguageModel from "../models/Language.model"

export const addLanguages = async () => {
    try {
        const languages = [
            {
                label: 'Английский',
                key: 'english',
                icon: '',
            },
            {
                label: 'Немецкий',
                key: 'german',
                icon: '',
            },
            {
                label: 'Французский',
                key: 'french',
                icon: '',
            },
        ]

        await Promise.all(languages.map(language => LanguageModel.findOneAndUpdate({ key: language.key }, language, { upsert: true })))

        // await LanguageModel.insertMany(languages)
    } catch (error) {
        console.log('migrationError addLanguages', error);
    }

}