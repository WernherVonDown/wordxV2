import SourceTypeModel from "../models/SourceType.model";



export const addSourceTypes = async () => {
    try {
        const sources = [
            {
                label: 'Книги',
                key: 'books',
            },
            {
                label: 'Сериалы',
                key: 'series',
            },
            {
                label: 'Фильмы',
                key: 'movies',
            },
            {
                label: 'Подборки слов',
                key: 'word_lists',
            },
        ]

        await SourceTypeModel.insertMany(sources)
    } catch (error) {
        console.log('migrationError addLanguages', error);
    }

}