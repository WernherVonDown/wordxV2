import _ from "lodash";
import { sourceService } from "../services/source.service"
import { publishParseFile } from "../broker/parser/publisher";

export const computeSources = async () => {
    const sources = await sourceService.getSourcesForComputing();
    const sourcesWithFile = _.remove(sources, "sourceFile.localUrl")
    console.log("computeSources", sourcesWithFile.length, sourcesWithFile.map(s => s.title || s.titleOriginal))
    await Promise.all(sourcesWithFile.map((s) => publishParseFile({
        filePath: _.get(s, "sourceFile.localUrl"),
        languageKey: _.get(s, "language.key"),
        sourceId: s._id,
        languageId: _.get(s, "language._id"),
    })))
} 