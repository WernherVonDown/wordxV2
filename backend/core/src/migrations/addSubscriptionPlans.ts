import SubscriptionPlanModel from "../models/SubscriptionPlan.model"

const subscriptionPlans = [
    {
        level: 0,
        price: 0,
        currency: undefined,
        title: 'Базовая подписка',
        description: undefined,
        benifits: ['Доступ ко всем функциям', 'Один язык для изучения'],
        discount: 0,
        discontEndDate: undefined,
        maxLangs: 1,
        bonusCoef: 1,
    },
    {
        level: 1,
        price: 10,
        currency: undefined,
        title: 'Подписка+',
        description: undefined,
        benifits: ['Доступ ко всем функциям', 'Два языка для изучения', 'Удвоенные бонусы'],
        discount: 0,
        discontEndDate: undefined,
        maxLangs: 2,
        bonusCoef: 2,
    },
    {
        level: 2,
        price: 15,
        currency: undefined,
        title: 'Подписка про',
        description: undefined,
        benifits: ['Доступ ко всем функциям', 'Сколько угодо языков для изучения', 'Удвоенные бонусы', 'Повышенный приоритет'],
        discount: 0,
        discontEndDate: undefined,
        maxLangs: 10,
        bonusCoef: 2,
    },
]


export const addSubscriptionPlans = async () => {
    await SubscriptionPlanModel.insertMany(subscriptionPlans);
}
