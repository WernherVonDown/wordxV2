import LanguageLevelModel from "../models/LanguageLevel.model";

export const addLanguageLevels = async () => {
    try {
        const languageLevels = [
            {
                label: 'A1',
                key: 'a1',
                description: 'Начальный',
                levelCount: 0,
            },
            {
                label: 'A2',
                key: 'a2',
                description: 'Элементарный',
                levelCount: 1,
            },
            {
                label: 'B1',
                key: 'b1',
                description: 'средний',
                levelCount: 2,
            },
            {
                label: 'B2',
                key: 'b2',
                description: 'средне-продвинутый',
                levelCount: 3,
            },
            {
                label: 'C1',
                key: 'c1',
                description: 'продвинутый',
                levelCount: 4,
            },
            {
                label: 'C2',
                key: 'c2',
                description: 'в совершенстве',
                levelCount: 5,
            }
        ]

        await LanguageLevelModel.insertMany(languageLevels)
    } catch (error) {
        console.log('migrationError addLanguages', error);
    }

}