import { Schema, model } from 'mongoose';
import { SourceDtoFormats } from '../const/source/SourceDtoFormats';
import { SourceStatuses } from '../const/source/SourceStatuses';
import { toDtoIfHas } from '../utils/mongo/toDtoIfHas';

const Source = new Schema({
    title: {
        type: String,
    },
    titleOriginal: {
        type: String,
    },
    posterUrl: {
        type: String,
    },
    description: {
        type: String,
    },
    poster: {
        type: Schema.Types.ObjectId,
        ref: 'File'
    },
    sourceFile: {
        type: Schema.Types.ObjectId,
        ref: 'File'
    },
    language: {
        type: Schema.Types.ObjectId,
        ref: 'Language'
    },
    type: {
        type: String,
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    },
    words: [{
        word: {
            type: Schema.Types.ObjectId,
            ref: 'Word'
        },
        count: {
            type: Number,
        },
        percent: {
            type: Number,
        },
        percentStat: {
            type: String,
            default: '',
        },
    }],
    level: {
        type: Schema.Types.ObjectId,
        ref: 'LanguageLeve',
    },
    choosenCount: {
        type: Number,
        default: 0
    },
    totalWordsCount: {
        type: Number,
        default: 0,
    },
    imdbId: {
        type: String,
    },
    kinopoiskId: {
        type: Number,
    },
    status: {
        type: String,
        default: SourceStatuses.deafult,
    },
    bookId: {
        type: String,
    },
    songId: {
        type: String,
    },
    author: {
        type: String,
    },
    youtubeId: {
        type: String,
    },
    computeErrors: [{
        type: String
    }],
    uniqueWordsCount: {
        type: Number,
        default: 0,
    } //TODO Add links
}, {
    versionKey: false,
    timestamps: true,
    autoCreate: true,
})

Source.methods.toDto = function toDto(format?: SourceDtoFormats) {
    if (format === SourceDtoFormats.short) {
        return {
            id: this._id,
            title: this.title,
            titleOriginal: this.titleOriginal,
            poster: toDtoIfHas(this.poster),
            language: toDtoIfHas(this.language),
            type: this.type,
            level: toDtoIfHas(this.level),
            totalWordsCount: this.totalWordsCount,
            uniqueWordsCount: this.uniqueWordsCount,
            imdbId: this.imdbId,
            kinopoiskId: this.kinopoiskId,
            posterUrl: this.posterUrl,
            status: this.status,
            bookId: this.bookId,
            songId: this.songId,
            author: this.author,
            youtubeId: this.youtubeId,
        }
    }
    return {
        id: this._id,
        title: this.title,
        titleOriginal: this.titleOriginal,
        poster: toDtoIfHas(this.poster),
        language: toDtoIfHas(this.language),
        type: this.type,
        user: toDtoIfHas(this.user),
        words: toDtoIfHas(this.words),
        level: toDtoIfHas(this.level),
        totalWordsCount: this.totalWordsCount,
        uniqueWordsCount: this.uniqueWordsCount,
        imdbId: this.imdbId,
        kinopoiskId: this.kinopoiskId,
        posterUrl: this.posterUrl,
        status: this.status,
        bookId: this.bookId,
        songId: this.songId,
        author: this.author,
        youtubeId: this.youtubeId,
    }
}

export default model('Source', Source);
