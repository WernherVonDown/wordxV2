import { Schema, model } from 'mongoose';

const Language = new Schema({
    label: {
        type: String,
    },
    key: {
        type: String,
    },
    icon: {
        type: String,
    },
}, {
    versionKey: false,
    timestamps: true,
    autoCreate: true,
})

Language.methods.toDto = function toDto() {
    return {
        label: this.label,
        id: this._id,
        key: this.key,
    }
}


export default model('Language', Language);

