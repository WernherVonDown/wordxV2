import { Schema, model } from 'mongoose';

const WordsListItem = new Schema({
    word: {
        type: Schema.Types.ObjectId,
        ref: 'Word'
    },
    count: {
        type: Number,
    },
    percent: {
        type: Number,
    },
})

export default model('WordsListItem', WordsListItem);