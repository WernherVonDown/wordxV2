import { Schema, model } from 'mongoose';

const LanguageLevel = new Schema({
    label: {
        type: String,
    },
    key: {
        type: String,
    },
    description: {
        type: String,
    },
    levelCount: {
        type: Number,
    }
}, {
    versionKey: false,
    timestamps: true,
    autoCreate: true,
})

LanguageLevel.methods.toDto = function toDto() {
    return {
        label: this.label,
        id: this._id,
        key: this.isActivated,
        description: this.description,
        levelCount: this.levelCount,
    }
}


export default model('LanguageLevel', LanguageLevel);

