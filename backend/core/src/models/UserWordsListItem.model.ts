import { Schema, model } from 'mongoose';

const UserWordsListItem = new Schema({
    word: {
        type: Schema.Types.ObjectId,
        ref: 'Word'
    },
    count: {
        type: Number,
    },
    // percent: {
    //     type: Number,
    // },
    sources: [{
        type: Schema.Types.ObjectId,
        ref: 'Source'
    }]
}, {
    versionKey: false,
    timestamps: true,
    autoCreate: true,
})

export default model('UserWordsListItem', UserWordsListItem);