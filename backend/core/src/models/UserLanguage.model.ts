import { Schema, model } from 'mongoose';
import { toDtoIfHas } from '../utils/mongo/toDtoIfHas';

const UserLanguage = new Schema({
    language: {
        type: Schema.Types.ObjectId,
        ref: 'Language'
    },
    level: {
        type: Schema.Types.ObjectId,
        ref: 'LanguageLevel'
    }
}, {
    versionKey: false,
    timestamps: true,
    autoCreate: true,
})

UserLanguage.methods.toDto = function toDto() {
    return {
        language: this.language,
        id: this._id,
        level: this.level,
    }
}


export default model('UserLanguage', UserLanguage);