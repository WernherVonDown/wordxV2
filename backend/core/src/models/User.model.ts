import { Schema, model, Document } from 'mongoose';
import { toDtoIfHas } from '../utils/mongo/toDtoIfHas';
import UserLanguageModel from './UserLanguage.model';

const User = new Schema({
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        unique: false,
    },
    isActivated: {
        type: Boolean,
        default: false
    },
    activationCode: {
        type: String,
    },
    resetPasswordLink: {
        type: String
    },
    googleAccessToken: {
        type: String
    },
    userName: {
        type: String,
        unique: true,
        required: true
    },
    languages: [
        {
            type: Schema.Types.ObjectId,
            ref: "UserLanguage"
        }
    ],
    wordSources: [
        {
            type: Schema.Types.ObjectId,
            ref: "UserWordSource"
        }
    ],
    knownWords: [{
        word: {
            type: Schema.Types.ObjectId,
            ref: 'Word'
        },
        count: {
            type: Number,
        },
        sources: [{
            type: Schema.Types.ObjectId,
            ref: 'Source'
        }],
        countBySource: {
            type: Schema.Types.Mixed,
            default: {},
        }
    }],
    sources: [{
        type: Schema.Types.ObjectId,
        ref: 'Source'
    }],
    notSourtedSources: [{
        type: Schema.Types.ObjectId,
        ref: 'Source'
    }],
    learningWords: [{
        word: {
            type: Schema.Types.ObjectId,
            ref: 'Word'
        },
        count: {
            type: Number,
        },
        sources: [{
            type: Schema.Types.ObjectId,
            ref: 'Source'
        }],
        countBySource: {
            type: Schema.Types.Mixed,
            default: {},
        }
    }],
    learnedWords: [{
        word: {
            type: Schema.Types.ObjectId,
            ref: 'Word'
        },
        count: {
            type: Number,
        },
        sources: [{
            type: Schema.Types.ObjectId,
            ref: 'Source'
        }],
        countBySource: {
            type: Schema.Types.Mixed,
            default: {},
        }
    }],
    notSortedWords: [{
        word: {
            type: Schema.Types.ObjectId,
            ref: 'Word'
        },
        count: {
            type: Number,
        },
        sources: [{
            type: Schema.Types.ObjectId,
            ref: 'Source'
        }],
        countBySource: {
            type: Schema.Types.Mixed,
            default: {},
        }
    }],
    subscriptionPlan: {
        type: Schema.Types.ObjectId,
        ref: 'SubscriptionPlan',
    },
    image: {
        type: String, default: '' //TODO ADD DEFAULT,
    },
    notifications: [{
        type: Schema.Types.ObjectId,
        ref: "Notification",
    }],
    wallet: {
        type: Number,
        default: 1000,
    },
}, {
    versionKey: false,
    timestamps: true,
    autoCreate: true,
});

export interface IUserDto {
    email: string;
    id: string;
    isActivated: boolean;
}

export interface IUser extends Document {
    email: string;
    id: string;
    isActivated: boolean;
    subscriptionPlan: string;
}

User.methods.toDto = async function toDto() {
    const languages = this.languages.map((l: any) => l?.toDto ? l.toDto() : l)//await toDtoIfHas(this.languages);
    const notifications = await Promise.all(this.notifications.map((l: any) => l?.toDto ? l.toDto() : l))

    return {
        email: this.email,
        id: this._id,
        isActivated: this.isActivated,
        languages,
        subscriptionPlan: toDtoIfHas(this.subscriptionPlan),
        sources: toDtoIfHas(this.sources),
        notSourtedSources: toDtoIfHas(this.notSourtedSources) || [],
        notifications: notifications.reverse(),
        wallet: this.wallet,
    }
}

export default model('User', User);