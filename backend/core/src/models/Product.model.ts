import { model, Schema } from "mongoose";

const Product = new Schema({
    title: {
        type: String,
        required: true,
    },
    key: {
        type: String,
        required: true,
    },
    price: {
        type: Number,
        required: true,
    },
}, {
    versionKey: false,
    timestamps: true,
    autoCreate: true,
});

Product.methods.toDto = function toDto() {
    return {
        id: this._id,
        title: this.title,
        key: this.key,
        price: this.price,
    }
}

export default model('Product', Product);
