import { Schema, model } from 'mongoose';

const Notification = new Schema({
    text: {
        type: String,
    },
    langKey: {
        type: String,
    }
}, {
    versionKey: false,
    timestamps: true,
    autoCreate: true,
});

Notification.methods.toDto = function toDto() {
    return {
        id: this._id,
        text: this.text,
        langKey: this.langKey,
    }
}

export default model('Notification', Notification);
