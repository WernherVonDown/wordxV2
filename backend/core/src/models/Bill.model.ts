import { model, Schema, Document } from "mongoose";
import { PaymentTypes } from "../const/payment/PaymentTypes";


const Bill = new Schema({
    user: {
        ref: 'User',
        type: Schema.Types.ObjectId,
    },
    subscriptionPlan: {
        type: Schema.Types.ObjectId,
        ref: 'SubscriptionPlan',
    },
    paidAt: {
        type: String,
    },
    amount: {
        type: Number,
    },
    paymentType: {
        type: String,
    },
    paid: {
        type: Boolean,
        default: false,
    },
    rebill: {
        type: Boolean,
        default: false,
    },
    paymentId: {
        type: String,
    },
    description: {
        type: String,
    },
    paymentMethodId: {
        type: String,
    },
    paymentDetails: {
        type: Schema.Types.Mixed,
    },
}, {
    versionKey: false,
    timestamps: true,
    autoCreate: true,
});

export default model('Bill', Bill);

export interface IBill extends Document {
    user: string;
    subscriptionPlan?: string;
    paidAt: string;
    amount: number;
    paymentType: PaymentTypes;
    paid: boolean;
    rebill?: boolean
    paymentId?: string;
    description?: string;
    paymentMethodId?: string;
    paymentDetails?: any;
}
