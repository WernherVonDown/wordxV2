import { Schema, model } from 'mongoose';

const WordContexts = new Schema({
    word: {
        type: Schema.Types.ObjectId,
        ref: 'Word',
    },
    contexts: [
        {type: String}
    ]
})

export default model('WordContexts', WordContexts);