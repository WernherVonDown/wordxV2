import { Schema, model } from 'mongoose';

const SourceType = new Schema({
    label: {
        type: String,
    },
    key: {
        type: String,
    },
})

SourceType.methods.toDto = function toDto() {
    return {
        id: this._id,
        label: this.label,
        key: this.key,
    }
}

export default model('SourceType', SourceType);
