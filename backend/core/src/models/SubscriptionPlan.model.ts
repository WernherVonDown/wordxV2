import { Schema, model } from 'mongoose';

const SubscriptionPlan = new Schema({
    level: {
        type: Number,
        required: true,
    },
    price: {
        type: Number,
        required: true,
    },
    currency: {
        type: String,
        default: 'RUB', //RUB
    },
    title: {
        type: String,
        equired: true,
    },
    description: {
        type: String,
    },
    benifits: [
        {
            type: String
        }
    ],
    discount: {
        type: Number,
    },
    discontEndDate: {
        type: String,
    },
    maxLangs: {
        type: Number,
    },
    bonusCoef: {
        type: Number,
    },
})

SubscriptionPlan.methods.toDto = function () {
    return {
        id: this._id,
        level: this.level,
        price: this.price,
        currency: this.currency,
        title: this.title,
        description: this.description,
        benifits: this.benifits,
        discount: this.discount,
        discontEndDate: this.discontEndDate,
        maxLangs: this.maxLangs,
        bonusCoef: this.bonusCoef,
    }
}

export default model('SubscriptionPlan', SubscriptionPlan);