import { Schema, model } from 'mongoose';

const Word = new Schema({
    text: {
        type: String,
    },
    translation: {
        type: String,
    },
    language: {
        type: Schema.Types.ObjectId,
        ref: 'Language'
    },
    globalCount: {
        type: Number,
        default: 0,
    },
    isRoot: {
        type: Boolean,
        default: false,
    },
    root: {
        type: Schema.Types.ObjectId,
    }
}, {
    versionKey: false,
    timestamps: true,
    autoCreate: true,
})

Word.methods.toDto = function toDto() {
    return {
        id: this._id,
        text: this.text,
        translation: this.translation,
    }
}

export default model('Word', Word);