import { Schema, model } from 'mongoose';
import { toDtoIfHas } from '../utils/mongo/toDtoIfHas';

const UserWordSource = new Schema({
    source: {
        type: Schema.Types.ObjectId,
        ref: 'Source'
    },
    knownCount: {
        type: Number,
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
}, {
    versionKey: false,
    timestamps: true,
    autoCreate: true,
})

UserWordSource.methods.toDto = async function toDto() {
    return {
        id: this._id,
        source: toDtoIfHas(this.source),
        knownCount: this.knownCount,
    }
}

export default model('UserWordSource', UserWordSource);