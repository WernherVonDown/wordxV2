import { model, Schema } from "mongoose";

const File = new Schema({
    fileName: {
        type: String,
    },
    extension: {
        type: String,
    },
    url: {
        type: String,
    },
    mimeType: {
        type: String,
    },
    uploader: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    },
    localUrl: {
        type: String,
    },
}, {
    versionKey: false,
    timestamps: true,
    autoCreate: true,
});

File.methods.toDto = function toDto() {
    return {
        id: this.id,
        url: this.url,
        localUrl: this.localUrl,
    }
}

export default model('File', File);