import { Document, model, Schema, LeanDocument } from "mongoose";
import { IScheduledTask } from "../const/scheduler/types";
import { scheduler } from "../scheduler/scheduler";
import minimist from "minimist";
import {sendTaskToMaster} from "../utils/sendTaskToMaster";

const ScheduledTask = new Schema({
    type: {
        type: String,
        required: true,
    },
    executedAt: {
        type: Date,
        required: true,
    },
    isActive: {
        type: Boolean,
        index: true,
        default: true,
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    },
    data: {
        type: Schema.Types.Mixed,
        default: {},
    },
}, {
    versionKey: false,
    timestamps: true,
    autoCreate: true,
});

export type IScheduledTaskDocument = IScheduledTask & Document;


const argv = minimist(process.argv.slice(2), {});

async function afterSaveScheduledTask(task: any, isNewTask?: boolean) {
    try {
        if (!task) {
            return;
        }

        if (!argv.isMaster) {
            await sendTaskToMaster(task, isNewTask)
        } else {
            if (isNewTask) {
                scheduler.planTask(task);
            } else {
                scheduler.rescheduleTask(task);
            }
        }
    } catch (e) {
        console.log('afterSaveScheduledTask', e)
    }
}

ScheduledTask.post("save", (task: IScheduledTaskDocument) => {
    console.log("SAAAVE", task.toObject())
    afterSaveScheduledTask(task.toObject(), true)
});

ScheduledTask.post("remove", (task: IScheduledTaskDocument) => {
    task.isActive = false;
    // afterSaveScheduledTask(task.toObject());
});

export default model("ScheduledTask", ScheduledTask);
