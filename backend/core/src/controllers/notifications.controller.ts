import { NextFunction, Request, Response } from "express";
import { userService } from "../services/user.service";

class NotificationsController {
    async getNotifications(req: Request, res: Response, next: NextFunction) {
        try {
            const { user, language } = res.locals;

            res.json([])
        } catch (error) {
            console.log('NotificationsController.getNotifications error;', error);
            next(error);
        }
    }

    async readNotification (req: Request, res: Response, next: NextFunction) {
        try {
            const { user } = res.locals;
            const { notificationId } = req.params;
            await userService.removeNotification(user.id, notificationId);
            res.sendStatus(200);
        } catch (error) {
            console.log('NotificationsController.readNotification error;', error);
            next(error);
        }
    }
}

export const notificationsController = new NotificationsController()
