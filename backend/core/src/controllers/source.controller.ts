import { NextFunction, Request, Response } from "express";
import { IMulaterFiles, IMulterFile } from "../const/file/interfaces";
import { filesService } from "../services/files.service";
import { sourceService } from "../services/source.service";
import { userService } from "../services/user.service";
import { wordsService } from "../services/words.service";
import { yandexS3Service } from "../externalServices/yandexS3.service";
import { getRandStr } from "../utils/getRandStr";
import { toDtoIfHas } from "../utils/mongo/toDtoIfHas";
import languageEncoding from "detect-file-encoding-and-language";
import fs from 'fs';
import { openSubtitles } from "../externalServices/openSubtitles.service";
import { ApiError } from "../utils/errors/ApiError";
import { publishParseFile } from "../broker/parser/publisher";
import { SourceStatuses } from "../const/source/SourceStatuses";
import { notificationsService } from "../services/notificatons.service";
import { publishSendNotification } from "../broker/socket/publisher";
import { GutenbergBooksService } from "../externalServices/gutenbergBooks.service";
import { sendHttpRequest } from "../utils/http/sendHttpRequest";
import { vars } from "../config/vars";
import path from "path";
import { getMovieUrlIfExists, saveMovieOnS3 } from "../utils/sourcesCache";
import { ISourceWord } from "../const/source/interfaces";
import redisCache from "../utils/redisCache";
import { GeniusService } from "../externalServices/genius.service";

class SourceController {
    async create(req: Request, res: Response, next: NextFunction) {
        try {
            const { user, language } = res.locals;
            const {
                title,
                imdbId,
                kinopoiskId,
                titleOriginal,
                posterUrl,
                bookId,
                type,
                songId,
                author,
                youtubeId,
            } = req.body;
            let files = req.files as unknown as { file: IMulterFile[], poster: IMulterFile[] };
            const bucketPath = `/sources/${language.key}/` + title + '_' + getRandStr()
            // console.log("reee", file[0], poster[0], req.files)
            const detectLanguge = await languageEncoding(files.file[0].path as any);
            console.log("Detect lang", detectLanguge)
            if (detectLanguge.language && detectLanguge.language !== language.key && (detectLanguge.confidence.language || 0) > 0.6) {
                throw ApiError.BadRequest(`Неверный язык`)
            }
            const uploadedFiles = await sourceService.uploadSourceS3(files, bucketPath);

            const { file, poster } = uploadedFiles;
            console.log('FILE', file[0].path)
           
            const textFile = await filesService.create({ fileName: title, uploader: user._id, url: file[0].s3Path || '', mimeType: file[0].mimetype, localUrl: file[0].path })
            let posterFile = null;
            if (poster && poster.length) {
                posterFile = await filesService.create({ fileName: poster[0].filename, uploader: user._id, url: poster[0].s3Path || '', mimeType: poster[0].mimetype })
            }

            

            const source = await sourceService.create(
                {
                    title,
                    poster: posterFile?._id,
                    sourceFile: textFile?.id,
                    language: language.id,
                    type, 
                    user: user?.id,
                    imdbId,
                    kinopoiskId,
                    bookId,
                    titleOriginal,
                    songId,
                    posterUrl: posterFile?.url || posterUrl,
                    status: SourceStatuses.waiting,
                    author,
                    youtubeId,
                })

            const notification = await notificationsService.create(
                `Субтитры "${title || titleOriginal}" ожидают обработки`,
                language.key
            );

            await userService.addNotification(user.id, notification._id);
            publishSendNotification({ userId: source.user, data: notification })
            await publishParseFile({
                filePath: file[0].path,
                languageKey: language.key,
                sourceId: source._id,
                languageId: language.id,
            })


            return res.json({});

            // const { words, uniqueCount, allCount, level } = await wordsService.analysSrtFile(file[0].path, language.id, language.key)

            // fs.unlink(file[0].path, (err) => {if(err) console.log('error remove file', file[0].path)});


            // return res.json({});
        } catch (error) {
            console.log('SourceController.create error;', error);
            next(error);
        }
    }

    async getSources(req: Request, res: Response, next: NextFunction) {
        try {
            const { user, language } = res.locals;
            const {
                query: {
                    page = 0
                }
            } = req;

            const sources = await sourceService.getByLangShort(language.id, Number(page));
            console.log("RES SOURCES", sources.length)
            res.json({ sources });
            await redisCache.add(req.originalUrl, { sources }, undefined, language.key);
        } catch (error) {
            console.log('SourceController.create error;', error);
            next(error);
        }
    }

    async getWords(req: Request, res: Response, next: NextFunction) {
        try {
            const { user } = res.locals;
            const { sourceId } = req.params;
            const words = await sourceService.getWordsIds(sourceId);
            const wordsForSorting = await userService.getWordsForSorting(user.id, words, sourceId);
            const SortSourceWordsWithData = await Promise.all(wordsForSorting.map(async w => {
                const wordId = w.word as string;
                const word = await wordsService.findById(wordId);
                const wordDto = toDtoIfHas(word)
                return { word: wordDto, count: w.count, percent: w.percent, percentStat: w.percentStat };
            }));
            res.json({ words: SortSourceWordsWithData });
        } catch (error) {
            console.log('SourceController.getWords error;', error);
            next(error);
        }
    }

    async getStatistics(req: Request, res: Response, next: NextFunction) {
        try {
            const { user } = res.locals;
            const { sourceId } = req.params;
            const words = await sourceService.getWordsIds(sourceId);
            const data = await userService.getWordsStatistics(user.id, words);
            res.json(data)
            next()
        } catch (error) {
            console.log('SourceController.getWords error;', error);
            next(error);
        }
    }

    async getSubtitles(req: Request, res: Response, next: NextFunction) {
        try {
            const { user, language } = res.locals;
            const { imdbId } = req.body;

            const movieUrl = await getMovieUrlIfExists(imdbId, language.key);

            if (movieUrl) {
                console.log("FOUND!!", movieUrl)
                res.json({ subtitleLink: movieUrl });
                return next()
            }

            const fileId = await openSubtitles.search(imdbId, language.key);
            if (!fileId) {
                throw ApiError.BadRequest('Субтитры не найдны');
            }
            const subtitleLink = await openSubtitles.download(fileId);
            
            const dir = path.join(vars.filePath, 'movies', language.key);
            if (!fs.existsSync(dir)){
                fs.mkdirSync(dir, { recursive: true });
            }
            const fileData = await sendHttpRequest({
                url: subtitleLink,
                method: 'GET',
                data: {}
            })
            console.log("EEE", subtitleLink)
            const data = fileData.data;

            const filePath = `${dir}/${imdbId}.srt`
            await fs.promises.writeFile(filePath, data);
            const uploadedFile = await saveMovieOnS3([filePath], imdbId, language.key);

            res.json({ subtitleLink: uploadedFile[0]?.Location });
            next()
        } catch (error) {
            console.log('SourceController.getSubtitles error;', error);
            next(error);
        }
    }
    
    async getBookText(req: Request, res: Response, next: NextFunction) {
        try {
            const { user, language } = res.locals;
            const { bookId } = req.body;
            const bookLink = await GutenbergBooksService.getBookUrlById(language.key, bookId)
            res.json({ bookLink });
            next()
        } catch (error) {
            console.log('SourceController.create error;', error);
            next(error);
        }
    }

    async getMusic(req: Request, res: Response, next: NextFunction) {
        try {
            const { user, language } = res.locals;
            
            res.json({  });
            next()
        } catch (error) {
            console.log('SourceController.create error;', error);
            next(error);
        }
    }

    async searchMusic(req: Request, res: Response, next: NextFunction) {
        try {
            const { q } = req.query;
            const { user, language } = res.locals;
            const songs = await GeniusService.searchSong(q as string)
            res.json(songs);
            next()
        } catch (error) {
            console.log('SourceController.create error;', error);
            next(error);
        }
    }

    async searchMusicLyricsById(req: Request, res: Response, next: NextFunction) {
        try {
            const { songId } = req.params;
            console.log("searchMusicLyricsById", songId)
            const { user, language } = res.locals;
            const text = await GeniusService.getLyrics(+songId as number)
            res.json(text);
            next()
        } catch (error) {
            console.log('SourceController.create error;', error);
            next(error);
        }
    }

    async searchSongById(req: Request, res: Response, next: NextFunction) {
        try {
            const { songId } = req.params;
            console.log("searchMusicLyricsById", songId)
            const song = await GeniusService.getSong(+songId as number)
            res.json(song);
            next()
        } catch (error) {
            console.log('SourceController.create error;', error);
            next(error);
        }
    }
}

export const sourceController = new SourceController()