import { NextFunction, Request, Response } from "express";
import { validationResult } from "express-validator";
import { vars } from "../config/vars";
import { billService } from "../services/bill.service";
import { sourceService } from "../services/source.service";
import { userService } from '../services/user.service';
import { DAY, MIN, SEC } from "../utils/convertors/convertedMilliseconds";
import { ApiError } from "../utils/errors/ApiError";
import _ from "lodash"
import { wordsService } from "../services/words.service";
import { toDtoIfHas } from "../utils/mongo/toDtoIfHas";
import redisCache from "../utils/redisCache";
import { Worker } from 'worker_threads';
import path from "path";

const REFRESH_TOKEN_MAX_AGE = DAY * 30;

const REMOVE_USER_TIMERS: any = {}

const ACTIVATION_CODE_TIMEOUT = 1.5 * MIN;

class UserController {
    async registration(req: Request, res: Response, next: NextFunction) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return next(ApiError.BadRequest('Validation error', errors.array()))
            }
            const { email, password, userName, languages } = req.body;
            const {user} = await userService.registration({ email, password, userName, languages });
            REMOVE_USER_TIMERS[email] = setTimeout(async () => {
                const u = await userService.getUserById(user.id);
                console.log("check user is active", u.userName, u.isActivated)
                if (!u.isActivated) {
                    await userService.removeUser(email);
                }
            }, ACTIVATION_CODE_TIMEOUT)
            
            
            return res.json({ activationTimeout: ACTIVATION_CODE_TIMEOUT })
        } catch (error) {
            console.log('UserController.registration error;', error);
            next(error);
        }
    }

    async resetPassword(req: Request, res: Response, next: NextFunction) {
        try {
            const { email } = req.body;
            await userService.resetPassword(email);
            return res.sendStatus(200);
        } catch (error) {
            console.log('UserController.resetPassword error;', error);
            next(error);
        }
    }

    async resetPasswordConfirm(req: Request, res: Response, next: NextFunction) {
        try {
            const { email, token, password } = req.body;
            await userService.resetPasswordConfirm(email, token, password);
            return res.sendStatus(200);
        } catch (error) {
            console.log('UserController.login resetPasswordConfirm;', error);
            next(error);
        }
    }

    async login(req: Request, res: Response, next: NextFunction) {
        try {
            const { email, password } = req.body;
            const userData = await userService.login(email, password);
            res.cookie('refreshToken', userData.refreshToken, { maxAge: REFRESH_TOKEN_MAX_AGE, httpOnly: true }); //secure=true https
            return res.json(userData);
        } catch (error) {
            console.log('UserController.login error;', error);
            next(error);
        }
    }

    async logout(req: Request, res: Response, next: NextFunction) {
        try {
            const { refreshToken } = req.cookies;
            const token = await userService.logout(refreshToken);
            res.clearCookie('refreshToken');
            return res.json(token);
        } catch (error) {
            console.log('UserController.logout error;', error)
            next(error);
        }
    }

    async activate(req: Request, res: Response, next: NextFunction) {
        try {
            const { activationCode, email } = req.body;
            // const activationLink = req.params.link;
            // await userService.activate(activationLink);
            const userData = await userService.activate(activationCode, email);
            res.cookie('refreshToken', userData.refreshToken, { maxAge: REFRESH_TOKEN_MAX_AGE, httpOnly: true }); //secure=true https
            clearTimeout(REMOVE_USER_TIMERS[email]);
            delete REMOVE_USER_TIMERS[email];
            return res.json(userData)//.redirect(vars.clientUrl + '/subscriptions');;
            // return res.redirect(vars.clientUrl + '/subscriptions');
        } catch (error) {
            console.log('UserController.activate error;', error);
            next(error);
        }
    }

    // async activate(req: Request, res: Response, next: NextFunction) {
    //     try {
    //         const activationLink = req.params.link;
    //         await userService.activate(activationLink);
    //         return res.redirect(vars.clientUrl + '/subscriptions');
    //     } catch (error) {
    //         console.log('UserController.activate error;', error);
    //         next(error);
    //     }
    // }

    async checkAuth(req: Request, res: Response, next: NextFunction) {
        try {
            const { refreshToken } = req.cookies;
            const { user } = res.locals;
            const userData = await userService.getPopulatedUserDto(user.id)
            res.json({user: userData});
        } catch (error) {
            console.log('UserController.refresh error;', error);
            next(error);
        }
    }

    async refresh(req: Request, res: Response, next: NextFunction) {
        try {
            const { refreshToken } = req.cookies;
            const userData = await userService.refresh(refreshToken)

            res.cookie('refreshToken', userData.refreshToken, { maxAge: REFRESH_TOKEN_MAX_AGE, httpOnly: true }); //secure=true https
            res.json(userData);
        } catch (error) {
            console.log('UserController.refresh error;', error);
            next(error);
        }
    }

    async getUsers(req: Request, res: Response, next: NextFunction) {
        try {
            res.json(['123', '321'])
        } catch (error) {
            console.log('UserController.getUsers error;', error);
            next(error);
        }
    }

    async googleAuth(req: Request, res: Response, next: NextFunction) {
        try {
            const { username, email, accessToken } = req.body;
            const userData = await userService.googleAuth(email, accessToken);
            res.cookie('refreshToken', userData.refreshToken, { maxAge: REFRESH_TOKEN_MAX_AGE, httpOnly: true }); //secure=true https
            return res.json(userData);
        } catch (error) {
            console.log('UserController.googleAuth error;', error);
            next(error);
        }
    }

    async addWords(req: Request, res: Response, next: NextFunction) {
        try {
            const { user } = res.locals;
            const { knownWords, learningWords, sourceId, needWords } = req.body;
            // const words = await sourceService.getWordsIds(sourceId);
            if (!sourceId) {
                return next(ApiError.BadRequest("нет sourceId"))
            }
            // const wordsForSorting = await userService.addWordsAfterSorting(
            //     user.id,
            //     words,
            //     knownWords,
            //     learningWords,
            //     sourceId,
            //     needWords,
            // )
            // const SortSourceWordsWithData = await Promise.all(wordsForSorting.map(async w => {
            //     const wordId = w.word as string;
            //     const word = await wordsService.findById(wordId);
            //     const wordDto = toDtoIfHas(word)
            //     return { id: wordId,word: wordDto, count: w.count };
            // }));
            const worker = new Worker(path.join(__dirname, '../workers/processWordsAfterSorting.js'), {
                workerData: { knownWords, learningWords, sourceId, needWords, user }
              });
               
              worker.on('message', (result) => {
                console.log("Worker res",result);
                if (result.error) {
                    return next(result.error)
                }
                res.json({ words: result })
              });
            // return res.json({ words: SortSourceWordsWithData });
            // console.log("ADD WORDS", { knownWords, learningWords, sourceId })
            // return res.json({});
        } catch (error) {
            console.log('UserController.addWords error;', error);
            next(error);
        }
    }
    
    async changeSubscriptionPlan(req: Request, res: Response, next: NextFunction) {
        try {
            const { user } = res.locals;
            const { subscriptionPlanId } = req.body;
            const subPlan = await userService.getSubscriptionPlan(subscriptionPlanId);
            const userSub = await userService.getUserSubscriptionPlan(user.id);

            if (subPlan && subPlan.price > 0 && userSub?._id?.toString() !== subscriptionPlanId) {
                return next();
            }
            await userService.changeSubscriptionPlan(user.id, subscriptionPlanId);
            return res.json({});
        } catch (error) {
            console.log('UserController.changeSubscriptionPlan error;', error);
            next(error);
        }
    }

    async getPaymentMethods(req: Request, res: Response, next: NextFunction) {
        try {
            const { user } = res.locals;
            const paymentMethods = await billService.getUserSuccessPayments(user.id);

            return res.json({
                paymentMethods: _.unionBy(paymentMethods, "paymentDetails.title"),
            });
        } catch (error) {
            console.log('UserController.getPaymentMethods error;', error);
            next(error);
        }
    }

    async changeLanguage(req: Request, res: Response, next: NextFunction) {
        try {
            const { user } = res.locals;
            const { languageId, remove } = req.body;
            console.log("CHAABGE",  { languageId, remove }, user.id)
            if (remove) {
                await userService.removeLanguage(user.id, languageId);
            } else {
                await userService.addLanguage(user.id, languageId);
            }
            // redisCache.refresh(user.id.toString());
            return res.json({});
        } catch (error) {
            console.log('UserController.changeLanguage error;', error);
            next(error);
        }
    }
}

export const userController = new UserController();