import { NextFunction, Request, Response } from "express";
import { GutenbergBooksService } from "../externalServices/gutenbergBooks.service";
import redisCache from "../utils/redisCache";

export class booksController {
    static async getBooks(req: Request, res: Response, next: NextFunction) {
        try {
            const { language } = res.locals;
            const { page = 1, search } = req.query;
            console.log("getBooks params", req.query)
            
            const books = await GutenbergBooksService.getSearchByLang(language.key, search as string, Number(page))
            // console.log('books', language, books)
            res.json(books);
            await redisCache.add(req.originalUrl, books);
        } catch (error) {
            console.log('booksController.getBooks error;', error);
            next(error);
        }
    }

    static async getBookById(req: Request, res: Response, next: NextFunction) {
        try {
            const { language } = res.locals;
            const { bookId } = req.params;
            
            const books = await GutenbergBooksService.getBookById(language.key, bookId)

            return res.json(books);
        } catch (error) {
            console.log('booksController.getBooks error;', error);
            next(error);
        }
    }
}