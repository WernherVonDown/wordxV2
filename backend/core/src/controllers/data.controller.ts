import { NextFunction, Request, Response } from "express";
import { dataService } from "../services/data.service";
import redisCache from "../utils/redisCache";

class DataController {
    async getSubscriptionPlans (req: Request, res: Response, next: NextFunction) {
        try {
            const plans = await dataService.getSubscriptionPlans();
            res.json(plans)
            await redisCache.add(req.originalUrl, plans);
        } catch (error) {
            console.log('DataController.getSubscriptionPlans error;', error);
            next(error);
        }
    }

    async getLanguages(req: Request, res: Response, next: NextFunction) {
        try {
            const langs = await dataService.getLanguages();
            res.json(langs)
            await redisCache.add(req.originalUrl, langs);
        } catch (error) {
            console.log('DataController.getLanguages error;', error);
            next(error);
        }
    }

    async getLanguageLevels(req: Request, res: Response, next: NextFunction) {
        try {
            const langs = await dataService.getLanguageLevels();
            res.json(langs)
            await redisCache.add(req.originalUrl, langs);
        } catch (error) {
            console.log('DataController.getLanguageLevels error;', error);
            next(error);
        }
    }

    async getProducts(req: Request, res: Response, next: NextFunction) {
        try {
            const products = await dataService.getProducts();
            res.json(products)
            await redisCache.add(req.originalUrl, products);
        } catch (error) {
            console.log('DataController.getProducts error;', error);
            next(error);
        }
    }
}

export const dataController = new DataController();