import { NextFunction, Request, Response } from "express";
import { MemeService } from "../externalServices/memes.service";

export class memeController {
    static async getRandomByLanguage(req: Request, res: Response, next: NextFunction) {
        try {
            const { language } = res.locals;
            
            const meme = await MemeService.getRandomByLang(language.key)
            console.log('language', language, meme)
            return res.json(meme);
        } catch (error) {
            console.log('memeController.getRandomByLanguage error;', error);
            next(error);
        }
    }
}