import { NextFunction, Request, Response } from "express";
import { userService } from "../services/user.service";
import { wordsService } from "../services/words.service";

class WordsController {
    async getWords(req: Request, res: Response, next: NextFunction) {
        try {
            const { user, language } = res.locals;
            const wordsInfo = await userService.getUserWords(user.id, language.id);
            return res.json(wordsInfo);
        } catch (error) {
            console.log('WordsController.getWords error;', error);
            next(error);
        }
    }

    async getWordsSources(req: Request, res: Response, next: NextFunction) {
        try {
            const { user, language } = res.locals;
            const userWordsSources = await userService.getUserWordsSource(user.id, language.id);
            return res.json({userWordsSources});
        } catch (error) {
            console.log('WordsController.getWordsSources error;', error);
            next(error);
        }
    }

    async learnedWords(req: Request, res: Response, next: NextFunction) {
        try {
            const { user} = res.locals;
            const { learnedWordsNew } = req.body;
            console.log("NEW LEARNED WORDS", learnedWordsNew)
            await userService.changeLearnedWords(user.id, learnedWordsNew);
            return res.json({});
        } catch (error) {
            console.log('WordsController.learnedWords error;', error);
            next(error);
        }
    }
}

export const wordsController = new WordsController();