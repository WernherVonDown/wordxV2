import { PaymentStatuses } from "@a2seven/yoo-checkout";
import * as pay from "@a2seven/yoo-checkout";
import { NextFunction, Request, Response } from "express";
import { PaymentTypes } from "../const/payment/PaymentTypes";
import { billService } from "../services/bill.service";
import { userService } from "../services/user.service";
import { yookassaService } from "../externalServices/yookassa.service";
import ScheduledTaskModel from "../models/ScheduledTask.model";
import { TaskTypes } from "../const/scheduler/TASK_TYPES";
import { DAY, MIN } from "../utils/convertors/convertedMilliseconds";
import { notificationsService } from "../services/notificatons.service";
import { publishSendNotification } from "../broker/socket/publisher";

class PaymentController {
    async payment(req: Request, res: Response, next: NextFunction) {
        try {
            const { user } = res.locals;
            const { paymentType, subscriptionPlanId, amount, paymentMethodId } = req.body;
            console.log("PAYMENT", { paymentType, subscriptionPlanId, amount, paymentMethodId })
            if (paymentType === PaymentTypes.subscription) {
                const subPlan = await userService.getSubscriptionPlan(subscriptionPlanId);
                if (subPlan) {
                    const bill = await billService.createBill({
                        user: user.id,
                        subscriptionPlan: subPlan._id,
                        amount: subPlan.price,
                        paymentType,
                        rebill: true,
                        description: subPlan.title,
                    });
                    const payment = await yookassaService.createPayment(bill, user);
                    await billService.setPaymentId(bill._id, payment.id);
                    return res.json({
                        success: true,
                        redirect: payment.confirmation.confirmation_url || '/'
                    })
                }
            }

            if (paymentType === PaymentTypes.wallet && amount > 0) {
                const bill = await billService.createBill({
                    user: user.id,
                    amount: amount,
                    paymentType,
                    rebill: false,
                    description: `Пополнение ${amount}`,
                    paymentMethodId,
                });
                if (paymentMethodId) {
                    const payment = await yookassaService.repay(bill, user);
                    await billService.setPaymentId(bill._id, payment.id);
                    return res.json({
                        success: true,
                        // redirect: payment.confirmation.confirmation_url || '/'
                    })
                } else {
                    const payment = await yookassaService.createPayment(bill, user);
                    await billService.setPaymentId(bill._id, payment.id);
                    return res.json({
                        success: true,
                        redirect: payment.confirmation.confirmation_url || '/'
                    })
                }
            }
            res.json({})
        } catch (error) {
            console.log('PaymentController.createBill error;', error);
            next(error);
        }
    }

    async paymentStatus(req: Request, res: Response, next: NextFunction) {
        try {
            console.log("EEEE", req.body, req.body.metadata)
            const { object: { id: pId, status, payment_method: { id, saved, card, title }, metadata: { billId, userId } } } = req.body;
            if (status === PaymentStatuses.succeeded) {
                const bill = await billService.getBillById(billId);
                if (!bill) {
                    throw new Error("Счёт не найден");
                }
                console.log("FOUND BILL", bill, saved)
                if (bill.paymentType === PaymentTypes.wallet) {
                    await userService.increaseConins(bill.user, bill.amount);
                    const notification = await notificationsService.create(
                        `Кошелёк пополнен на ${bill.amount} монет!`,
                        '',
                    );
        
                    await userService.addNotification(bill.user, notification._id);
                    publishSendNotification({userId: bill.user, data: notification})
                }

                if (bill.rebill && saved) {
                    const nextBill = await billService.payAndRebill(bill, id, { card, title });
                    
                    console.log("NEXT BILL", nextBill)
                    await ScheduledTaskModel.create({
                        type: TaskTypes.PAY_SUBSCRIPTION,
                        user: nextBill.user,
                        active: true,
                        executedAt: Date.now() + (5 * DAY),
                        data: {
                            billId: nextBill._id,
                        }
                    })
                    const user = await userService.getUserById(bill.user);
                    if (user.subscriptionPlan?.toString() !== bill.subscriptionPlan.toString()) {
                        await userService.changeSubscriptionPlan(bill.user, bill.subscriptionPlan);
                        console.log("subscriptionPlan установлен", bill);
                    }
                }
            } else if (status === PaymentStatuses.waiting_for_capture) {
                await yookassaService.capture(pId).then(e => console.log("CAPTURED", e)).catch(e => console.log("ERRORR CAPTURE", pId, e))
            }
            
            res.sendStatus(200)
        } catch (error) {
            console.log('PaymentController.paymentStatus error;', error);
            next(error);
        }
    }
}

export const paymentController = new PaymentController();