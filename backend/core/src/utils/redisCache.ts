import { redisClient } from "../boot/startRedis";

const getRefreshKey = (refreshKey: string) => `refresh:${refreshKey}`
const getCacheKey = (key: string, refreshKey?: string) => !refreshKey ? `cache:${key}` : `cache:${getHash(refreshKey)}:${key}`;

function getHash(input: string){
    return input
    var hash = 0, len = input.length;
    for (var i = 0; i < len; i++) {
      hash  = ((hash << 5) - hash) + input.charCodeAt(i);
      hash |= 0; // to 32bit integer
    }
    return hash;
  }

const deleteKeysByPattern = async (pattern: string) => {
    const keys = await redisClient.keys(pattern);
    let count = 0;
    if (keys.length) {
        count = await redisClient.del(keys);
    }
    console.log(`Redis cache: ${count} deleted by ${pattern}`);
}

export default {
    add: async (key: string, data: any, timeSec: number = 60 * 60 * 24, refreshKey?: string) => {
        try {
            console.log("add cache", key, refreshKey?.slice(0, 10), refreshKey && getHash(refreshKey))
            const strData = JSON.stringify(data);
            await redisClient.setEx(getCacheKey(key, refreshKey), timeSec, strData);
        } catch (error) {
            console.log("Error; redisCache.add", error)
        }
    },
    get: async (key: string, refreshKey?: string) => {
        try {
            console.log("get cache", key, refreshKey?.slice(0, 10), refreshKey && getHash(refreshKey))
            const strData = await redisClient.get(getCacheKey(key, refreshKey));
            if (strData) {
                return JSON.parse(strData);
            }
        } catch (error) {
            console.log("Error; redisCache.get", error)
        }
    },
    refresh: async (refreshKey: string) => {
        try {
            console.log("refresh cache", refreshKey?.slice(0, 10))
            await deleteKeysByPattern(`cache:${getHash(refreshKey)}:*`)
        } catch (error) {
            console.log("Error; redisCache.refresh", error)
        }
    },
    clear: async () => {
        try {
            await deleteKeysByPattern('cache:*')
        } catch (error) {
            console.log("Error; redisCache.clear", error)
        }
    }
}