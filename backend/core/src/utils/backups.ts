import path from "path";
import { vars } from "../config/vars";
import fs from 'fs';
import { yandexS3Service } from "../externalServices/yandexS3.service";

const processedFiles = new Set()

export const watchBackups = () => {
    console.log("watchBackups", vars.backupPath)
    if (!vars.backupPath) return;
    if (!fs.existsSync(vars.backupPath)) {
        fs.mkdirSync(vars.backupPath)
    }
    fs.watch(vars.backupPath, async (eventType, filename) => {
        if (!processedFiles.has(filename) && eventType === 'change') {
            console.log(`Файл ${filename} был изменен. Событие: ${eventType}`);
            processedFiles.add(filename)
            await new Promise(res => setTimeout(res, 10000))
            saveBackup(filename as string)
        }
    });
}

const getBackupFolder = () => `/backups/${vars.envName || 'unknown'}`

const getBackupPath = (filename: string) => `${getBackupFolder()}/${filename}`

const saveBackup = async (filename: string) => {
    if (!vars.backupPath) return;
    const backupFolder = getBackupFolder();
    const pathToFile = path.join(vars.backupPath, filename);
    console.log("SAVE BACKUP", pathToFile, backupFolder)
    const backups = await yandexS3Service.getList(backupFolder);
    console.log("BACKUPS", backups)
    if (backups.Contents.length < vars.maxBackups) {
        await yandexS3Service.upload(pathToFile, backupFolder);
        console.log("SAAAFED")
    } else {
        const extraBackups = backups.Contents.length - vars.maxBackups;
        const shouldBeRemoved = backups.Contents.sort((a, b) => new Date(a.LastModified) > new Date(b.LastModified) ? 1 : -1).slice(0, extraBackups + 1);
        await yandexS3Service.upload(pathToFile, backupFolder);
        console.log("TOO MUCH", extraBackups, shouldBeRemoved)
        await Promise.all(shouldBeRemoved.map(f => yandexS3Service.removeFile(f.Key)));
        fs.chmod(pathToFile, '777', (err) => {
            err && console.log("CHMODE ERRR", err)
            fs.unlink(pathToFile, (e) => e && console.log(`delete file ${pathToFile}; error`, e));
        })
        
    }

}