import { yandexS3Service } from "../externalServices/yandexS3.service";
import { sendHttpRequest } from "./http/sendHttpRequest";

const yandexS3Url = 'https://wordx.storage.yandexcloud.net';

export const getBookUrlIfExists = async (bookId: string | number): Promise<string> => {
    try {
        const bookPath = getBookPath(bookId) //yandexS3Url + getBookPath(bookId);
        console.log("CHECK BOOK EXISTS", bookPath)
        // const res = await sendHttpRequest({
        //     method: 'GET',
        //     url: bookPath,
        //     data: {},
        // })
        const res = await yandexS3Service.getList(bookPath)
        console.log("CHECK BOOK RES", res)
        const bookKey = res.Contents.find(b => b.Key.indexOf('.txt'))?.Key
        if (bookKey) {
            return `${yandexS3Url}/${bookKey}`
        }
        return ''
    } catch (error) {
        return '';
    }
};

export const getMovieUrlIfExists = async (movieId: string, language: string) => {
    const moviePath = getMoviePath(movieId, language)
    const res = await yandexS3Service.getList(moviePath);
    const movieKey = res.Contents[0]?.Key;
    console.log("EXITSTS", res)
    if (movieKey) {
        return `${yandexS3Url}/${movieKey}`
    }

    return '';
}

export const saveMovieOnS3 = async (files: string[], movieId: string, language: string) => {
    return yandexS3Service.uploadFiles(files, getMoviePath(movieId, language));
}

export const getMoviePath = (movieId: string, language: string) => {
    return `/movies/${movieId}_${language}`
}

export const saveBookOnS3 = async (files: string[], bookId: number | string) => {
    return yandexS3Service.uploadFiles(files, getBookPath(bookId));
}

export const getBookPath = (bookId: number | string) => {
    return `/books/${bookId}`;
};