import { getMasterProcId } from './getMasterProcId';
import pm2 from 'pm2';


export const sendTaskToMaster = (task, isNewTask) => {
    const packet = {
        data: {...task, isNewTask},
        topic: 'task',
    };

    return new Promise(async (resolve, reject) =>
        pm2.sendDataToProcessId(await getMasterProcId(), packet, (err) => {
            if (err) {
                return reject(err);
            }
            resolve()
        }))
};
