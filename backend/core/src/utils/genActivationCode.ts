import { getRandomInt } from "./getRandomInt";

export function genActivationCode (len: number = 4): string {
    let res = '';
    for(let i = 0; i < len; i++) {
        res += getRandomInt()
    }

    return res;
}
