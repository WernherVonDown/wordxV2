import process from 'process';
import { Channel, Connection } from "amqplib";

export const createChannel = async (connection: Connection): Promise<Channel> => {
    const channel = await connection.createChannel();
    channel.on('error', (e) => {
        console.log(e.message);
        process.exit();
    });

    return channel;
}