import { ControllerOpt, IBrokerHandler } from "../../../types/broker";
import { getConnection } from "./getConnection";
import { wrapMsgPayloadWithCatch } from "./wrapMsgPayloadWithCatch";

interface IArgs {
    queueName: string;
    prefetch: number;
    noAck?: boolean;
}

export const createConsumer = async ({
    queueName, prefetch, noAck,
}: IArgs, handler: ({ payload }: ControllerOpt) => Promise<void>) => {
    const connection = await getConnection();
    const channel = await connection.createChannel();
    await channel.prefetch(prefetch, true);
    await channel.consume(queueName, wrapMsgPayloadWithCatch(handler, channel))
}
