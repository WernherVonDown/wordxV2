import process from 'process';
import amqp, { Connection } from "amqplib"
import { brokerConfig } from "../../config/broker"

export const createConnection = async (): Promise<Connection> => {
    const connection = await amqp.connect(brokerConfig.url + '?heartbeat=0'
    );
    connection.on('error', (e) => {
        console.log("broker:error", e);
        process.exit();
    })

    connection.on('blocked', (reason) => {
        console.log("broker:blocked", reason);
        process.exit();
    })

    return connection;
}