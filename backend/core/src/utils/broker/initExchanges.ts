import { Connection } from "amqplib";
import { createChannel } from "./createChannel";
import * as exchangesList from '../../config/brokerExchanges';
import { createExchange } from "./createExchange";

export const initExchanges = async (connection: Connection): Promise<void> => {
    const channel = await createChannel(connection);
    const exchanges = Object.values(exchangesList);
    await Promise.all(
        exchanges.map((exchange) => (
            createExchange({
                channel,
                exchange,
            })
        ))
    )

    await channel.close();
}