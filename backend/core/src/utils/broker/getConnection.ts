import { Connection } from 'amqplib';
import { createConnection } from './createConnection';

let connectionState: Promise<Connection>;

export const getConnection = (): Promise<Connection> => {
    if (!connectionState) {
        console.log("broker:connection:start");
        connectionState = createConnection();
    }
    return connectionState;
}