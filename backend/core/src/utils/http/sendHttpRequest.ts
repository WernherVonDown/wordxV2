import axios, { Method, CancelToken } from 'axios';
import { ResponseType } from 'axios';
import axiosThrottle from 'axios-request-throttle';
axiosThrottle.use(axios, { requestsPerSecond: 1 });
interface IArgs {
    method: Method;
    url: string;
    data: any;
    params?: Record<string, any>;
    headers?: Record<string, any>;
    cancelToken?: CancelToken;
    onUploadProgress?: (progressEvent: any) => void;
    onDownloadProgress?: (progressEvent: any) => void;
    responseType?: ResponseType;
}

export const sendHttpRequest = (args: IArgs) => {
    const {
        url,
        method,
        data,
        params,
        headers,
        cancelToken,
        responseType,
        onUploadProgress,
        onDownloadProgress,
    } = args;
    return axios({
        url,
        method,
        data,
        params,
        headers,
        cancelToken,
        responseType,
        onUploadProgress,
        onDownloadProgress,
    });
};
