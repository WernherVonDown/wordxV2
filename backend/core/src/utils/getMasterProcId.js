import pm2 from 'pm2';

export function getMasterProcId() {
    return new Promise((resolve, reject) => {
        pm2.list((err, list) => {
            if (err) return reject(err);
            list.map((process) => {
                if (process.name.startsWith('wordx') && process.pm2_env.args && process.pm2_env.args.includes('--isMaster')) {
                    resolve(process.pm_id)
                }
            });
        })
    });
}
