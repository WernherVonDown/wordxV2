import { removeTagsFromSrt } from "./removeTagsFromSrt";

export const srtToText = (srtText: string) => {
    const res = srtText.toString().replace(/\n?([0-9]+)\n([0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3}) --> ([0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3})\n/gm, '')
    // console.log('RES', removeTagsFromSrt(res))
    return removeTagsFromSrt(res);
    return srtText.replace(/\n\n*/g,'\r\n').replace(/\r\n/g, ' ').replace(/[^a-zA-Z .!?']/gmi, "").replace(/\s+/g, ' ').trim()
}
