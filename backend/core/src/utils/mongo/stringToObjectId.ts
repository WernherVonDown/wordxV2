import { Types } from "mongoose";

export const stringToObjectId = (str: string) => new Types.ObjectId(str);
