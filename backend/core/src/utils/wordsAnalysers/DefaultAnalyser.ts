//@ts-ignore
import countWords from "count-words";
import { LanguageKeys } from "../../const/language/Languagekeys";

export class DefaultAnalyser {
    constructor(
        private languageKey: LanguageKeys,
    ) {}
    analysStr = (text: string) => {
        const countedWordsObject = countWords(text, true);
        const wordsCounts = Object.values(countedWordsObject) as number[];
        const uniqueWordsCount = wordsCounts.length;
        const allWordsCount = wordsCounts.reduce((p: number, c: number) => p + c, 0)
        const result = Object.keys(countedWordsObject).map((w, i) => (
            {
                word: w,
                count: countedWordsObject[w],
                percent: countedWordsObject[w] / allWordsCount
            }
        ))
        console.log('RESULT', { uniqueWordsCount, allWordsCount })
        return { words: result, uniqueCount: uniqueWordsCount, allCount: allWordsCount };
    }
}
