let res = {};
function startsWithCapital(word){
    return word.charAt(0) === word.charAt(0).toUpperCase()
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function decapitalizeFirstLetter(string) {
    return string.charAt(0).toLowerCase() + string.slice(1);
}

const a = `liebäugelnd	liebäugelndste
liebäugelnd	liebäugelndstem
liebäugelnd	liebäugelndsten
liebäugelnd	liebäugelndster
liebäugelnd	liebäugelndstes
Liebäugelnde	Liebäugelnden
Liebäugelndere	Liebäugelnderen
Liebäugelndste	Liebäugelndsten
liebbehalten	liebbehält
liebbehalten	liebbehalte
liebbehalten	liebbehaltend
liebbehalten	liebbehaltene
liebbehalten	liebbehaltenem
liebbehalten	liebbehaltenen
liebbehalten	liebbehaltener
liebbehalten	liebbehaltenes
Liebbehalten	Liebbehaltens
liebbehalten	liebbehaltest
liebbehalten	liebbehaltet
liebbehalten	liebbehältst
liebbehalten	liebbehielt
liebbehalten	liebbehielte
liebbehalten	liebbehielten
liebbehalten	liebbehieltest
liebbehalten	liebbehieltet
liebbehalten	liebzubehalten
liebbehaltend	liebbehaltende
liebbehaltend	liebbehaltendem
liebbehaltend	liebbehaltenden
liebbehaltend	liebbehaltender
liebbehaltend	liebbehaltendes
Liebbehaltende	Liebbehaltenden
Liebbehaltene	Liebbehaltenen
Liebe	Lieben
liebebedürftig	liebebedürftige
liebebedürftig	liebebedürftigem
liebebedürftig	liebebedürftigen
liebebedürftig	liebebedürftiger
liebebedürftig	liebebedürftigere
liebebedürftig	liebebedürftigerem
liebebedürftig	liebebedürftigeren
liebebedürftig	liebebedürftigerer
liebebedürftig	liebebedürftigeres
liebebedürftig	liebebedürftiges
liebebedürftig	liebebedürftigste
liebebedürftig	liebebedürftigstem
liebebedürftig	liebebedürftigsten
liebebedürftig	liebebedürftigster
liebebedürftig	liebebedürftigstes
Liebebedürftige	Liebebedürftigen
Liebebedürftigere	Liebebedürftigeren
Liebelei	Liebeleien
lieben	geliebt
lieben	lieb
lieben	Liebe`.split('\n').map(e => e.split('\t')).forEach(e => {
    let w1 = e[0];
    let w2 = e[1];
    if (startsWithCapital(w1) !== startsWithCapital(w2)) {
        if (startsWithCapital(w2)) {
            w2 = decapitalizeFirstLetter(w2)
        } else {
            w2 = capitalizeFirstLetter(w2)
        }
    }
    res[w2] = w1;
    res[w1] = w1;
})

console.log("a", res)