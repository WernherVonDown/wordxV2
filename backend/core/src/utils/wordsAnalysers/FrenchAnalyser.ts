//@ts-ignore
import countWords from "count-words";
import { YandexDict } from "../../externalServices/yandexDict.service";
//@ts-ignore
import NlpjsTFr from 'nlp-js-tools-french';
//https://www.npmjs.com/package/words-splitter
class FrenchAnalyser {
    private yandexDict: YandexDict;

    constructor() {
        this.yandexDict = new YandexDict('fr-ru')
    }

    async translate(text: string) {
        try {
            const res: any[] = await this.yandexDict.translate(text);
            if (res && res[0]?.tr?.length) {
                // console.log('t', res[0].tr, text, res[0].text)
                const maxFr = Math.max(...res[0].tr.map((t: any) => t.fr))
                // console.log("MAX FR", maxFr)
                const resTran = res[0].tr.filter((t: any) => t.fr === maxFr).map((t: any) => t.text).join(', ')
                if (res[0].text !== text) {
                    return {tr: resTran, text: res[0].text}
                }
                return {tr: resTran}
            } else {
                // console.log("SOME SHIITT", res)
            }
        } catch (error) {
            console.log("EnglishAnalyser.translate error", error)
        }
    }

    analysStr = (text: string) => {   
        text = text.replace(/(j|s|c|l|m|t|d)'/gmi, "")
        const nlpToolsFr = new NlpjsTFr(text, {strictness: true});
        const lematizedWords = nlpToolsFr.lemmatizer();
        // console.log("ANALYS", lematizedWords)
        const maxIndex = Math.max(...lematizedWords.map((l: any) => l.id));
        // console.log("TEXT", maxIndex)
        const replacedWords = new Set();
        for(let i = 0; i <=maxIndex; i++) {
            
            const words = lematizedWords.filter((w: any) => w.id === i);
            const word = words[0]?.word;

            if (replacedWords.has(word)) {
                continue;
            }

            if (!isNaN(word)) {
                text = text.replace(new RegExp(word, "igm"), "");
                continue;
            }

            const lemmas = words.map((w: any) => w.lemma).join(' ');
            // console.log("REPLACE LEMMA", word, lemmas, words, i)
            text = text.replace(new RegExp(`(^|@|\\W)${word}(\\W|$)`, "igm"), ` ${lemmas} `);
            replacedWords.add(word)
        }
        // console.log("RESULT TEXT", text)
        const countedWordsObject = countWords(text, true);
        // console.log("COUNTED OBJECT", countedWordsObject)
        const wordsCounts = Object.values(countedWordsObject) as number[];
        const uniqueWordsCount = wordsCounts.length;
        const allWordsCount = wordsCounts.reduce((p: number, c: number) => p + c, 0)
        const result = Object.keys(countedWordsObject).map((w, i) => (
            {
                word: w,
                count: countedWordsObject[w],
                percent: countedWordsObject[w] / allWordsCount
            }
        ))
        return { words: result, uniqueCount: uniqueWordsCount, allCount: allWordsCount };
    }
}

export const frenchAnalyser = new FrenchAnalyser();


// https://www.npmjs.com/package/nlp-js-tools-french
// https://www.npmjs.com/package/pluralize-fr
// https://www.npmjs.com/package/dictionaries-in-array
// https://www.npmjs.com/package/french-verbs
// https://www.npmjs.com/package/@zxcvbn-ts/language-fr
// https://www.npmjs.com/package/french-verbs-list

// https://www.npmjs.com/package/french-dev-memes :)))

// https://dictionaryapi.dev/
// https://github.com/lyndabelfar/Dictionary/blob/main/script.js

//https://www.bookrix.com/books;lang:fr.html