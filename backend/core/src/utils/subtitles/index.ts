//@ts-ignore
import OpenSubtitles from 'opensubtitles-api';

const OS = new OpenSubtitles('OSTestUserAgent');
//https://opensubtitles.stoplight.io/docs/opensubtitles-api/6be7f6ae2d918-download
// https://github.com/vankasteelj/opensubtitles.com
OS.search({
    imdbid: 'tt0314979',
    sublanguageid: 'fre',
    gzip: true
}).then((e: any) => console.log("RES", e)).catch((e: any) => console.log("ERROR", e))