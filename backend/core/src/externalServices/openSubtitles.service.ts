//@ts-ignore
import OS from 'opensubtitles.com';
import process from 'process';
import { vars } from '../config/vars';
import { LanguageKeys } from '../const/language/Languagekeys';
import { randomIntFromInterval } from '../utils/randomIntFromInterval';
const os = new OS({ apikey: vars.openSubtitles.apiKey })

const OSLangs = {
    [LanguageKeys.english]: 'en',
    [LanguageKeys.french]: 'fr',
    [LanguageKeys.german]: 'de',
}

//411d95e6
//https://www.npmjs.com/package/metafilm
//https://github.com/Shivamk99/movies-info

class OpenSubtitles {
    os: OS;
    getOs = async () => {
        if (!this.os) {
            await os.login({
                username: vars.openSubtitles.username,
                password: vars.openSubtitles.password,
            })
            this.os = os;
        }
       
        return this.os
    }
    search = async (imdb_id: string, languageKey: LanguageKeys) => {
        const os = await this.getOs()
        const lang = OSLangs[languageKey];
        console.log("GOT OS", os, lang)
        const res = await os.subtitles({
            imdb_id,
            languages: lang,
        })
        // console.log("RESULT", res.data.map((e: any) => e.attributes))
        const file = res.data.map((e: any) => e?.attributes?.files[0]);
        // TODO Доступны только переведенные
        if (file?.length) {
            console.log("RESULT 2", file[0]?.file_id)
            return file[0]?.file_id;
        }
    }

    async download(file_id: string) {
        const os = await this.getOs()
        const res = await os.download({
            file_id,
        });

        console.log("RES", res?.link)
        return res?.link;
    }


}

export const openSubtitles = new OpenSubtitles();