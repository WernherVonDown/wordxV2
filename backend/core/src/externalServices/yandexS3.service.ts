//@ts-ignore
import EasyYandexS3 from "easy-yandex-s3";
import { vars } from "../config/vars";

interface IYS3List {
    IsTruncated: boolean,
    Contents: {
        Key: string,
        LastModified: string,
        ETag: string,
        ChecksumAlgorithm: any[],
        Size: number,
        StorageClass: string,
        Owner: any[]
    }[],
    Name: string,
    Prefix: string,
    Delimiter: string,
    MaxKeys: number,
    CommonPrefixes: { Prefix: string }[],
    KeyCount: number
}

interface IYS3UploadRes  {
    ETag: string,
    Location: string,
    key: string,
    Key: string,
    Bucket: 'wordx'
  }

class YandexS3Service {
    private s3;
    constructor() {
        this.s3 = new EasyYandexS3({
            auth: {
                accessKeyId: vars.yandex.accessKeyId,
                secretAccessKey: vars.yandex.secretAccessKey,
            },
            Bucket: vars.yandex.bucketName,
            debug: true,
        })
    }

    async upload(pathToLocalFile: string, pathInBucket: string = '', options: any = {}) {
        console.log("ys3 upload", pathToLocalFile, pathInBucket)
        if (!pathToLocalFile) return false;
        const upload = await this.s3.Upload({
            path: pathToLocalFile,
            save_name: true,
            ...options,
        }, pathInBucket);

        return upload;
    }

    async removeFile(pathInBucket: string) {
        await this.s3.Remove(pathInBucket);
    }

    async uploadFiles (pathToLocalFiles: string[], pathInBucket: string = '', options: any = {}): Promise<IYS3UploadRes[]> {
        const upload = await this.s3.Upload(pathToLocalFiles.map(p => ({
            path: p,
            save_name: true,
            ...options,
        })), pathInBucket);

        return upload;
    }

    async getList(dirPath: string): Promise<IYS3List> {
        return this.s3.GetList(
            dirPath,
        );

    }
}

export const yandexS3Service = new YandexS3Service();