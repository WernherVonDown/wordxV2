import { PaymentStatuses, YooCheckout } from '@a2seven/yoo-checkout';
import { vars } from '../config/vars';
import { IBill } from '../models/Bill.model';
import { IUser, IUserDto } from '../models/User.model';

class YookassaService {
    private yooKassa: YooCheckout = new YooCheckout({
        shopId: vars.yookassa.shopId,
        secretKey: vars.yookassa.secretKey
    })

    async createPayment(bill: IBill, user: IUser) {
        const payment = await this.yooKassa.createPayment({
            amount: {
                value: `${bill.amount}`,
                currency: "RUB"
            },
            payment_method_data: {
                type: "bank_card"
            },
            confirmation: {
                type: "redirect",
                return_url: vars.clientUrl + '/languages'
            },
            receipt: {
                customer: {
                    email: 'da1996boss@mail.ru',//user.email
                  },
                items: [
                    {
                        amount: {
                            value: `${bill.amount}`,
                            currency: "RUB",
                        },
                        description: bill.description || `Заказ ${bill._id}`,
                        quantity: '1',
                        vat_code: 1,
                    }
                ]
            },
            description: bill.description || `Заказ ${bill._id}`,
            save_payment_method: true,
            metadata: {
                billId: bill._id,
                email: user.email,
                userId: user.id,
            }
        });

        console.log("PAYMENT", payment)
        return payment;
    }

    async repay(bill: IBill, user: IUser) {
        const payment = await this.yooKassa.createPayment({
            amount: {
                value: `${bill.amount}`,
                currency: "RUB"
            },
            payment_method_id: bill.paymentMethodId,
            description: bill.description || `Заказ ${bill._id}`,
            receipt: {
                customer: {
                    email: 'da1996boss@mail.ru',//user.email
                  },
                items: [
                    {
                        amount: {
                            value: `${bill.amount}`,
                            currency: "RUB",
                        },
                        description: bill.description || `Заказ ${bill._id}`,
                        quantity: '1',
                        vat_code: 1,
                    }
                ]
            },
            metadata: {
                billId: bill._id,
                email: user.email,
                userId: user.id,
            }
        });

        console.log("REPAYMENT", payment, {
            amount: {
                value: `${bill.amount}`,
                currency: "RUB"
            },
            payment_method_id: bill.paymentMethodId,
            description: bill.description || `Заказ ${bill._id}`,
            metadata: {
                billId: bill._id,
                email: user.email,
                userId: user.id,
            }
        })
        if (payment.status === PaymentStatuses.waiting_for_capture) {
            await this.capture(payment.id);
        }
        // await yooKassa.capturePayment(payment.id, {})
        return payment;
    }

    async capture(paymentId: string) {
        return this.yooKassa.capturePayment(paymentId, {});
    }
}

export const yookassaService = new YookassaService();