//@ts-ignore
// import memes from 'random-memes';
import { LanguageKeys } from '../const/language/Languagekeys';
// /    "random-memes": "github:WernherVonDown/random-memes",
interface IMeme {
    image: string,
    category?: string,
    captions: string,
    id: string,
}

const ConvertLanguageKey = {
    [LanguageKeys.english]: 'en',
    [LanguageKeys.french]: 'fr',
    [LanguageKeys.german]: 'de',
}

export class MemeService {
    static async getRandom(): Promise<IMeme> {
        return {
            image: 'string',
            category: 'string',
            captions: 'string',
            id: 'string',
        }
        // return memes.random()
    }

    static async getRandomByLang(langugeKey: LanguageKeys): Promise<IMeme> {
        return {
            image: 'string',
            category: 'string',
            captions: 'string',
            id: 'string',
        }
        // return memes.fromReddit(ConvertLanguageKey[langugeKey]);
    }
}