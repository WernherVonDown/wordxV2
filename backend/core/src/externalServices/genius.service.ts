import * as Genius from 'genius-lyrics';
import _ from "lodash"
import { vars } from '../config/vars';

const token = vars.genius.token

const client = new Genius.Client(token)

interface ISongSearch {
    partial: boolean;
    title: string;
    fullTitle: string;
    featuredTitle: string;
    id: number;
    thumbnail: string;
    image: string;
    url: string;
    endpoint: string;
    artist: any;
    album?: any;
    releasedAt?: Date;
    instrumental: boolean;
}

const fields = [
    'partial',
    'title',
    'fullTitle',
    'featuredTitle',
    'id',
    'thumbnail',
    'image',
    'url',
    'endpoint',
    'artist',
    'album',
    'releasedAt',
    'instrumental',
]

export const start = async ()=> {
    const client = new Genius.Client(token)
    const searches = await client.songs.search("Rienhard Mey");
    console.log("SONTS", await client.artists.get(102053))
// Pick first one
const firstSong = searches[0];
// console.log(firstSong);

// Ok lets get the lyrics
const lyrics = await firstSong.lyrics();
// console.log("Lyrics of the Song:\n", lyrics, "\n");
}

export class GeniusService {
    static async searchSong (q: string): Promise<ISongSearch[]> {
        const searches = await client.songs.search(q);
        console.log('searches', searches)
        return searches.map(s => s._raw) as ISongSearch[];
    } 

    static async getLyrics(songId: number): Promise<any> {
        const song = await client.songs.get(songId);
        const text = await song.lyrics()
        return text
    } 

    static async getSong(songId: number): Promise<ISongSearch> {
        const song = await client.songs.get(songId);
        return song?._raw;
    } 
} 