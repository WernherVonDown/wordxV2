import { AxiosResponse } from 'axios';
import { LanguageKeys } from '../const/language/Languagekeys';
import { sendHttpRequest } from '../utils/http/sendHttpRequest';
import fs from 'fs';
import { vars } from '../config/vars';
import path from 'path';
import { getBookUrlIfExists, saveBookOnS3 } from '../utils/sourcesCache';

interface IMeme {
    image: string,
    category?: string,
    captions: string,
    id: string,
}

const GUTENBERG_ENDPOINT = 'https://gutendex.com/books'

enum ShortLanguage {
    en = 'en',
    fr = 'fr',
    de = 'de'
}

const ConvertLanguageKey = {
    [LanguageKeys.english]: ShortLanguage.en,
    [LanguageKeys.french]: ShortLanguage.fr,
    [LanguageKeys.german]: ShortLanguage.de,
}



export interface IGutenbergBook {
    id: number,
    title: string,
    authors: [
        {
            name: string,
            birth_year: number,
            death_year: number
        }
    ],
    translators: [],
    subjects: string[],
    bookshelves: string[],
    languages: ShortLanguage[],
    copyright: false,
    media_type: "Text",
    formats: {
        "application/x-mobipocket-ebook": string,
        "application/epub+zip": string,
        "image/jpeg": string,
        "text/html; charset=iso-8859-1": string,
        "text/plain; charset=iso-8859-1": string,
        "text/html": string,
        "text/plain": string,
        "application/rdf+xml": string,
        "text/plain; charset=us-ascii": string,
        "text/plain; charset=utf-8": string,
    },
    download_count: number
}

interface IBookDto {
    id: number,
    title: string,
    author: string,
    cover: string,
    popularity: number,
}

const bookToDto = (book: IGutenbergBook): IBookDto => ({
    id: book.id,
    title: book.title,
    author: book.authors.map(a => a.name).join(','),
    cover: book.formats['image/jpeg'],
    popularity: book.download_count,
})

interface GutenbergApiResponse {
    count: number,
    next: string | null,
    previos: string | null,
    results: IGutenbergBook[],
}

interface IBooksResponse {
    page: number,
    count: number,
    next: boolean,
    previos: boolean,
    results: IBookDto[],
}

const pageRegex = /page=(\d+)/;

export class GutenbergBooksService {
    static async getBooks(url?: string): Promise<GutenbergApiResponse> {
        const res: AxiosResponse<GutenbergApiResponse> = await sendHttpRequest({
            method: 'GET',
            url: `${url || GUTENBERG_ENDPOINT}`,
            params: {
                languages: "en,fr,de",
            },
            data: {}
        });

        return res.data;
    }


    static async getBookById(languageKey: LanguageKeys, bookId: string): Promise<IBookDto> {
        const res: AxiosResponse<GutenbergApiResponse> = await sendHttpRequest({
            method: 'GET',
            url: `${GUTENBERG_ENDPOINT}`,
            params: {
                languages: ConvertLanguageKey[languageKey],
                ids: bookId,
            },
            data: {}
        });

        if (!res.data.results.length) {
            throw "Книга не найдена"
        }

        return bookToDto(res.data.results[0])
    }

    static async getBookUrlById(languageKey: LanguageKeys, bookId: string): Promise<string> {
        const bookUrl = await getBookUrlIfExists(bookId);
        if (bookUrl) {
            console.log("JOING BOOK", bookUrl)
            return bookUrl;
        }
        const res: AxiosResponse<GutenbergApiResponse> = await sendHttpRequest({
            method: 'GET',
            url: `${GUTENBERG_ENDPOINT}`,
            params: {
                languages: ConvertLanguageKey[languageKey],
                ids: bookId,
            },
            data: {}
        });

        if (!res.data.results.length) {
            throw "Книга не найдена"
        }

        const bookLink = await this.saveBook(res.data.results[0]);

        if (!bookLink) {
            throw "Ошибка при загрзки книги"
        }

        return bookLink
    }

    static async saveBook(r: IGutenbergBook) {
        const dir = path.join(vars.filePath, 'books', String(r.id));
        console.log("SAVE DIR", dir)
        if (!fs.existsSync(dir)){
            fs.mkdirSync(dir, { recursive: true });
        }
        const resultFiles: string[] = [];
        if (!fs.existsSync(`${dir}/info.json`)) {
            const json = JSON.stringify(r);
            await fs.promises.writeFile(`${dir}/info.json`, json, 'utf8');
        }
        resultFiles.push(`${dir}/info.json`)
        const AllowedFormats = [
            'image/jpeg',
            'text/plain',
            'text/plain; charset=utf-8',
        ]
        if (!r.formats['image/jpeg']) {
            console.log("WITHOUT COVER", r.id, r.title, r.formats)
        }
    
        if (!r.formats['text/plain'] && !r.formats['text/plain; charset=utf-8']) {
            console.log("WITHOUT BOOK", r.id, r.title, r.formats)
        } else {
            if (r.formats['text/plain; charset=utf-8']) {
                //@ts-ignore
                delete r.formats['text/plain']
            }
        }
       
        await Promise.all(Object.entries(r.formats).map(async ([key, value]) => {
            if (!AllowedFormats.includes(key)) {
                return;
            }
            let fileName = value.split('/').pop();
            // console.log('FORMATS', key, fileName)
            if (key === 'image/jpeg') {
                fileName = 'cover.jpg'
            }
            if (key === 'text/plain' || key === 'text/plain; charset=utf-8') {
                fileName = 'book.txt';
            }
            if (fs.existsSync(`${dir}/${fileName}`)) {
                resultFiles.push(`${dir}/${fileName}`)
                return;
            }
            try {
                const fileDat = await sendHttpRequest({
                    url: value,
                    method: 'GET',
                    responseType: 'image/jpeg' === key ? 'stream' : undefined,
                    data: {}
                })
                let data = fileDat.data;
                if (key === 'image/jpeg') {
                    fileName = 'cover.jpg'
                }
                if (key === 'text/plain' || key === 'text/plain; charset=utf-8') {
                    // console.log("before computing...", `${dir}/${fileName}`)
                    fileName = 'book.txt';
        
                    // data = data.replace(/\*\*\* END OF THE PROJECT.*$/usmi, '')
                    //     .replace(/(.*)\*\*\* START OF THE PROJECT(.*)\*\*\*/usmi, '');
                    // console.log("computed", `${r.title}`)
                }
                if (!fs.existsSync(`${dir}/${fileName}`)) {
                    // console.log("creating...", `${dir}/${fileName}`)
                    if (key === 'image/jpeg') {
                        data.pipe(fs.createWriteStream(`${dir}/${fileName}`));
                        
                    } else {
                        // console.log("before computing...", r.title)
                        data = data.replace(/\*\*\* END OF THE PROJECT.*$/usmi, '')
                        .replace(/(.*)\*\*\* START OF THE PROJECT(.*)\*\*\*/usmi, '');
                        // console.log("computed", `${r.title}`)
                        await fs.promises.writeFile(`${dir}/${fileName}`, data);
                    }
                    
                } else {
                    // console.log('not created', fileName, r.title)
                }
                resultFiles.push(`${dir}/${fileName}`)
            } catch (error) {
                console.log("ONERRROR", error)
            }

            
        }))
        console.log("RESULT FILES", resultFiles)
        if (resultFiles.length) {
            const uploadRes = await saveBookOnS3(resultFiles, r.id);
            console.log("UPLOAD RES", uploadRes)
            return uploadRes.find(u => ~u.key.indexOf('.txt'))?.Location
        }
        return ''
    }

    static async getSearchByLang(languageKey: LanguageKeys, search: string = '', page: number = 1): Promise<IBooksResponse> {

        console.log('getSearchByLang', {
            languages: ConvertLanguageKey[languageKey],
            page,
            search,
        })
        const res: AxiosResponse<GutenbergApiResponse> = await sendHttpRequest({
            method: 'GET',
            url: `${GUTENBERG_ENDPOINT}`,
            params: {
                languages: ConvertLanguageKey[languageKey],
                page,
                search,
            },
            data: {}
        });
        const nextPage = res.data.next?.match(pageRegex);
        const prevPage = res.data.previos?.match(pageRegex);
        // console.log("RESULT", res.data)
        let currentPage = 1;
        if (nextPage) {
            currentPage = Number(nextPage[1]) - 1;
        } else if (prevPage) {
            currentPage = Number(prevPage[1]) + 1;
        }
        return {
            page: currentPage,
            count: res.data.count,
            next: !!res.data.next,
            previos: !!res.data.previos,
            results: res.data.results.map(bookToDto),
        }
    }
}
let computedCount = 0;

const start = async (url?: string) => {
    console.log("COMPUTED", computedCount)
    const res = await GutenbergBooksService.getBooks(url);
    console.log("RESSS", res.count)
    await handleResults(res.results);
    computedCount += res.results.length || 0;
    if (!res.next) {
        console.log("FINNNNNNNNNNNNNNN EEEE", computedCount)
    } else {
        start(res.next);
    }
}

const handleRes = async (r: IGutenbergBook) => {
    if (!r) return;
    const dir = `/home/lord/Документы/books/${r.id}`;
    // console.log('CHECK', r)
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir, { recursive: true });
    }
    if (!fs.existsSync(`${dir}/info.json`)) {
        const json = JSON.stringify(r);
        await fs.promises.writeFile(`${dir}/info.json`, json, 'utf8');
    }
    // const fileDat = await sendHttpRequest({
    //     url: r.formats['image/jpeg'],
    //     method: 'GET',
    //     data: {}
    // })
    // console.log("RESS", fileDat.data)
    // const text = fileDat.data
    //     .replace(/\*\*\* END OF THE PROJECT.*$/usmi, '')
    //     .replace(/(.*)\*\*\* START OF THE PROJECT(.*)\*\*\*/usmi, '');
    // console.log("DATA", text)
    const AllowedFormats = [
        'image/jpeg',
        'text/plain',
        'text/plain; charset=utf-8'
    ]
    if (!r.formats['image/jpeg']) {
        console.log("WITHOUT COVER", r.id, r.title, r.formats)
    }

    if (!r.formats['text/plain'] && !r.formats['text/plain; charset=utf-8']) {
        console.log("WITHOUT BOOK", r.id, r.title, r.formats)
    } else {
        if (r.formats['text/plain; charset=utf-8']) {
            //@ts-ignore
            delete r.formats['text/plain']
        }
    }
    Object.entries(r.formats).map(async ([key, value]) => {
        if (!AllowedFormats.includes(key)) {
            return;
        }
        let fileName = value.split('/').pop();
        // console.log('FORMATS', key, fileName)
        const fileDat = await sendHttpRequest({
            url: value,
            method: 'GET',
            responseType: 'image/jpeg' === key ? 'stream' : undefined,
            data: {}
        })
        let data = fileDat.data;
        if (key === 'image/jpeg') {
            fileName = 'cover.jpg'
        }
        if (key === 'text/plain' || key === 'text/plain; charset=utf-8') {
            // console.log("before computing...", `${dir}/${fileName}`)
            fileName = 'book.txt';

            // data = data.replace(/\*\*\* END OF THE PROJECT.*$/usmi, '')
            //     .replace(/(.*)\*\*\* START OF THE PROJECT(.*)\*\*\*/usmi, '');
            // console.log("computed", `${r.title}`)
        }
        if (!fs.existsSync(`${dir}/${fileName}`)) {
            // console.log("creating...", `${dir}/${fileName}`)
            if (key === 'image/jpeg') {
                data.pipe(fs.createWriteStream(`${dir}/${fileName}`));
            } else {
                // console.log("before computing...", r.title)
                data = data.replace(/\*\*\* END OF THE PROJECT.*$/usmi, '')
                .replace(/(.*)\*\*\* START OF THE PROJECT(.*)\*\*\*/usmi, '');
                // console.log("computed", `${r.title}`)
                await fs.promises.writeFile(`${dir}/${fileName}`, data);
            }
            
        } else {
            // console.log('not created', fileName, r.title)
        }

    })
}

const handleResults = async (results: IGutenbergBook[]) => {
    // handleRes(results[0])
    await Promise.all(results.map(handleRes))
}

// start()