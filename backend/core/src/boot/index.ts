import { runMigrations } from '../migrations';
import { startBroker } from './startBroker';
import startMongo from './startMongo';
import { startServer } from './startServer';
import { scheduler } from '../scheduler/scheduler';
import { startRedis } from './startRedis';
import { vars } from '../config/vars';
import { watchBackups } from '../utils/backups';

export const runBootTasks = async () => {
    try {
        await startMongo();
        await startRedis();
        await startBroker();
    
        if (vars.isMaster) {
            await runMigrations();
            scheduler.init();
            watchBackups();
        }
        await startServer();
        console.log('Boot succeed!');
    } catch (error) {
        console.log('run boot tasks error', error)
        process.exit();
    }
}