import { createClient } from 'redis';
import { vars } from '../config/vars';

export const redisClient = createClient({
    url: vars.redisUrl,
});

export const startRedis = async () => {
    await redisClient.connect();
}