import schedule, { Job, JobCallback } from "node-schedule";
import moment from "moment";

export class ScheduledTask {
    private rule: schedule.RecurrenceRule;
    private job: Job;
    constructor() {
        this.rule = new schedule.RecurrenceRule();
        this.job = null as unknown as Job;
    }

    setDate(timestamp: Date | number) {
        const date = moment(
            !Number.isNaN(timestamp) ? Number(timestamp) : Date.now()
        );
        this.rule = new schedule.RecurrenceRule();
        this.rule.year = date.year();
        this.rule.month = date.month();
        this.rule.date = date.date();
        this.rule.hour = date.hours();
        this.rule.minute = date.minutes();
        this.rule.second = date.second();
    }

    scheduleJob(job: JobCallback) {
        this.job = schedule.scheduleJob(this.rule, job);
    }

    rescheduleJob(timestamp: Date) {
        this.setDate(timestamp);
        schedule.rescheduleJob(this.job, timestamp);
    }

    cancelJob() {
        if (this.job) {
            this.job.cancel();
        }
    }
}
