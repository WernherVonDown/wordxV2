import { TaskTypes } from "../../const/scheduler/TASK_TYPES";
import { IScheduledTask } from "../../const/scheduler/types";
import { yandexS3Service } from "../../externalServices/yandexS3.service";
import { yookassaService } from "../../externalServices/yookassa.service";
import ScheduledTaskModel from "../../models/ScheduledTask.model";
import { billService } from "../../services/bill.service";
import { userService } from "../../services/user.service";

export const paySubscription = (task: IScheduledTask) => {
    return async function () {
        try {
            const user = await userService.getUserById(task.user);
            const bill = await billService.getBillById(task.data.billId);

            if (!user || !bill) {
                throw new Error(`user or bill not found ${user}, ${bill}`)
            }

            const payment = await yookassaService.repay(bill, await user.toDto());
            await billService.setPaymentId(bill._id, payment.id);

            console.log("EXECUTE TASK paySubscription", task, payment)
            await ScheduledTaskModel.deleteMany({
                type: TaskTypes.PAY_SUBSCRIPTION,
                isActive: true,
                user: task.user,
                data: {
                    billId: task.data.billId,
                }
            });
        } catch (error) {
            console.log("paySubscription error", error)
        }
    }
}