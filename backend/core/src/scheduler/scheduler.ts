import { TaskTypes } from "../const/scheduler/TASK_TYPES";
import ScheduledTaskModel, { IScheduledTaskDocument } from "../models/ScheduledTask.model";
import { ScheduledTask } from "./ScheduledTask";
import { paySubscription } from "./tasks/paySubscription";

class Scheduler {
    private tasks: any;
    constructor() {
        this.tasks = {};
    }

    async init() {
        process.on('message', this.handleMessage.bind(this));
        await this.restore();
    }

    handleMessage(data: any) {
        const task = data.data;
        if (task.isNewTask && data.topic === 'task') {
            this.planTask(task);
        } else {
            this.rescheduleTask(task);
        }
    }

    getJobForScheduledTask(task: any) {
        switch (task.type) {
            case TaskTypes.PAY_SUBSCRIPTION:
                return paySubscription(task);
            default:
                console.log({level: 'error', message: `Unknown scheduled task type ${task.type}`});
                return null;
        }
    }

    planTask(scheduleTask: IScheduledTaskDocument) {
        const job = this.getJobForScheduledTask(scheduleTask) as any;
        if (!job || !scheduleTask.isActive) {
            return null;
        }

        if (typeof scheduleTask.executedAt === "string")
            scheduleTask.executedAt = new Date(scheduleTask.executedAt);

        if (scheduleTask.executedAt.getTime() < Date.now()) {
            job();
            return;
        }

        const task = new ScheduledTask();
        task.setDate(scheduleTask.executedAt.getTime());
        task.scheduleJob(job);

        this.tasks[scheduleTask.id] = task;
        console.log("Scheduler.planTask;", scheduleTask);
    }

    async restore() {
        try {
            const tasks: IScheduledTaskDocument[] =
                (await ScheduledTaskModel.find({
                    isActive: true,
                })) as IScheduledTaskDocument[];

            const now = Date.now();

            await Promise.all(
                tasks.map((task: IScheduledTaskDocument) => {
                    const job = this.getJobForScheduledTask(task) as any;
                    if (task.executedAt.getTime() <= now) {
                        if (job) {
                            job();
                        }
                    } else {
                        this.planTask(task);
                    }
                })
            );
        } catch (err) {
            console.log({level: 'error', message: `Error; Scheduler.restore; ${err}`});
        }
    }

    rescheduleTask(scheduleTask: IScheduledTaskDocument) {
        this.cancelTask(scheduleTask);
        this.planTask(scheduleTask);
    }

    cancelTask(scheduleTask: IScheduledTaskDocument) {
        const task = this.tasks[scheduleTask.id];
        if (!task) {
            return null;
        }

        task.cancelJob();
        delete this.tasks[scheduleTask.id];

        console.log("SchedulerFactory.cancelTask;");
    }
}

export const scheduler = new Scheduler();
