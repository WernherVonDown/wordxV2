import express from 'express';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import router from '../router';
import { errorMiddleware } from '../middlewares/error.middleware';
import { vars } from './vars';
import bodyParser from 'body-parser';

const app = express();

app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({limit: '50mb', extended: true}));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(cors({
    credentials: true,
    origin: vars.clientUrl,
}));
app.use('/api', router);

app.use(errorMiddleware);

export { app };