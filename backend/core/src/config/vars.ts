import dotenv from 'dotenv';
import minimist from 'minimist';

dotenv.config();

const argv = minimist(process.argv.slice(2), {});

export const vars = Object.freeze({
    PORT: process.env.PORT || 5000,
    apiUrl: process.env.API_URL || 'http://localhost:8000',
    clientUrl: process.env.CLIENT_URL || 'http://localhost:3000',
    mongo: {
        url: process.env.MONGO_URL || 'mongodb://mongo:27017/database?replicaSet=rs0',
    },
    jwt: {
        accessSecret: process.env.JWT_ACCESS_SECRET || 'access_secret',
        refreshSecret: process.env.JWT_REFRESH_SECRET || 'refresh_secret',
    },
    email: {
        auth: {
            user: process.env.MAIL_USER,
            pass: process.env.MAIL_PASSWORD,
        },
    },
    openSubtitles: {
        apiKey: process.env.OPEN_SUBTITLES_API_KEY,
        username: process.env.OPEN_SUBTITLES_USERNAME,
        password: process.env.OPEN_SUBTITLES_PASSWORD,
    },
    yandex: {
        bucketName: process.env.YANDEX_S3_BUCKET_NAME || '',
        accessKeyId: process.env.YANDEX_S3_KEY_ID || '',
        secretAccessKey: process.env.YANDEX_S3_SECRET || '',
        dictKey: process.env.YANDEX_DICT_KEY,
    },
    yookassa: {
        secretKey: process.env.UKASSA_API_KEY || '',
        shopId: process.env.UKASSA_SHOP_ID || '',
    },
    rabbit: {
        user: process.env.RABBIT_USER || 'rabbitmq',
        pass: process.env.RABBIT_PASS || 'rabbitmq',
        host: process.env.RABBIT_HOST || 'localhost:5672',
    },
    filePath: '/tmp/',
    redisUrl: process.env.REDIS_URL || 'redis://localhost:7001/1',
    isMaster:  argv.isMaster,
    backupPath: process.env.BACKUP_PATH,
    envName: process.env.ENV_NAME,
    maxBackups: Number(process.env.MAX_BACKUPS || 5),
    genius: {
        token: process.env.GENIUS_TOKEN || "cKrztXVsFhGG2cJXjcwGasqC2RqVs8HII-Ex93iy2fRHGuWJcSdzukaf6NuEWX6_"
    }
})