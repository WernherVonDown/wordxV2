const { default: startMongo } = require("../boot/startMongo");
const { startRedis } = require("../boot/startRedis");
const { sourceService } = require("../services/source.service");
const { userService } = require("../services/user.service");
const { parentPort, workerData } = require('worker_threads')

const start = async ({ user, knownWords, learningWords, sourceId, needWords }) => {
    await startMongo()
    await startRedis()

    const words = await sourceService.getWordsIds(sourceId);
    
    const wordsForSorting = await userService.addWordsAfterSorting(
        user.id,
        words,
        knownWords,
        learningWords,
        sourceId,
        needWords,
    )
    const SortSourceWordsWithData = await Promise.all(wordsForSorting.map(async w => {
        const wordId = w.word;
        const word = await wordsService.findById(wordId);
        const wordDto = toDtoIfHas(word)
        return { id: wordId, word: wordDto, count: w.count };
    }));
    return SortSourceWordsWithData;
}

start(workerData).then(e => {
    parentPort.postMessage(e)
}).catch(error => parentPort.postMessage({ error }))
