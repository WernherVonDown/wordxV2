import { ControllerOpt } from "../../../types/broker";
import fs from 'fs';
import { wordsService } from "../../services/words.service";
import { sourceService } from "../../services/source.service";
import mongoose from "mongoose";
import { IParsedWord } from "../../const/words/interfaces";
import { LanguageKeys } from "../../const/language/Languagekeys";
import { SourceStatuses } from "../../const/source/SourceStatuses";
import { notificationsService } from "../../services/notificatons.service";
import { userService } from "../../services/user.service";
import { publishSendNotification } from "../socket/publisher";
import redisCache from "../../utils/redisCache";

interface IArgs extends ControllerOpt {
    payload: {
        words: IParsedWord[],
        uniqueCount: number,
        allCount: number,
        level: string,
        sourceId: string,
        languageId: string,
        languageKey: LanguageKeys,
        filePath: string,
    }
}

const parseFileResult = async ({ payload, data, channel }: IArgs) => {
    const {
        words: parsedWords,
        uniqueCount: parsedUnique,
        allCount: parssedAllCount,
        level,
        sourceId,
        languageId,
        languageKey,
        filePath,
    } = payload;
    try {
        console.log("controller.parseFileResult", payload);
        const { words, uniqueCount, allCount } = await wordsService.addParsedWords({
            words: parsedWords,
            languageId,
            languageKey,
        });

        const source = await sourceService.addWords({
            words,
            uniqueWordsCount: uniqueCount,
            totalWordsCount: allCount,
            level,
            sourceId,
            status: SourceStatuses.done,
        })

        console.log("ADDED SOURCE", source)
        if (source) {
            const notification = await notificationsService.create(
                `Субтитры "${source.title || source.titleOriginal}" обработались`,
                source.language.key
            );

            await userService.addNotification(source.user, notification._id);
            publishSendNotification({ userId: source.user, data: notification })
            redisCache.refresh(source.language.key)
        }

        fs.unlink(filePath, (err) => { if (err) console.log('error remove file', filePath) });
        channel.ack(data);
    } catch (error) {
        channel.ack(data);
        console.log("parseFileResult error", error)
    }
}

interface IArgsStatus extends ControllerOpt {
    payload: {
        sourceId: string,
        status: SourceStatuses,
        error?: string,
    }
}

const parseFileStatus = async ({ payload, data, channel }: IArgsStatus) => {
    const {
        sourceId,
        status,
        error,
    } = payload;
    try {
        const source = await sourceService.setWordsStatus(sourceId, status, error);
        console.log("SUBTITLES STATUS", source)
        if (source) {
            let msg = `Статус обработки субтитров "${source.title || source.titleOriginal}" изменился`
            if (status === SourceStatuses.computing) {
                msg = `Субтитры "${source.title || source.titleOriginal}" обрабатываются`
            } else if (status === SourceStatuses.error) {
                msg = `Ошибка при обработке "${source.title || source.titleOriginal}"`
            }
            const notification = await notificationsService.create(
                msg,
                source.language.key
            );

            await userService.addNotification(source.user, notification._id);
            publishSendNotification({ userId: source.user, data: notification })
        }

        channel.ack(data);
    } catch (error) {
        channel.ack(data);
        console.log("parseFileStatus error", error)
    }
}

export default {
    parseFileResult,
    parseFileStatus,
}