import { PARSER_EXCHANGE } from "../../config/brokerExchanges";
import { createPublisher } from "../../utils/broker/createPublisher";

export const publishParseFile = createPublisher({
    exchangeName: PARSER_EXCHANGE.name,
    queue: PARSER_EXCHANGE.queues.PARSE_FILE,
})
