import { PARSER_EXCHANGE } from "../../config/brokerExchanges";
import { createConsumer } from "../../utils/broker/createConsumer";
import controller from "./controller";

export const initParseFileResultConsumer = () => Promise.all([
    createConsumer(
        {
            queueName: PARSER_EXCHANGE.queues.PARSE_FILE_RESULT.name,
            prefetch: 1,
        },
        controller.parseFileResult,
    ),
    createConsumer(
        {
            queueName: PARSER_EXCHANGE.queues.PARSE_FILE_STATUS.name,
            prefetch: 100,
        },
        controller.parseFileStatus,
    )
]);
