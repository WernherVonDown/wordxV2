import { SOCKET_EXCHANGE } from "../../config/brokerExchanges";
import { createPublisher } from "../../utils/broker/createPublisher";

export const publishSendNotification = createPublisher({
    exchangeName: SOCKET_EXCHANGE.name,
    queue: SOCKET_EXCHANGE.queues.SEND_NOTIFICATION,
})