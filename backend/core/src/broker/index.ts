import { initParseFileResultConsumer } from "./parser/consumer";

export const initConsumers = async () => {
    console.log("broker:consumers:init:start");
    await initParseFileResultConsumer()
    console.log("broker:consumers:init:complete");
}