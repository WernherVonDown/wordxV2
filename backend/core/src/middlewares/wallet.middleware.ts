import { NextFunction, Request, Response } from "express";
import { publishSendNotification } from "../broker/socket/publisher";
import { notificationsService } from "../services/notificatons.service";
import productService from "../services/product.service";
import { userService } from "../services/user.service";
import { ApiError } from "../utils/errors/ApiError";

const walletValidate = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const {
            productId,
        } = req.query;
        console.log("WALLETTT 1", productId)
        const product = await productService.getProductById(productId as string);
        if(!product) {
            return next(ApiError.BadRequest('Продукт не найден'));
        }
        if (res.locals.user.wallet < product.price) {
            return next(ApiError.BadRequest('Недостаточно монет'));
        }

        res.locals.productPrice = product.price;
        next();
    } catch (error) {
        return next(ApiError.BadRequest('Ошибка при валидации кошелька'));
    }
}

const walletСapture = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { productPrice, user } = res.locals;
        await userService.captureCoins(user.id, productPrice);
        console.log("СПИСАНИЕ", productPrice, user.id)
        const notification = await notificationsService.create(
            `Списано ${productPrice} монет`, ''
        );

        await userService.addNotification(user.id, notification._id);
        publishSendNotification({userId: user.id, data: notification})
        next();
    } catch (error) {
        return next(ApiError.BadRequest('Ошибка списания'));
    }
}

export const wrapWalletMiddleware = (controller: any): any => [walletValidate, controller, walletСapture]
