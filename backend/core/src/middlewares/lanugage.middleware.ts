import { NextFunction, Request, Response } from "express";
import { ApiError } from "../utils/errors/ApiError";
import { languageService } from "../services/language.service";

export const languageMiddleware = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { languageKey } = req.params;
        if (!languageKey) {
            return next(ApiError.BadRequest('Нет языка в параметрах'));
        }

        const language = await languageService.getBykey(languageKey);

        if (!language) {
            return next(ApiError.BadRequest('Язык не найден'));
        }

        const languageDto = await language.toDto();

        res.locals.language = languageDto;
        next();
    } catch (error) {
        return next(ApiError.BadRequest('Язык не найден'));
    }
}