import { NextFunction, Request, Response } from "express";
import { ApiError } from "../utils/errors/ApiError";
import { languageService } from "../services/language.service";
import redisCache from "../utils/redisCache";
import _ from "lodash";


export const cacheMiddleware = (keyPath?: string, fromReq?: boolean) => {
    return async (req: Request, res: Response, next: NextFunction) => {
        try {
            let refreshKey;
            if (keyPath && _.get(fromReq ? req : res, keyPath)) {
                refreshKey = _.get(fromReq ? req : res, keyPath);
            }
            // console.log("EEE", req.method, req.path, req.originalUrl, refreshKey);
            if (req.method === 'GET') {
                const cachedData = await redisCache.get(req.originalUrl, refreshKey);
                if (cachedData) {
                    console.log("cached result", req.originalUrl)
                    return res.json(cachedData);
                }
            }
            next();
        } catch (error) {
            next();
            // return next(ApiError.BadRequest('Cache error'));
        }
    }
}