import { NextFunction, Request, Response } from "express";
import { ApiError } from "../utils/errors/ApiError";
import { sourceService } from "../services/source.service";

export const sourceMiddleware = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { sourceId } = req.params;
        if (!sourceId) {
            return next(ApiError.BadRequest('Нет id в параметрах'));
        }

        const source = await sourceService.getById(sourceId);

        if (!source) {
            return next(ApiError.BadRequest('Источник не найден'));
        }

        next();
    } catch (error) {
        return next(ApiError.BadRequest('Источник не найден, ошибка'));
    }
}