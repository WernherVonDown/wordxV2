import { IProduct } from "./interfaces";
import { ProductTypes } from "./ProductTypes";

export const Products: IProduct[] = [
    {
        title: 'Статисктика источника',
        key: ProductTypes.statistics,
        price: 5,
    },
    {
        title: 'Субтитры',
        key: ProductTypes.subtitles,
        price: 49,
    },
    {
        title: 'Книга',
        key: ProductTypes.books,
        price: 89,
    }
]
