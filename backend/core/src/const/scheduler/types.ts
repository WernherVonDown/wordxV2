import { TaskTypes } from "./TASK_TYPES";

interface IPaySubscriptionData {
    billId: string,
}

export interface IScheduledTask {
    type: TaskTypes;
    executedAt: Date;
    isActive: boolean;
    user: string;
    data: IPaySubscriptionData | any;
}
