import { toDtoIfHas } from "../../utils/mongo/toDtoIfHas";
import { IPopulatedUserWord } from "./interfaces";

export const userWordToDto = (word: IPopulatedUserWord | IPopulatedUserWord[]) => Array.isArray(word) ? 
word.map(w => ({
    count: w.count,
    sources: w.sources,
    word: toDtoIfHas(w.word),
    countBySource: w.countBySource,
}))
: ({
    count: word.count,
    sources: word.sources,
    word: toDtoIfHas(word.word),
    countBySource: word.countBySource,
})