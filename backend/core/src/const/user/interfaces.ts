import { ISource } from "../source/interfaces";
import { IWord } from "../words/interfaces";
import { Document } from "mongoose";

export interface IUserWord extends Document {
    word: string | IWord;
    count: number;
    sources: string[] | ISource[];
    countBySource?: any;
}

export interface IPopulatedUserWord extends Document {
    word: IWord;
    count: number;
    sources: string | ISource;
    countBySource:{ [key: string]: number }
}