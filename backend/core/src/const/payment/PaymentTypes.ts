export enum PaymentTypes {
    subscription = "subscription",
    wallet = 'wallet',
}
export interface IUserCard {
    first6: string;
    last4: string;
    expiry_month: string;
    expiry_year: string;
    card_type: string;
    issuer_country?: string;
    issuer_name?: string;
    source?: string;
}
