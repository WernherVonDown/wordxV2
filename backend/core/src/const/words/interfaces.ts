import { Document } from "mongoose";

export interface IWord extends Document {
    text: string,
    translation:string,
    language: string,
    globalCount: number,
    isRoot: boolean,
    root: string | IWord;
}

export interface IParsedWord {
    word: string,
    count: number,
    percent: number,
    percentStat?: string,
}
