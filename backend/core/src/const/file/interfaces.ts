export interface IMulterFile {
  fieldname: string;
  originalname: string;
  encoding: string;
  mimetype: string;
  destination: string;
  filename: string;
  path: string;
  size: number;
  s3Path?: string;
}


export interface IMulaterFiles {
  [fieldname: string]: IMulterFile[];
}