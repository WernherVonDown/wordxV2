export enum SourceStatuses {
    deafult = 'deafult',
    computing = 'computing',
    moderation = 'moderation',
    approved = 'approved',
    disapprove = 'disapproved',
    error = 'error',
    waiting = 'waiting',
    done = 'done',
}
