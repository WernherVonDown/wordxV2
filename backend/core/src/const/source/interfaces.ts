import { ILanguage } from "../language/interfaces";
import { IWord } from "../words/interfaces";

export interface ISourceWord {
    word: string | IWord;
    count: number;
    percent: number;
    percentStat?: string;
}

export interface ISourceType {
    key: string;
    label: string;
    id: string;
}

export interface ISourcePoster {
    id: string;
    url: string;
}

export interface ISource {
    id: string;
    language: ILanguage;
    poster: ISourcePoster;
    title: string;
    totalWordsCount: number;
    uniqueWordsCount: number;
    type: ISourceType;
}