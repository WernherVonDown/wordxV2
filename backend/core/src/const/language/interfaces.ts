import { LanguageKeys } from "./Languagekeys";

export interface ILangaugeRegister {
    key: LanguageKeys,
    levelKey: string;
}

export interface ILanguage {
    id: LanguageKeys;
    label: string;
    key: string;
}

export interface ILanguageLevel {
    label: string;
    id: string;
    key: string;
    description: string;
    levelCount: number;
}

export interface IUserLanguage {
    language: ILanguage;
    id: string;
    level: ILanguageLevel;
}
