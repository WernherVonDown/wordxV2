import NotificationModel from "../models/Notification.model";

class NotificationsService {
    async getNotifications (userId: string, languageId: string) {

    }

    async readNotification (userId: string, notificationId: string) {

    }

    async create(text: string, langKey: string) {
        const notification = await NotificationModel.create({
            text,
            langKey,
        });

        return notification.toDto();
    }
}

export const notificationsService = new NotificationsService();
