import { IUserCard, PaymentTypes } from "../const/payment/PaymentTypes";
import BillModel, { IBill } from "../models/Bill.model";

class BillService {
    async createBill({
        user, subscriptionPlan, amount, paymentType, rebill, description, paymentMethodId }:
        { user: string, subscriptionPlan?: string, amount: number, paymentType: PaymentTypes, rebill?: boolean, description: string, paymentMethodId?: string },
        paymentDetails?: {card: IUserCard, title: string}) {
        try {
            const bill = await BillModel.create({
                user,
                subscriptionPlan,
                amount,
                paymentType,
                rebill,
                description,
                paymentMethodId,
                paymentDetails,
            })
            return bill;
        } catch (error) {
            console.log('PaymentController.createBill error;', error);
        }
    }

    async setPaymentId(billId: string, paymentId: string) {
        await BillModel.findOneAndUpdate({
            _id: billId,
        }, {
            paymentId,
        })
    }

    async payAndRebill(billId: string, paymentMethodId: string, paymentDetails?: {card: IUserCard, title: string}) {
        const bill = await this.payBill(billId);
        if (bill) {
            const { user, subscriptionPlan, amount, paymentType, description } = bill;
            const rebill = await this.createBill({ user, subscriptionPlan, amount, paymentType, rebill: true, description, paymentMethodId }, paymentDetails);
            return rebill;
        }
    }

    async getUserSuccessPayments(user: string) {
        return BillModel.find({
            user,
            payd: true,
            rebill: true,
        }, {
            paymentMethodId: 1,
            paymentDetails: 1,
        })
    }

    async getBillById(billId: string) {
        return BillModel.findOne({ _id: billId });
    }

    async payBill(billId: string) {
        const bill = await BillModel.findOneAndUpdate({
            _id: billId,
        }, {
            paidAt: new Date().toString(),
            paid: true,
        }, {
            new: true,
        });

        return bill;
    }
}

export const billService = new BillService();
