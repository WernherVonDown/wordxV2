import path from "path";
import FileModel from "../models/File.model";

class FilesService {
    async create({ fileName, uploader, url, mimeType, localUrl }: { fileName: string, uploader: string, url: string, mimeType?: string, localUrl?: string }) {
        const extension = path.extname(fileName);
        const res = await FileModel.create({
            fileName,
            uploader,
            extension,
            url,
            mimeType,
            localUrl,
        })

        return res;
    }
}

export const filesService = new FilesService();