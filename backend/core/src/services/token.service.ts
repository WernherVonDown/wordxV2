import jwt from 'jsonwebtoken';
import { vars } from '../config/vars';
import tokenModel from '../models/Token.model';
import { IUserDto } from '../models/User.model';

class TokenService {
    generateTokens(payload: IUserDto) {
        const accessToken = jwt.sign(payload, vars.jwt.accessSecret, { expiresIn: '1d' });
        const refreshToken = jwt.sign(payload, vars.jwt.refreshSecret, { expiresIn: '60d' });

        return {
            accessToken,
            refreshToken,
        }
    }

    async getTokenByUser(userId: string) {
        const tokenData = await tokenModel.findOne({ user: userId });
        return tokenData?.refreshToken
    }

    validateAccessToken(token: string) {
        try {
            const userData = jwt.verify(token, vars.jwt.accessSecret);
            return userData as IUserDto;
        } catch (error) {
            return null;
        }
    }

    validateRefreshToken(token: string) {
        try {
            const userData = jwt.verify(token, vars.jwt.refreshSecret);
            return userData as IUserDto;
        } catch (error) {
            return null;
        }
    }

    async findToken(refreshToken: string) {
        const tokenData = await tokenModel.findOne({ refreshToken });
        return tokenData;
    }

    async saveToken(userId: string, refreshToken: string) {
        const tokenData = await tokenModel.findOne({ user: userId });

        if (tokenData) {
            tokenData.refreshToken = refreshToken;
            return tokenData.save();
        }

        const token = await tokenModel.create({ user: userId, refreshToken });
        return token;
    }

    async removeToken(refreshToken: string) {
        const tokenData = await tokenModel.deleteOne({ refreshToken });
        return tokenData;
    }
}

export const tokenService = new TokenService();