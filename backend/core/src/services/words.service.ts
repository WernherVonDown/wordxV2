import fs from "fs";
import { srtToText } from "../utils/convertors/srtToText";
import languageEncoding from "detect-file-encoding-and-language";
//@ts-ignore
import countWords from "count-words";
import WordModel from "../models/Word.model";
import { Document } from "mongoose";
import { getWordsAnalyser } from "../utils/wordsAnalysers";
import { LanguageKeys } from "../const/language/Languagekeys";
import { ApiError } from "../utils/errors/ApiError";
import { IParsedWord } from "../const/words/interfaces";


class WordsService {
    analysSrtFile = async (filePath: string, languageId: string, languageKey: LanguageKeys) => {
        // const file = fs.readFileSync(filePath, 'utf-8').normalize('NFKD').trim()
        const file = await new Promise((res, rej) => fs.readFile(filePath, 'utf8', (err, data) => {
            if (err) rej(err);
            res(data);
        })) as string;
        //@ts-ignore
        const detectLanguge = await languageEncoding(filePath);
        console.log("DETECT LANG", languageKey, detectLanguge)
        if (detectLanguge.language !== languageKey) {
            throw ApiError.BadRequest(`Неверный язык`)
        }

        const textFile = srtToText(file);
        
        // const { words, uniqueCount, allCount, level } = await wordsAnalyser.analysStr(textFile);
        
    }

    async addParsedWords(
        { words, languageId, languageKey }: 
        { words: IParsedWord[], languageId: string, languageKey: LanguageKeys}) {
        const wordsAnalyser = getWordsAnalyser(languageKey);
        const wordListItems = await Promise.all(words.map(async ({ word, count, percent, percentStat }: any) => {
            let wordExists = await WordModel.findOne({
                text: word,
                language: languageId,
            });
            let translation, text;
            if (!wordExists || !wordExists.isRoot) {
                const res = await wordsAnalyser.translate(word);
                translation = res?.tr;
                text = res?.text;
                if (text) {
                    wordExists = await WordModel.findOne({
                        text: text,
                        language: languageId,
                    });
                    if (wordExists) {
                        word = text;
                    }
                }
                
            }

            let wordDocument;
            if (!wordExists) {
                wordDocument = await WordModel.create({
                    text: word,
                    isRoot: !text && !!translation,
                    language: languageId,
                    globalCount: count,
                    translation: translation,
                })
            } else {
                wordDocument = await WordModel.findOneAndUpdate({
                    text: word,
                    language: languageId,
                }, {
                    text: word,
                    isRoot: !text,
                    $inc: {
                        globalCount: count,
                    },
                    language: languageId,
                }, {
                    upsert: true,
                    new: true,
                });
            }

            return {
                word: wordDocument._id,
                count,
                percent,
                percentStat,
            }

        }))

        const wordListItemsUnique = wordListItems.filter((value, index, self) => self.findIndex(v => v.word.toString() === value.word.toString()) === index)
        const allCount = wordListItemsUnique.reduce((prev, curr) => prev + curr.count, 0)
        return { words: wordListItemsUnique, uniqueCount: wordListItemsUnique.length, allCount };
    }
    

    analysStr = (text: string) => {
        const countedWordsObject = countWords(text, true);
        const wordsCounts = Object.values(countedWordsObject) as number[];
        const uniqueWordsCount = wordsCounts.length;
        const allWordsCount = wordsCounts.reduce((p: number, c: number) => p + c, 0)
        const result = Object.keys(countedWordsObject).map((w, i) => (
            {
                word: w,
                count: countedWordsObject[w],
                percent: countedWordsObject[w] / allWordsCount
            }
        ))
        // console.log('RESULT', { uniqueWordsCount, allWordsCount })
        return { words: result, uniqueCount: uniqueWordsCount, allCount: allWordsCount };
    }

    async findById(wordId: string) {
        return WordModel.findOne({_id: wordId});
    }
}

export const wordsService = new WordsService();