import LanguageLevelModel from "../models/LanguageLevel.model";

class LanguageLevelService {
    async getBykey(key: string) {
        return LanguageLevelModel.findOne({ key });
    }
}

export const languageLevelService = new LanguageLevelService();