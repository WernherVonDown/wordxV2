import LanguageModel from "../models/Language.model";
import LanguageLevelModel from "../models/LanguageLevel.model";
import ProductModel from "../models/Product.model";
import SubscriptionPlanModel from "../models/SubscriptionPlan.model";
import { toDtoIfHas } from "../utils/mongo/toDtoIfHas";

class DataService {
    async getSubscriptionPlans() {
        const res = await SubscriptionPlanModel.find({})

        return Promise.all(res.map(toDtoIfHas));
    }

    async getLanguages() {
        const res = await LanguageModel.find({})

        return Promise.all(res.map(toDtoIfHas));
    }

    async getLanguageLevels() {
        const res = await LanguageLevelModel.find({})

        return Promise.all(res.map(toDtoIfHas));
    }

    async getProducts() {
        const res = await ProductModel.find({})
        return res.map(toDtoIfHas)
    }
}

export const dataService = new DataService();