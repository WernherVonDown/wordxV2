import ProductModel from "../models/Product.model"

export default {
    async getProductById(_id: string) {
        return ProductModel.findOne({ _id })
    },
}
