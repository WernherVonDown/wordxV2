import { ILangaugeRegister } from "../const/language/interfaces";
import UserLanguageModel from "../models/UserLanguage.model";
import { ApiError } from "../utils/errors/ApiError";
import { languageService } from "./language.service";
import { languageLevelService } from "./languageLevel.service";

class UserLanguageService {
    async create(languageData: ILangaugeRegister) {
        const languageLevel = await languageLevelService.getBykey(languageData.levelKey);
        const language = await languageService.getBykey(languageData.key);

        if (!languageLevel) {
            throw ApiError.BadRequest(`Unknown language level ${languageData.levelKey}`)
        }

        if (!language) {
            throw ApiError.BadRequest(`Unknown language ${languageData.key}`)
        }

        const userLanguage = await UserLanguageModel.findOneAndUpdate(
            { language: language._id, level: languageLevel._id },
            { language: language._id, level: languageLevel._id },
            { upsert: true, new: true }
        );

        return userLanguage;
    }
}

export const userLanguageService = new UserLanguageService();