import UserModel from "../models/User.model";
import bcrypt from "bcrypt";
import { v4 } from "uuid";
import { mailService } from "./mail.service";
import { tokenService } from './token.service';
import { vars } from "../config/vars";
import path from "path";
import { ApiError } from "../utils/errors/ApiError";
import userModel from "../models/User.model";
import { ILangaugeRegister } from "../const/language/interfaces";
import UserLanguageModel from "../models/UserLanguage.model";
import { userLanguageService } from "./userLanguage.service";
import { ISourceWord } from "../const/source/interfaces";
import { IPopulatedUserWord, IUserWord } from "../const/user/interfaces";
import { toDtoIfHas } from "../utils/mongo/toDtoIfHas";
import { userWordToDto } from "../const/user/userWordToDto";
import { Types } from "mongoose";
import { stringToObjectId } from "../utils/mongo/stringToObjectId";
import UserWordSourceModel from "../models/UserWordSource.model";
import { genActivationCode } from "../utils/genActivationCode";
import SubscriptionPlanModel from "../models/SubscriptionPlan.model";
import LanguageModel from "../models/Language.model";
import redisCache from "../utils/redisCache";

const DEFAULT_WORDS_LIMIT = 10000;

class UserService {
    async registration(
        { email, password, userName, languages = [] }:
            { email: string, password: string, userName: string, languages: ILangaugeRegister[] }
    ) {
        const candidate = await UserModel.findOne({ email });
        if (candidate) {
            throw ApiError.BadRequest(`A user with email=${email} already exists`)
        }

        const candidateUserName = await UserModel.findOne({ userName });

        if (candidateUserName) {
            throw ApiError.BadRequest(`Имя пользователя уже занято`)
        }

        const hashedPassword = await bcrypt.hash(password, 3);

        const isTesting = email.split('@')[1] === 'test.ru'
        const activationCode = isTesting ? '0000' : genActivationCode();
        const resetPasswordLink = v4();
        const userLanguages = await Promise.all(languages.map(l => userLanguageService.create(l)));

        const user = await UserModel.create(
            { email, password: hashedPassword, resetPasswordLink, userName, languages: userLanguages.map(ul => ul._id), activationCode }
        );
        await mailService.sendActivationMail(
            email,
            activationCode
        );
        console.log("REGISTER", languages, userLanguages, { isTesting })
        const userDto = await this.getPopulatedUserDto(user._id);
        const tokens = tokenService.generateTokens(userDto);
        await tokenService.saveToken(userDto.id, tokens.refreshToken);

        return { ...tokens, user: userDto };
    }

    async resetPassword(email: string) {
        const candidate = await UserModel.findOne({ email });
        if (!candidate) {
            throw ApiError.BadRequest(`A user with email=${email} not exists`)
        }

        if (candidate.googleAccessToken) {
            throw ApiError.BadRequest(`A user with email=${email} uses google auth`)
        }

        await mailService.sendResetPasswordMail(
            email,
            vars.clientUrl + `/remind-password-confirm?token=${candidate.resetPasswordLink}&email=${email}`
        );
    }

    async resetPasswordConfirm(email: string, resetPasswordLink: string, password: string) {
        const candidate = await UserModel.findOne({ email, resetPasswordLink });
        if (!candidate) {
            throw ApiError.BadRequest(`Error`)
        }

        const hashedPassword = await bcrypt.hash(password, 3);

        candidate.resetPasswordLink = v4();
        candidate.password = hashedPassword;
        await candidate.save();
    }

    async activate(activationCode: string, email: string) {
        const user = await UserModel.findOne({ activationCode, email });
        if (!user) {
            throw ApiError.BadRequest("Некорректная ссылка активации");
        }

        user.isActivated = true;
        await user.save();
        const userDto = await this.getPopulatedUserDto(user._id);
        const tokens = tokenService.generateTokens(userDto);
        await tokenService.saveToken(userDto.id, tokens.refreshToken);

        return { ...tokens, user: userDto };
    }

    async removeUser(email: string) {
        await UserModel.deleteOne({ email })
    }

    // async activate(activationLink: string) {
    //     const user = await UserModel.findOne({ activationLink });
    //     if (!user) {
    //         throw ApiError.BadRequest("Некорректная ссылка активации");
    //     }

    //     user.isActivated = true;
    //     await user.save();
    // }

    async getPopulatedUserDto(_id: string) {
        const user = await userModel.findById(_id).populate({
            path: "languages",
            populate: [{
                path: 'language',
                model: 'Language',
                transform: (doc) => doc?.toDto ? doc?.toDto() : doc
            },
            {
                path: 'level',
                model: 'LanguageLevel',
                transform: (doc) => doc?.toDto ? doc?.toDto() : doc
            }
            ]
        }).populate({
            path: 'subscriptionPlan',
        }).populate({
            path: 'notifications'
        });
        const userDto = await user?.toDto();
        return userDto;
    }

    async getWordsIds(userId: string): Promise<{ learningWords: IUserWord[], learnedWords: IUserWord[], knownWords: IUserWord[], notSortedWords: IUserWord[] }> {
        const user = await UserModel.findOne({
            _id: userId,
        }).lean();

        return {
            learningWords: user?.learningWords || [],
            learnedWords: user?.learnedWords || [],
            knownWords: user?.knownWords || [],
            notSortedWords: user?.notSortedWords || [],
        }
    }

    async getSubscriptionPlan(subscriptionPlanId: string) {
        const subPlan = await SubscriptionPlanModel.findOne({ _id: subscriptionPlanId });
        return subPlan;
    }

    async changeSubscriptionPlan(userId: string, subscriptionPlanId: string) {
        const subPlan = await SubscriptionPlanModel.find({ _id: subscriptionPlanId });

        if (!subPlan) {
            throw ApiError.BadRequest("План не найден");
        }

        await UserModel.findOneAndUpdate({
            _id: userId,
        },
            {
                subscriptionPlan: subscriptionPlanId,
            });
    }

    async addLanguage(userId: string, languageId: string) {
        const language = await LanguageModel.findOne({ _id: languageId });
        if (!language) {
            throw ApiError.BadRequest("Язык не найден");
        }
        console.log("ADD", userId, languageId)

        const userLanguage = await UserLanguageModel.create({
            language: languageId,
        })

        await UserModel.findOneAndUpdate({
            _id: userId,
        },
            {
                $addToSet: { languages: userLanguage._id }
            }
        );
    }

    async removeLanguage(userId: string, languageId: string) {
        const language = await UserLanguageModel.findOne({ _id: languageId });
        if (!language) {
            throw ApiError.BadRequest("Язык не найден");
        }

        await UserModel.findOneAndUpdate({
            _id: userId,
        },
            {
                $pull: { languages: languageId }
            }
        );
    }

    async addWordsByWordsListKey(userId: string, words: ISourceWord[], sourceId: string, wordsListKey: string, countBySource?: boolean) {
        const user = await UserModel.findOne({
            _id: userId,
        })
        if (user) {
            if (!user[wordsListKey]) {
                user[wordsListKey] = []
            }
            const wordsMap = new Map();
            user[wordsListKey].forEach((w: IUserWord) => {
                wordsMap.set(w.word.toString(), w)
            })
            for (let i = 0; i < words.length; i++) {
                const currnetWord = words[i];
                const el = wordsMap.get(currnetWord.word.toString());//user[wordsListKey].find((e: IUserWord) => e.word.toString() === currnetWord.word.toString());
                if (el) {
                    el.sources = [...new Set([...el.sources, sourceId])];
                    el.count += currnetWord.count;
                    // console.log("COUNT BY SOURCE", el, el.countBySource, el.countBySource[sourceId])
                    if (!el.countBySource) {
                        el.countBySource = {
                            [sourceId]: currnetWord.count,
                        }
                    } else if (!el.countBySource[sourceId]) {
                        el.countBySource = {
                            ...el.countBySource,
                            [sourceId]: currnetWord.count,
                        }
                    }
                } else {
                    // console.log("ON NEW", {
                    //     word: currnetWord.word,
                    //     count: currnetWord.count,
                    //     sources: [sourceId],
                    //     countBySource: {
                    //         [sourceId]: currnetWord.count,
                    //     }
                    // })
                    user[wordsListKey].push({
                        word: currnetWord.word,
                        count: currnetWord.count,
                        sources: [sourceId],
                        countBySource: {
                            [sourceId]: currnetWord.count,
                        }
                    })
                }
            }
        }
        await user.save()
    }

    async addLearningWords(userId: string, sourceId: string, words: ISourceWord[]) {
        await this.addWordsByWordsListKey(userId, words, sourceId, 'learningWords');
    }

    async addNotSortedWords(userId: string, sourceId: string, words: ISourceWord[]) {
        await this.addWordsByWordsListKey(userId, words, sourceId, 'notSortedWords');
    }

    async removeFromNotSortedWords(userId: string, words: ISourceWord[]) {
        const user = await UserModel.findOne({
            _id: userId,
        }, {
            notSortedWords: 1
        })
        console.log("REMOVE SORTED", user.notSortedWords?.length, words.length)
        const wordsSet = new Set(words.map(w => w.word.toString()))
        if (user && user.notSortedWords?.length) {
            user.notSortedWords = user.notSortedWords.filter((w: any) => !wordsSet.has(w.word.toString()));
            console.log("NEW NOT SORTED", user.notSortedWords.length)
            await user.save()
        }

    }

    async addLearnedWords(userId: string, sourceId: string, words: ISourceWord[]) {
        await this.addWordsByWordsListKey(userId, words, sourceId, 'learnedWords');
    }

    async addKnownWords(userId: string, sourceId: string, words: ISourceWord[]) {
        await this.addWordsByWordsListKey(userId, words, sourceId, 'knownWords');
    }

    async addSortedSourceId(userId: string, sourceId: string, knownCount: number, removeNotSorted: boolean) {
        const wordSource = await UserWordSourceModel.findOneAndUpdate({
            source: sourceId,
            user: userId,
        }, {
            source: sourceId,
            knownCount,
            user: userId,
        }, {
            new: true,
            upsert: true,
        })
        // redisCache.refresh(userId.toString());
        await UserModel.findOneAndUpdate({ _id: userId }, {
            $addToSet: {
                wordSources: wordSource._id,
                sources: sourceId,
                notSourtedSources: sourceId,
            }
        })
        if (removeNotSorted) {
            await UserModel.findOneAndUpdate({ _id: userId }, {
                $pull: {
                    notSourtedSources: sourceId,
                }
            })
        }
    }

    async getUserById(userId: string) {
        return UserModel.findOne({
            _id: userId,
        })
    }

    async getUserSubscriptionPlan(userId: string) {
        const user = await UserModel.findOne({
            _id: userId,
        }).populate("subscriptionPlan")

        return user?.subscriptionPlan;
    }

    async getUserWordsSource(userId: string, languageId: string) {
        const user = await UserModel.findOne({
            _id: userId,
        }).populate({
            path: "wordSources",
            populate: [{
                path: 'source',
                model: 'Source',
            }],
        });

        if (!user) {
            throw ApiError.BadRequest("Пользователь не найден")
        }
        const userWordsSources = await Promise.all(
            user.wordSources
                .filter((s: any) => s.source?.language.toString() === languageId.toString())
                .map((s: any) => toDtoIfHas(s))
        )
        return userWordsSources;
    }

    async getUserWords(userId: string, languageId: string) {
        const user = await UserModel.findOne({
            _id: userId,
        }).populate({
            path: "learningWords.word learnedWords.word knownWords.word notSortedWords.word",
        });

        if (!user) {
            throw ApiError.BadRequest("Пользователь не найден")
        }

        const learningWords = user.learningWords.filter((w: IPopulatedUserWord) => w.word?.language.toString() === languageId.toString())
        const knownWordsCount = user.knownWords.filter((w: IPopulatedUserWord) => w.word?.language.toString() === languageId.toString()).length
        const learnedWordsCount = user.learnedWords.filter((w: IPopulatedUserWord) => w.word?.language.toString() === languageId.toString()).length;
        const notSortedWordsCount = user.notSortedWords.filter((w: IPopulatedUserWord) => w.word?.language.toString() === languageId.toString()).length;
        return { learningWords: userWordToDto(learningWords), knownWordsCount, learnedWordsCount, notSortedWordsCount }
    }

    async addWordsAfterSorting(
        userId: string,
        words: ISourceWord[],
        sortedKnownWords: ISourceWord[],
        sortedLearningWords: ISourceWord[],
        sourceId: string,
        needWords?: boolean,
        limit: number = DEFAULT_WORDS_LIMIT,
    ) {
        const sourceAdded = await UserModel.findOne({ _id: userId, sources: sourceId });
        const { learningWords, learnedWords, knownWords, notSortedWords: myNotSorted } = await userService.getWordsIds(userId);
        const sortedWords = sortedKnownWords.concat(sortedLearningWords);
        
        console.log("SORTED WORD", sortedWords.length, !!sourceAdded)
        if (!sourceAdded) {
            const learningWordsSet = new Set(learningWords.map(w =>  w.word.toString()))
            const learnedWordsSet = new Set(learnedWords.map(w =>  w.word.toString()))
            const knownWordsSet = new Set(knownWords.map(w =>  w.word.toString()))
            const myNotSortedSet = new Set(myNotSorted.map(w =>  w.word.toString()))

            const sortedLearningWordsSet = new Set(sortedLearningWords.map(w => w.word.toString()));
            const sortedKnownWordsSet = new Set(sortedKnownWords.map(w => w.word.toString()))

            const {
                existingLearningWords,
                existingLearnedWrods,
                existingKnownWords,
                existingNotSorted,
                notSortedWords,
            } = words.reduce((p, w) => {
                if (learningWordsSet.has(w.word.toString())) {
                    p.existingLearningWords.push(w)
                    learningWordsSet.delete(w.word.toString())
                    return p
                }
    
                if (learnedWordsSet.has(w.word.toString())) {
                    learnedWordsSet.delete(w.word.toString())
                    p.existingLearnedWrods.push(w)
    
                    return p
                } 
    
                if (knownWordsSet.has(w.word.toString())) {
                    knownWordsSet.delete(w.word.toString())
                    p.existingKnownWords.push(w);
                    return p
                } 
    
                if (myNotSortedSet.has(w.word.toString())) {
                    myNotSortedSet.delete(w.word.toString())
                    p.existingNotSorted.push(w)
                    // p.notSortedWords.push(w)
                    return p
                } 
                else {
                    if (!sortedLearningWordsSet.has(w.word.toString()) && !sortedKnownWordsSet.has(w.word.toString()))
                        p.notSortedWords.push(w)
                }
    
                return p;
            }, {
                existingLearningWords: [] as ISourceWord[],
                existingLearnedWrods: [] as ISourceWord[],
                existingKnownWords: [] as ISourceWord[],
                existingNotSorted: [] as ISourceWord[],
                notSortedWords: [] as ISourceWord[],
            })
            await this.addLearningWords(userId, sourceId, existingLearningWords.concat(sortedLearningWords))
            
            await this.addLearnedWords(userId, sourceId, existingLearnedWrods)
            
            await this.addKnownWords(userId, sourceId, existingKnownWords.concat(sortedKnownWords));
            await this.addNotSortedWords(userId, sourceId, notSortedWords.concat(existingNotSorted));
            await this.addSortedSourceId(userId, sourceId, existingKnownWords.concat(sortedKnownWords).concat(existingLearnedWrods).length, notSortedWords.concat(existingNotSorted).length === 0);

            await this.removeFromNotSortedWords(userId, sortedWords)


///////////////
// const existingLearningWords = words.filter(w => learningWords.find(lw => w.word.toString() === lw.word.toString()));
            // const existingLearnedWrods = words.filter(w => learnedWords.find(lw => w.word.toString() === lw.word.toString()));
            // const existingKnownWords = words.filter(w => knownWords.find(lw => w.word.toString() === lw.word.toString()));
            // const existingNotSorted = words.filter(w => myNotSorted.find(lw => w.word.toString() === lw.word.toString()));
            // const allExistingWords = [...existingLearningWords.concat(sortedLearningWords), ...existingLearnedWrods, ...existingKnownWords.concat(sortedKnownWords), ...existingNotSorted]
            // const notSortedWords = words.filter(w => !allExistingWords.find(lw => lw.word.toString() === w.word.toString()));

            // await this.addLearningWords(userId, sourceId, existingLearningWords.concat(sortedLearningWords))
            
            // await this.addLearnedWords(userId, sourceId, existingLearnedWrods)
            
            // await this.addKnownWords(userId, sourceId, existingKnownWords.concat(sortedKnownWords));
            // await this.addNotSortedWords(userId, sourceId, notSortedWords.concat(existingNotSorted));
            // await this.addSortedSourceId(userId, sourceId, existingKnownWords.concat(sortedKnownWords).length, notSortedWords.concat(existingNotSorted).length === 0);

            // await this.removeFromNotSortedWords(userId, sortedWords)
            if (needWords) {
                return notSortedWords.concat(existingNotSorted).sort((w1, w2) => w2.count - w1.count).slice(0, limit);
            } else {
                return []
            }

        } else {
            const knownWordsSet = new Set(knownWords.map(w =>  w.word.toString()))
            const myNotSortedSet = new Set(myNotSorted.map(w =>  w.word.toString()))

            // words = words.filter(w => myNotSorted.find(lw => w.word.toString() === lw.word.toString()));
            const sortedWordsSet = new Set(sortedWords.map(w =>  w.word.toString()))
            
            const existingKnownWords = words.filter(w => knownWordsSet.has(w.word.toString()));
            
            const notSortedAfterRemove = myNotSorted.filter(w => !sortedWordsSet.has(w.word.toString()));
            const existingNotSorted = notSortedAfterRemove.filter(w => myNotSortedSet.has(w.word.toString()));
            const existingNotSortedSet = new Set(existingNotSorted.map(w =>  w.word.toString()))
            await this.addKnownWords(userId, sourceId, sortedKnownWords);
            await this.addLearningWords(userId, sourceId, sortedLearningWords)
            await this.addSortedSourceId(userId, sourceId, existingKnownWords.concat(sortedKnownWords).length, existingNotSorted.length === 0);

            await this.removeFromNotSortedWords(userId, sortedWords)
            if (needWords) {
                const res = words.filter(w => existingNotSortedSet.has(w.word.toString()))
                return res
                    .sort((w1, w2) => w2.count - w1.count).slice(0, limit);
            } else {
                return [];
            }

        }

        // const notSortedWords = await this.getNotSortedWords(userId, sourceId);
        // return notSortedWords.slice(0, limit);
    }

    async getNotSortedWords(userId: string, sourceId: string): Promise<IUserWord[]> {
        const resUser = await UserModel.findOne({ _id: userId, "notSortedWords.sources": sourceId }, { notSortedWords: 1 }).lean()
        return resUser.notSortedWords || []
    }

    async getWordsForSorting(userId: string, words: ISourceWord[], sourceId: string, limit: number = DEFAULT_WORDS_LIMIT) {
        const sourceAdded = await UserModel.findOne({ _id: userId, notSourtedSources: sourceId });
        const { learningWords, learnedWords, knownWords, notSortedWords: myNotSorted } = await userService.getWordsIds(userId);
        const learningWordsSet = new Set(learningWords.map(w =>  w.word.toString()))
        const learnedWordsSet = new Set(learnedWords.map(w =>  w.word.toString()))
        const knownWordsSet = new Set(knownWords.map(w =>  w.word.toString()))
        const myNotSortedSet = new Set(myNotSorted.map(w =>  w.word.toString()))

        // words = words.sort((w1, w2) => w2.count - w1.count).slice(0, limit)

        if (sourceAdded) {
            // console.log("EEE 1", words.filter(w => myNotSortedSet.has(w.word.toString())).length)
            return words.filter(w => myNotSortedSet.has(w.word.toString())).sort((w1, w2) => w2.count - w1.count).slice(0, limit);
        }

        // const existingLearningWords = words.filter(w => learningWords.find(lw => w.word.toString() === lw.word.toString()));
        // const existingLearnedWrods = words.filter(w => learnedWords.find(lw => w.word.toString() === lw.word.toString()));
        // const existingKnownWords = words.filter(w => knownWords.find(lw => w.word.toString() === lw.word.toString()));
        // const existingNotSorted = words.filter(w => myNotSorted.find(lw => w.word.toString() === lw.word.toString()));
        // const allExistingWords = [...existingLearningWords, ...existingLearnedWrods, ...existingKnownWords, ...existingNotSorted]
        // const notSortedWords = words.filter(w => !allExistingWords.find(lw => lw.word.toString() === w.word.toString()));
        const {
            notSortedWords,
        } = words.reduce((p, w) => {
            if (learningWordsSet.has(w.word.toString()) || learnedWordsSet.has(w.word.toString()) || knownWordsSet.has(w.word.toString())) {
                return p
            }
            p.notSortedWords.push(w)
            return p
        }, {
            notSortedWords: [] as ISourceWord[],
        })
        console.log("EEE", notSortedWords.length)
        return notSortedWords.sort((w1, w2) => w2.count - w1.count).slice(0, limit);
    }

    async getWordsStatistics(userId: string, words: ISourceWord[]): Promise<{ notSortedWords: ISourceWord[], learningWords: ISourceWord[], knownWords: ISourceWord[], existingNotSorted: ISourceWord[] }> {
        const { learningWords, learnedWords, knownWords, notSortedWords: myNotSorted } = await userService.getWordsIds(userId);
        const learningWordsSet = new Set(learningWords.map(w =>  w.word.toString()))
        const learnedWordsSet = new Set(learnedWords.map(w =>  w.word.toString()))
        const knownWordsSet = new Set(knownWords.map(w =>  w.word.toString()))
        const myNotSortedSet = new Set(myNotSorted.map(w =>  w.word.toString()))

        const {
            existingLearningWords,
            existingLearnedWrods,
            existingKnownWords,
            existingNotSorted,
            notSortedWords,
            allWords,
            allLearningWords,
            allNotSortedWords,
            allSortingWords,
        } = words.reduce((p, w) => {
            p.allWords += w.count;
            if (learningWordsSet.has(w.word.toString())) {
                p.existingLearningWords.push(w)
                p.allLearningWords += w.count
                learningWordsSet.delete(w.word.toString())
                return p
            }

            if (learnedWordsSet.has(w.word.toString())) {
                learnedWordsSet.delete(w.word.toString())
                p.existingLearnedWrods.push(w)

                return p
            } 

            if (knownWordsSet.has(w.word.toString())) {
                knownWordsSet.delete(w.word.toString())
                p.existingKnownWords.push(w);
                return p
            } 

            if (myNotSortedSet.has(w.word.toString())) {
                myNotSortedSet.delete(w.word.toString())
                p.existingNotSorted.push(w)
                p.allNotSortedWords += w.count;
                p.allSortingWords += w.count;
                p.notSortedWords.push(w)
                return p
            } 
            else {
                p.allNotSortedWords += w.count;
                p.notSortedWords.push(w)
            }

            return p;
        }, {
            existingLearningWords: [] as ISourceWord[],
            existingLearnedWrods: [] as ISourceWord[],
            existingKnownWords: [] as ISourceWord[],
            existingNotSorted: [] as ISourceWord[],
            notSortedWords: [] as ISourceWord[],
            allWords: 0,
            allLearningWords: 0,
            allNotSortedWords: 0,
            allSortingWords: 0,
        })

        const knownWordsAll = existingKnownWords.concat(existingLearnedWrods)
        const allKnownWords = knownWordsAll.reduce((p, c) => c.count + p, 0)
        const allSortingPercent = allSortingWords / allWords;
        const allKnownWordsPercent = allKnownWords / allWords;
        const allLearningWordsPercent = allLearningWords / allWords;
        const allNotSortedWordsPercent = allNotSortedWords / allWords
        console.log('STATISTICS', { allKnownWordsPercent, allLearningWordsPercent, allNotSortedWordsPercent, allSortingPercent }, allKnownWordsPercent + allLearningWordsPercent + allNotSortedWordsPercent, allSortingPercent)
        //@ts-ignore
        return ({ sortingPercent: existingNotSorted.length / words.length, knownPercent: knownWordsAll.length / words.length, learningPercent: existingLearningWords.length / words.length, notSortedPercent: notSortedWords.length / words.length, allKnownWordsPercent, allLearningWordsPercent, allNotSortedWordsPercent, allSortingPercent });
    }
    // STATISTICS {
    //     allKnownWordsPercent: 0.8129326396018585,
    //     allLearningWordsPercent: 0.005035933666278038,
    //     allNotSortedWordsPercent: 0.1820314267318635,
    //     allSortingPercent: 0.03369934049106444
    //   }

    // STATISTICS {
    //     allKnownWordsPercent: 0.7597508135103624,                                
    //     allLearningWordsPercent: 0.005035933666278038,
    //     allNotSortedWordsPercent: 0.1820314267318635,
    //     allSortingPercent: 0.03369934049106444
    //   } 0.946818173908504 0.03369934049106444
    // STATISTICS {
    //     allKnownWordsPercent: 0.7576781977016601,
    //     allLearningWordsPercent: 0.0050430201200190195,
    //     allNotSortedWordsPercent: 0.17648140885253072,
    //     allSortingPercent: 0.03387812759222688
    //   }
    async addNotification(userId: string, notificationId: string) {
        await UserModel.findOneAndUpdate({
            _id: userId,
        }, {
            $addToSet: {
                notifications: notificationId
            }
        })
        // redisCache.refresh(userId.toString());
    }

    async increaseConins(userId: string, amount: number) {
        await UserModel.findOneAndUpdate({
            _id: userId,
        }, {
            $inc: {
                wallet: amount
            }
        })
        // redisCache.refresh(userId.toString());
    }

    async captureCoins(userId: string, amount: number) {
        await UserModel.findOneAndUpdate({
            _id: userId,
        }, {
            $inc: {
                wallet: -amount
            }
        })
        // redisCache.refresh(userId.toString());
    }

    async removeNotification(userId: string, notificationId: string) {
        await UserModel.findOneAndUpdate({
            _id: userId,
        }, {
            $pull: {
                notifications: notificationId
            }
        })
    }

    async changeLearnedWords(userId: string, learnedWordsIds: string[]) {
        const user = await UserModel.findOne({
            _id: userId,
        }).populate({
            path: 'wordSources',
        })
        const learnedWordsIdsSet = new Set(learnedWordsIds.map(w => w.toString()))
        // const learningWordsNew = user.learningWords.filter(
        //     (w: IUserWord) => !learnedWordsIdsSet.has(w.word.toString())
        // )

        // const learnedWordsNew = user.learningWords.filter(
        //     (w: IUserWord) => learnedWordsIdsSet.has(w.word.toString())
        // )
        const {
            learningWordsNew, 
            learnedWordsNew,
        } = user.learningWords.reduce((acc:any, w: IUserWord) => {
            if (!learnedWordsIdsSet.has(w.word.toString())) {
                acc.learningWordsNew.push(w)
            } else {
                acc.learnedWordsNew.push(w);
            }
            return acc;
        }, {
            learningWordsNew: [], 
            learnedWordsNew: [],
        })
        for (const lw of learnedWordsNew) {
            const { sources }: IUserWord = lw;
            await Promise.all(sources.map(async sId => {
                const userSource = user.wordSources.find((s: any) => s.source.toString() === sId.toString());
                if (userSource?.knownCount) {
                    userSource.knownCount += 1;
                    await userSource?.save()
                }
            }))
        }
        user.learningWords = learningWordsNew;
        user.learnedWords = user.learnedWords.concat(learnedWordsNew);
        await user.save()
    }

    async googleAuth(email: string, accessToken: string) {
        let user = await UserModel.findOne({ email })
        if (!user) {
            const resetPasswordLink = v4();
            user = await UserModel.create({ email, password: '', activationLink: '', resetPasswordLink, googleAccessToken: accessToken, isActivated: true, userName: email });
        } else {
            if (!user.googleAccessToken) {
                throw ApiError.BadRequest('This email is already used')
            }
        }

        const userDto = await this.getPopulatedUserDto(user._id);
        const tokens = tokenService.generateTokens(userDto);
        await tokenService.saveToken(userDto.id, tokens.refreshToken);

        return { ...tokens, user: userDto };
    }

    async login(email: string, password: string) {
        const user = await UserModel.findOne({ email })
        if (!user) {
            throw ApiError.BadRequest('User with this email not found')
        }

        if (!user.isActivated) {
            throw ApiError.BadRequest('Подтвердите почту')
        }

        const isPassEquals = await bcrypt.compare(password, user.password);

        if (!isPassEquals) {
            throw ApiError.BadRequest('Password is incorrect')
        }

        const userDto = await this.getPopulatedUserDto(user._id);
        const tokens = tokenService.generateTokens(userDto);
        await tokenService.saveToken(userDto.id, tokens.refreshToken);

        return { ...tokens, user: userDto };
    }

    async logout(refreshToken: string) {
        const token = await tokenService.removeToken(refreshToken);
        return token;
    }

    async refresh(refreshToken: string) {
        if (!refreshToken) {
            throw ApiError.UnauthorizedError();
        }
        const userData = tokenService.validateRefreshToken(refreshToken);
        const tokenFromDb = await tokenService.findToken(refreshToken);
        if (!userData || !tokenFromDb) {
            throw ApiError.UnauthorizedError();
        }

        const userDto = await this.getPopulatedUserDto(userData.id);
        const tokens = tokenService.generateTokens(userDto);
        await tokenService.saveToken(userDto.id, tokens.refreshToken);

        return { ...tokens, user: userDto };
    }
}

export const userService = new UserService();