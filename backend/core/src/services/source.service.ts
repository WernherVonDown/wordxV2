import path from "path";
import { IMulaterFiles } from "../const/file/interfaces";
import { SourceDtoFormats } from "../const/source/SourceDtoFormats";
import SourceModel from "../models/Source.model";
import { toDtoIfHas } from "../utils/mongo/toDtoIfHas";
import { yandexS3Service } from "../externalServices/yandexS3.service";
import { SourceStatuses } from "../const/source/SourceStatuses";
import redisCache from "../utils/redisCache";

const GET_SOURCES_LIMIT = 20;
class SourceService {
    async create(
        { title, words = [], description, poster, sourceFile, language, type, user, level, totalWordsCount = 0, uniqueWordsCount = 0,
            imdbId,
            kinopoiskId,
            titleOriginal,
            posterUrl,
            status,
            bookId,
            songId,
            author,
            youtubeId,
        }:
            {
                title: string, description?: string, words?: any[], poster?: string | null, sourceFile: string, language: string, type: string, user: string, level?: string, totalWordsCount?: number, uniqueWordsCount?: number,
                imdbId?: string,
                kinopoiskId?: string,
                titleOriginal?: string,
                posterUrl?: string,
                status: SourceStatuses,
                bookId?: string,
                songId?: string,
                author?: string,
                youtubeId?: string
            }
    ) {
        const res = await SourceModel.create(
            {
                title,
                description,
                poster,
                sourceFile,
                language,
                type,
                user,
                level,
                totalWordsCount,
                uniqueWordsCount,
                words,
                imdbId,
                kinopoiskId,
                titleOriginal,
                posterUrl,
                status,
                bookId,
                songId,
                author,
                youtubeId,
            }
        )

        return res;
    }

    async addWords(
        { sourceId, words, level, totalWordsCount, uniqueWordsCount, status }:
            { sourceId: string, words: any[], level?: string, totalWordsCount: number, uniqueWordsCount: number, status?: string }
    ) {
        return await SourceModel.findOneAndUpdate({
            _id: sourceId,
        }, {
            words,
            level,
            totalWordsCount,
            uniqueWordsCount,
            status,
        }, {
            new: true,
        }).populate('language')
    }

    async setWordsStatus(sourceId: string, status: SourceStatuses, error?: string) {
        const pushError = error ? { $push: { computeErrors: error } } : {}

        return await SourceModel.findOneAndUpdate({
            _id: sourceId,
        }, {
            status,
            ...pushError,
        }, {
            new: true,
        }).populate('language')
    }

    async getByLangShort(language: string, page = 0) {
        return SourceModel.find(
            { language, status: SourceStatuses.done, },
            null,
            { sort: { updatedAt: -1 } }
        )
            .skip(page * GET_SOURCES_LIMIT)
            .limit(GET_SOURCES_LIMIT)
            .populate({
                path: "poster sourceFile language type",
            })
            .then(sources => sources.map(s => toDtoIfHas(s, SourceDtoFormats.short)))
    }

    async getByLang(language: string) {
        return SourceModel.find({ language }).populate({
            select: "poster sourceFile language type",
            path: 'words',
            populate: [{
                path: "word",
                model: "Word",
                transform: toDtoIfHas,
            }]
        }).then(sources => sources.map(s => toDtoIfHas(s)))
    }

    async uploadSourceS3(files: IMulaterFiles, bucketPath: string): Promise<IMulaterFiles> {
        if (files) {
            for (const fileArr in files) {
                files[fileArr] = await Promise.all(files[fileArr].map(async file => {
                    try {
                        const { path, filename, mimetype } = file;
                        const res = await yandexS3Service.upload(path, bucketPath);
                        if (res) {
                            file.s3Path = res.Location;
                        }
                        return file;
                    } catch (error) {
                        return file
                    }
                }))
            }

        }
        return files;
    }

    async getWordsIds(sourceId: string) {
        const source = await SourceModel.findOne({ _id: sourceId })//.lean()
        const res = source?.toJSON()?.words || [];
        return res;
    }

    async getById(sourceId: string) {
        return SourceModel.findOne({ _id: sourceId });
    }

    async getSourcesForComputing() {
        return SourceModel.find({
            status: {
                $in: [
                    SourceStatuses.approved,
                    SourceStatuses.computing,
                    SourceStatuses.waiting
                ]
            }
        }).populate("sourceFile language")
    }
}

export const sourceService = new SourceService();