import LanguageModel from "../models/Language.model";

class LanguageService {
    async getBykey(key: string) {
        return LanguageModel.findOne({ key });
    }
}

export const languageService = new LanguageService();