import mongoose from "mongoose";
import { getMongoConnection } from "../boot/startMongo";

export const getSourceModel = () => {
    return getMongoConnection()?.db.collection("sources") as any
};
