import mongoose from "mongoose";
import { getMongoConnection } from "../boot/startMongo";

export const getWordModel = () => {

    return getMongoConnection()?.db.collection("words") as any
};
