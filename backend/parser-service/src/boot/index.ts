import { startBroker } from "./startBroker"
import startMongo from "./startMongo";

export const runBootTasks = async () => {
    await startMongo();
    await startBroker()
}
