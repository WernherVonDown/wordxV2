import { ControllerOpt } from "../../../types/broker";
import fs from 'fs';
import { wordsService } from "../../services/words.service";
import { LanguageKeys } from "../../const/language/LanguageKeys";
import { sourceService } from "../../services/source.service";
import mongoose from "mongoose";
import { publishParseFileResult, publishParseFileStatus } from "./publisher";
import { SourceStatuses } from "../../const/SourceStatuses";
//@ts-ignore
import createTaskQueue from "sync-task-queue";

const MAX_TASKS_SET = 300;
const tasksSet = new Set();
const taskQueue = createTaskQueue();

interface IArgs extends ControllerOpt {
    payload: {
        filePath: string,
        languageKey: LanguageKeys,
        sourceId: string,
        languageId: string,
    }
}

const onParse = async ({ filePath, languageKey, sourceId, languageId }: {
    filePath: string,
    languageKey: LanguageKeys,
    sourceId: string,
    languageId: string,
}) => {
    try {
        await publishParseFileStatus({
            sourceId,
            status: SourceStatuses.computing,
        })
        console.log("controller.parseFile start", { filePath, languageKey, sourceId, languageId });
        console.time('Парсинг');
        const { words, uniqueCount, allCount, level } = await wordsService.analysSrtFile(filePath, languageKey)
        console.timeEnd('Парсинг');
        console.log("DONE", sourceId, words.length, uniqueCount, allCount, level)
        await publishParseFileResult({ words, uniqueCount, allCount, level, sourceId, languageKey, languageId, filePath })
    } catch (error: any) {
        console.log("ERROR", error)
        tasksSet.delete(sourceId);
        publishParseFileStatus({
            sourceId,
            status: SourceStatuses.error,
            error: error?.message || 'unknown error',
        })
    }
}

const parseFile = async ({ payload, data, channel }: IArgs) => {
    const { filePath, languageKey, sourceId, languageId
    } = payload;

    //Терминатор 2: Судный день ЛОМАЕТ НАХУЙ
    console.log("controller.parseFile", payload);
    if (!fs.existsSync(filePath)) {
        console.log("File not found", payload)
        tasksSet.delete(sourceId);
        await publishParseFileStatus({
            sourceId,
            status: SourceStatuses.error,
            error: "file not found",
        })
        return channel.ack(data);
    }
    if (!tasksSet.has(sourceId)) {
        taskQueue.enqueue(() => onParse(payload));

        tasksSet.add(sourceId);
        if (tasksSet.size >= MAX_TASKS_SET) {
            const [first] = tasksSet;
            tasksSet.delete(first);
        }
        
    } else {
        console.log('duplicate task in queque',payload)
    }

    channel.ack(data);
}

export default {
    parseFile,
}