import { PARSER_EXCHANGE } from "../../config/brokerExchanges";
import { createPublisher } from "../../utils/broker/createPublisher";

export const publishParseFileResult = createPublisher({
    exchangeName: PARSER_EXCHANGE.name,
    queue: PARSER_EXCHANGE.queues.PARSE_FILE_RESULT,
})

export const publishParseFileStatus = createPublisher({
    exchangeName: PARSER_EXCHANGE.name,
    queue: PARSER_EXCHANGE.queues.PARSE_FILE_STATUS,
})