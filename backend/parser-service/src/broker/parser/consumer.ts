import { PARSER_EXCHANGE } from "../../config/brokerExchanges";
import { createConsumer } from "../../utils/broker/createConsumer";
import controller from "./controller";

export const initParseFileConsumer = () => Promise.all([
    createConsumer(
        {
            queueName: PARSER_EXCHANGE.queues.PARSE_FILE.name,
            prefetch: 100,
        },
        controller.parseFile,
    )
]);
