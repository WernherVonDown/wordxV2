import { initParseFileConsumer } from "./parser/consumer";

export const initConsumers = async () => {
    console.log("broker:consumers:init:start");
    await initParseFileConsumer()
    console.log("broker:consumers:init:complete");
}