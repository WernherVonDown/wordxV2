import { runBootTasks } from "./boot";

async function start() {
    await runBootTasks();
}

start().catch((e) => {
    console.log("error boot", e);
    process.exit();
})