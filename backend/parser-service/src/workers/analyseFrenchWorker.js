const { workerData, parentPort } = require("worker_threads");
const analyseFrench = require("../utils/wordsAnalysers/analyseFrench");

const res = analyseFrench(workerData)
parentPort?.postMessage(res)

