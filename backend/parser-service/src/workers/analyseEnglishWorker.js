const { workerData, parentPort } = require("worker_threads");
const analyseEnglish = require("../utils/wordsAnalysers/analyseEnglish");

const res = analyseEnglish(workerData)
parentPort?.postMessage(res)

