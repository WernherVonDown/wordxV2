export interface ILanguagePhrase {
    regExp: RegExp,
    phrase: string,
    exclude?: boolean,
}
