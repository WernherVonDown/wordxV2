export enum LanguageKeys {
    english = 'english',
    german = 'german',
    french = 'french',
}
