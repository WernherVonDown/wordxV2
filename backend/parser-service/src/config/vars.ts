import dotenv from 'dotenv';

dotenv.config();

export const vars = {
    rabbit: {
        user: process.env.RABBIT_USER || 'rabbitmq',
        pass: process.env.RABBIT_PASS || 'rabbitmq',
        host: process.env.RABBIT_HOST || 'localhost:5672',
    },
    mongo: {
        url: process.env.MONGO_URL || 'mongodb://mongo:27017/database?replicaSet=rs0',
    },
    yandex: {
        bucketName: process.env.YANDEX_S3_BUCKET_NAME || '',
        accessKeyId: process.env.YANDEX_S3_KEY_ID || '',
        secretAccessKey: process.env.YANDEX_S3_SECRET || '',
        dictKey: process.env.YANDEX_DICT_KEY,
    },
}
