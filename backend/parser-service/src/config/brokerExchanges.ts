import { IExchange } from "../../types/broker";

export const PARSER_EXCHANGE = {
    name: 'parser',
    type: 'direct',
    options: {
        durable: true,
    },
    queues: {
        PARSE_FILE: {
            name: 'parser.parseFile',
            binding: 'parser.parseFile',
            options: {
                durable: true,
            }
        },
        PARSE_FILE_RESULT: {
            name: 'parser.parseFileResult',
            binding: 'parser.parseFileResult',
            options: {
                durable: true,
                arguments: { 'x-queue-type': 'stream' },
            }
        },
        PARSE_FILE_STATUS: {
            name: 'parser.parseFileStatus',
            binding: 'parser.parseFileStatus',
            options: {
                durable: true,
            }
        },
    },
} as IExchange;
