import mongoose, { ObjectId } from "mongoose";
import { getSourceModel } from "../models/Source.model";

class SourceService {
    async create(
        { title, words, description, poster, sourceFile, language, type, user, level, totalWordsCount = 0, uniqueWordsCount = 0,
            imdbId,
            kinopoiskId,
            titleOriginal,
            posterUrl,
         }:
            { title: string, description?: string, words: any[], poster?: mongoose.Types.ObjectId | null, sourceFile: mongoose.Types.ObjectId, language: mongoose.Types.ObjectId, type: string, user: mongoose.Types.ObjectId, level?: string, totalWordsCount: number, uniqueWordsCount: number,
                imdbId?: string,
                kinopoiskId?: string,
                titleOriginal?: string,
                posterUrl?: string,
            }
    ) {
        const SourceModel = getSourceModel();
        const res = await SourceModel.insertOne(
            {
                title,
                description,
                poster,
                sourceFile,
                language,
                type,
                user,
                level,
                totalWordsCount,
                uniqueWordsCount,
                words,
                imdbId,
                kinopoiskId,
                titleOriginal,
                posterUrl,
            }
        )

        return res;
    }
}

export const sourceService = new SourceService();