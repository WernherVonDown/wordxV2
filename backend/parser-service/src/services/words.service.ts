import fs from "fs";
import { srtToText } from "../utils/convertors/srtToText";
import languageEncoding from "detect-file-encoding-and-language";
//@ts-ignore
import countWords from "count-words";
import { Document } from "mongoose";
import { getWordsAnalyser } from "../utils/wordsAnalysers";
import { LanguageKeys } from "../const/language/LanguageKeys";
import { getWordModel } from "../models/Word.model";


class WordsService {
    analysSrtFile = async (filePath: string, languageKey: LanguageKeys) => {
        const WordModel = getWordModel()
        const file = await new Promise((res, rej) => fs.readFile(filePath, 'utf8', (err, data) => {
            if (err) rej(err);
            res(data);
        })) as string;

        const textFile = srtToText(file);
        const wordsAnalyser = getWordsAnalyser(languageKey);
        const { words, uniqueCount, allCount, level } = await wordsAnalyser.analysStr(textFile);
        return { words, uniqueCount, allCount, level };
    }

    analysStr = (text: string) => {
        const WordModel = getWordModel()
        const countedWordsObject = countWords(text, true);
        const wordsCounts = Object.values(countedWordsObject) as number[];
        const uniqueWordsCount = wordsCounts.length;
        const allWordsCount = wordsCounts.reduce((p: number, c: number) => p + c, 0)
        const result = Object.keys(countedWordsObject).map((w, i) => (
            {
                word: w,
                count: countedWordsObject[w],
                percent: countedWordsObject[w] / allWordsCount
            }
        ))

        return { words: result, uniqueCount: uniqueWordsCount, allCount: allWordsCount };
    }

    async findById(wordId: string) {
        const WordModel = getWordModel()
        return WordModel.findOne({_id: wordId});
    }
}

export const wordsService = new WordsService();