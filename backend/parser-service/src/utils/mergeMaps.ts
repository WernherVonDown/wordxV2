export const mergeMaps = (map1: Map<string, number>, map2: Map<string, number>) => {
    const mergedMap = new Map([...map1, ...map2]);

    for (const [key, value] of map2.entries()) {
        if (mergedMap.has(key)) {
            const mergedValue = mergedMap.get(key) || 0;
            mergedMap.set(key, mergedValue + value);
        }
    }

    return mergedMap;
}
