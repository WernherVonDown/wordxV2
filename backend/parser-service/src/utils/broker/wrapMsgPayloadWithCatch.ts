import { Channel } from 'amqp-connection-manager';
import { ControllerOpt, IBrokerHandler } from '../../../types/broker';

interface IArgs {
    handlers: IBrokerHandler[];
    channel: Channel;
    noAck?: boolean;
}

export const wrapMsgPayloadWithCatch = (
    func: ({ payload, data, channel }: ControllerOpt) => Promise<void>,
    channel: Channel | null,
): ((data: any | null) => void) => {
    return (data) => {
        if (data) {
            const jsonBody = data.content.toString();
            const payload = JSON.parse(jsonBody);

            func({ payload, data, channel: channel as Channel }).catch((e) => {
                const message =
                    `\t*1.Stack*:\n\t\`${e.stack}\`` +
                    '\n\t*2.Message info*:' +
                    `\n\t  \`exchange: ${data.fields.exchange}` +
                    `\n\t  routingKey: ${data.fields.routingKey}` +
                    `\n\t  redelivered: ${data.fields.redelivered}\`` +
                    `\n\t*3.Message data*:\n\`${JSON.stringify(payload, null, '   ')}\`` +
                    `\n\t*4.Error*:\n\t\`${e.message}\`\n`;
                console.log("Error", message)
                setTimeout(() => {
                    if (channel) channel.nack(data);
                }, 10000);
            });
        }
    };
};
