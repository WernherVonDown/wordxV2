// import { Connection } from 'amqp-connection-manager';
import { createConnection } from './createConnection';

let connectionState: Promise<any>;

export const getConnection = (): Promise<any> => {
    if (!connectionState) {
        console.log("broker:connection:start");
        connectionState = createConnection();
    }
    return connectionState;
}