//@ts-ignore
import countWords from "count-words";
import { YandexDict } from "../../externalServices/yandexDict.service";
import { germanLemmasDict2 } from "./data/germanLemmasDict2";
import Docker from 'dockerode'
import { Writable } from "stream";
const docker = new Docker();


const command: string = "Die schnelle braune möchtest Füchsin springt über den faulen Hund. Und sie hat gesagt. Ich bringe mit";
const imageName: string = "filter/german-lemmatizer:0.5.0";

function lowerCaseFirstLetter(text: string): string {
  return text.replace(/(^\s*\w|[\.\!\?]\s*\w)/gm, match => match.toLowerCase());
}

const runContainer = (text: string): Promise<string> => {
    return new Promise((resolve, reject) => {
      let output = '';
  
      docker.run(
        imageName,
        [
            `${text}`
        ],
        new Writable({
          write(chunk: Buffer, encoding, callback) {
            output += chunk.toString();
            callback();
          }
        }),
        {
          Tty: true,
          stdin: true,
          name: 'lemmatizer-container-' + Date.now() 
        }
      )
      .then(([err, container]) => {
        container.remove()
        resolve(output);
      })
      .catch((err) => {
        reject(err);
      });
    });
  };
  
  // runContainer(command)
  //   .then((output) => {
  //     console.log('Output:', output);
  //   })
  //   .catch((err) => {
  //     console.error('Error:', err);
  //   });

//https://www.reddit.com/r/datasets/comments/1uyd0t/200000_jeopardy_questions_in_a_json_file/
//https://github.com/nlp-compromise/nlp-corpus/
//https://github.com/nlp-compromise/nlp-corpus/blob/master/freq/root.js

class GermanAnalyser {
    private yandexDict: YandexDict;

    constructor() {
        this.yandexDict = new YandexDict('de-ru')
    }

    async translate(text: string) {
        try {
            const res: any[] = await this.yandexDict.translate(text);
            if (res[0]?.tr?.length) {
                const maxFr = Math.max(...res[0].tr.map((t: any) => t.fr))
                // console.log("MAX FR", maxFr)
                const resTran = res[0].tr.filter((t: any) => t.fr === maxFr).map((t: any) => t.text).join(', ')
                // console.log("RESS", text, resTran)
                if (res[0].text !== text) {
                    return {tr: resTran, text: res[0].text}
                }
                return {tr: resTran}
            }
        } catch (error) {
            console.log("EnglishAnalyser.translate error", error)
        }
    }

    getLemma = (word: string) => {
        return germanLemmasDict2[word] || germanLemmasDict2[word.toLowerCase()] || word;
    }

    analysStr = async (text: string) => {
      text = await runContainer(text);
      text = lowerCaseFirstLetter(text.replace(/'(s)/gmi, ""))
        .replace(/[^\p{L}\s]/gu, " ")
        .replace(/\s+/g, ' ');

      const lemmas = text.split(' ').filter(t => t);
      const lemmaCounts = new Map();
      for (const lemma of lemmas) {
        lemmaCounts.set(lemma, (lemmaCounts.get(lemma) || 0) + 1);
      }

        // console.log(lemmaCounts);
        const allWordsCount = Array.from(lemmaCounts.values()).reduce((p, c) => p + c, 0)
        const result = Array.from(lemmaCounts.entries())
            .map(([word, count]) => ({
                word,
                count,
                percent: count / allWordsCount
            }))

        return { words: result, uniqueCount: lemmaCounts.size, allCount: allWordsCount };
    }

    analysStrOld = (text: string) => {
        text = text.replace(/'s/, "");
        const countedWordsObject = countWords(text, false);
        Object.keys(countedWordsObject).forEach(w => {
            const lemma = this.getLemma(w);
            if (lemma !== w) {
                if (countedWordsObject[lemma]) {
                    countedWordsObject[lemma] += countedWordsObject[w];
                } else {
                    countedWordsObject[lemma] = countedWordsObject[w];
                }

                delete countedWordsObject[w];
            }
        })
        
        const wordsCounts = Object.values(countedWordsObject) as number[];
        const uniqueWordsCount = wordsCounts.length;
        const allWordsCount = wordsCounts.reduce((p: number, c: number) => p + c, 0)
        const result = Object.keys(countedWordsObject).map((w, i) => (
            {
                word: w,
                count: countedWordsObject[w],
                percent: countedWordsObject[w] / allWordsCount
            }
        ))
        // console.log('RESULT', text, result)
        return { words: result, uniqueCount: uniqueWordsCount, allCount: allWordsCount };
        return { words: [], uniqueCount: 0, allCount: 0 };
    }
}

export const germanAnalyser = new GermanAnalyser();

// https://www.npmjs.com/package/german-verbs-dict
// https://www.npmjs.com/package/german-adjectives-dict
// https://www.npmjs.com/package/all-the-german-words
// https://www.npmjs.com/package/dictionaries-in-array
// https://www.npmjs.com/package/german-verbs
// https://www.npmjs.com/package/german-adjectives
// https://www.npmjs.com/package/german-words
// https://www.npmjs.com/package/german-determiners
// https://www.npmjs.com/package/german-dict-helper ++
// https://www.npmjs.com/package/@jonhermansen/word-definition +++
// https://www.npmjs.com/package/ubersetzung
// https://www.npmjs.com/package/german-words-dict
// https://www.npmjs.com/package/an-array-of-german-words
// https://www.npmjs.com/package/wordnet-lmf-en
// https://github.com/michmech/lemmatization-lists/blob/master/lemmatization-de.txt
// https://github.com/michmech/lemmatization-lists

// https://www.npmjs.com/package/natural
// https://github.com/jfilter/german-lemmatizer
// https://github.com/dieschnittstelle/german-lemmatizer-docker +--+
// https://github.com/jsalbr/spacy-lemmatizer-de-fix