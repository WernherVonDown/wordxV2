import { LanguageKeys } from "../../const/language/LanguageKeys";
import { DefaultAnalyser } from "./DefaultAnalyser";
import { englishAnalyser } from "./EnglishAnalyser";
import { frenchAnalyser } from "./FrenchAnalyser";
import { germanAnalyser } from "./GermanAnalyser";

const wordsAnalysers: any = {
    [LanguageKeys.english]: englishAnalyser,
    [LanguageKeys.french]: frenchAnalyser,
    [LanguageKeys.german]: germanAnalyser,
    getDefault: (languageKey: LanguageKeys) => new DefaultAnalyser(languageKey) 
}
//https://www.bookrix.com/books;lang:fr.html
// https://linguabooster.com/en/fr/books/original
//https://yandex.com/dev/dictionary/doc/dg/reference/lookup.html
// https://trac.opensubtitles.org/projects/opensubtitles/wiki/DevReadFirst
//https://openlibrary.org/dev/docs/api/books
export const getWordsAnalyser = (languageKey: LanguageKeys) => wordsAnalysers[languageKey] || wordsAnalysers.getDefault(languageKey);