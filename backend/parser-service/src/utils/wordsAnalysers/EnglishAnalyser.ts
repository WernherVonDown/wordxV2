//@ts-ignore
import countWords from "count-words";
import { YandexDict } from "../../externalServices/yandexDict.service";
//@ts-ignore
import grade from 'vocabulary-level-grader';
import { Worker } from "worker_threads";
// const analyseEnglish = require('./analyseEnglish');
import path from "path";
// import { languageLevelService } from "../../services/languageLevel.service";
//https://github.com/yishn/chinese-tokenizer
//https://github.com/Tjatse/node-readability/wiki/Handbook#output
//https://github.com/peterolson/chinese-lexicon
//https://github.com/agj/3000-traditional-hanzi
class EnglishAnalyser {
    private yandexDict: YandexDict;

    constructor() {
        this.yandexDict = new YandexDict('en-ru')
    }

    async translate(text: string) {
        try {
            const res: any[] = await this.yandexDict.translate(text);
            if (res[0]?.tr?.length) {
                const maxFr = Math.max(...res[0].tr.map((t: any) => t.fr))
                // console.log("MAX FR", maxFr)
                const resTran = res[0].tr.filter((t: any) => t.fr === maxFr).map((t: any) => t.text).join(', ')
                // console.log("RESS", text, res[0])
                if (res[0].text !== text) {
                    return {tr: resTran, text: res[0].text}
                }
                return {tr: resTran}
            }
        } catch (error) {
            console.log("EnglishAnalyser.translate error", error)
        }
    }

    analysStr = async (text: string) => {
        const worker = new Worker(path.join(__dirname + '../../../workers/analyseEnglishWorker.js'), {workerData: text})
        const res = await new Promise((res, rej) => {
            worker.on('message', data => res(data))
            worker.on('error', err => rej(err))
        });
        return res;
        // return analyseEnglish(text);
        // const countedWordsObject = countWords(text, true);
        // const wordsCounts = Object.values(countedWordsObject) as number[];
        // const uniqueWordsCount = wordsCounts.length;
        // const allWordsCount = wordsCounts.reduce((p: number, c: number) => p + c, 0)
        // const result = Object.keys(countedWordsObject).map((w, i) => (
        //     {
        //         word: w,
        //         count: countedWordsObject[w],
        //         percent: countedWordsObject[w] / allWordsCount
        //     }
        // ))
        const gradeWords = grade(text)
        const uniqueWords = gradeWords.words;
        // console.log("EEEE", uniqueWords)
        const words = uniqueWords.map((w: any[], i: number) => (
            {
                word: w[0],
                count: w[1],
                percent: w[1] / gradeWords.meta?.words,
            }
        ))

        let level;
        // if (gradeWords.meta?.grade) {
        //     level = await languageLevelService.getBykey(gradeWords.meta?.grade.toLowerCase())
        // }


        // console.log('RESULT', level._id, { words, uniqueCount: uniqueWords.length, allCount: gradeWords.meta?.words, level: level?._id?.toString() })

        return { words, uniqueCount: uniqueWords.length, allCount: gradeWords.meta?.words, level: level };
        //return { words: result, uniqueCount: uniqueWordsCount, allCount: allWordsCount };
    }
}

export const englishAnalyser = new EnglishAnalyser();
// https://www.npmjs.com/package/english-verbs-helper
// https://www.npmjs.com/package/english-plurals-list
// https://www.npmjs.com/package/ml-sentiment ---
// https://www.npmjs.com/package/tokenize-english
// https://www.npmjs.com/package/tokenize-text
// https://www.npmjs.com/package/search-text-tokenizer
// https://www.npmjs.com/package/is-singular
// https://www.npmjs.com/package/wordnet
// https://www.npmjs.com/package/en-wordnet
// https://www.npmjs.com/package/en-dictionary
// https://www.npmjs.com/package/owlbot-js
// https://owlbot.info/
// http://naturalnode.github.io/natural/Tokenizers.html
// https://www.npmjs.com/package/an-array-of-english-words
// https://www.npmjs.com/package/@stdlib/datasets-male-first-names-en
// https://www.npmjs.com/package/a-set-of-english-words

// https://www.npmjs.com/package/truly-unique
// https://www.npmjs.com/package/is-english
// https://www.npmjs.com/package/console-translator

// https://www.npmjs.com/package/vocabulary-level-grader +++
// https://www.npmjs.com/package/vocabulary-list-statistics
// https://www.npmjs.com/package/dictionaries-in-array ++

// https://www.npmjs.com/package/word-pictionary-list 

// https://www.npmjs.com/package/verbix -- 

// https://www.npmjs.com/package/enru-dict +++
// https://www.npmjs.com/package/en-inflectors +++
// https://www.npmjs.com/package/wink-pos-tagger +++
// https://www.npmjs.com/package/extract-lemmatized-nonstop-words ++++
// https://www.npmjs.com/package/wordlevel +++
// https://www.npmjs.com/package/lemme-lex +++
// https://www.npmjs.com/package/@derock.ir/lemmas-forms ++++
// https://www.npmjs.com/package/javascript-lemmatizer ++ 

// https://dictionary.skyeng.ru/doc/api/external

// //https://www.npmjs.com/package/coupling-dict-chinese ---



///https://www.npmjs.com/package/subtitles