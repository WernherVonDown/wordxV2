const grade = require('vocabulary-level-grader');

const analyseEnglish = (text) => {
    const gradeWords = grade(text)
    const uniqueWords = gradeWords.words;
    const words = uniqueWords.map((w, i) => (
        {
            word: w[0],
            count: w[1],
            percent: w[1] / gradeWords.meta?.words,
        }
    ))

    let level;
    return { words, uniqueCount: uniqueWords.length, allCount: gradeWords.meta?.words, level: level };
}

module.exports = analyseEnglish;