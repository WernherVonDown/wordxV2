//@ts-ignore
import countWords from "count-words";
import { YandexDict } from "../../externalServices/yandexDict.service";
//@ts-ignore
import NlpjsTFr from 'nlp-js-tools-french';
//https://www.npmjs.com/package/words-splitter

//https://github.com/Einenlum/french-verbs-list
//https://github.com/MarkKahn/stem
//https://github.com/rustico/fr-conjugation
//https://github.com/gauthier-th/conjugation-fr
//https://github.com/words/an-array-of-french-words

//https://github.com/search?l=JavaScript&p=12&q=french&type=Repositories
//@ts-ignore
import nlp from 'fr-compromise';

//@ts-ignore
// import { frenchPhrases  } from "./data/frenchPharases.js";
import { mergeMaps } from "../mergeMaps";
import path from "path";
import { Worker } from "worker_threads";
// import { ILanguagePhrase } from "../../const/interfaces";


// const getPhrases = (text: string): {
//     phrases: Map<string, number>,
//     text: string
// } => {
//     const phrases = new Map();
//     frenchPhrases.map(({ regExp, phrase, exclude }: ILanguagePhrase) => {
//         const matchResult = text.match(regExp)
//         if (matchResult?.length) {
//             phrases.set(phrase, matchResult?.length);
//         }
//     })

//     frenchPhrases
//         .filter((p: ILanguagePhrase) => phrases.has(p.phrase) && p.exclude)
//         .map((p: ILanguagePhrase) => {
//             text = text.replace(p.regExp, '')
//         })

//     return {
//         text,
//         phrases,
//     }
// }

class FrenchAnalyser {
    private yandexDict: YandexDict;

    constructor() {
        this.yandexDict = new YandexDict('fr-ru')
    }

    async translate(text: string) {
        try {
            const res: any[] = await this.yandexDict.translate(text);
            if (res && res[0]?.tr?.length) {
                const maxFr = Math.max(...res[0].tr.map((t: any) => t.fr))
                // console.log("MAX FR", maxFr)
                const resTran = res[0].tr.filter((t: any) => t.fr === maxFr).map((t: any) => t.text).join(', ')
                if (res[0].text !== text) {
                    return { tr: resTran, text: res[0].text }
                }
                return { tr: resTran }
            } else {
                // console.log("SOME SHIITT", res)
            }
        } catch (error) {
            console.log("EnglishAnalyser.translate error", error)
        }
    }

    analysStr = async (text: string) => {
        const worker = new Worker(path.join(__dirname + '../../../workers/analyseFrenchWorker.js'), {workerData: text})
        const res = await new Promise((res, rej) => {
            worker.on('message', data => res(data))
            worker.on('error', err => rej(err))
        });
        //@ts-ignore
        console.log("FRENCH RES", res?.uniqueCount)
        return res;
//         const {
//             text: textNew,
//             phrases,
//         } = getPhrases(text);

//         console.log("RESULT", phrases)
//         text = textNew
//         const doc = nlp(
//             text
//                 .replace(/(j|s|c|l|m|t|d|qu|n)('|’|`)/gmis, "$1e ")
//                 .replace(/-t-|-|–|,|»|«|\*|"/gmis, " ")
//                 // .replace(/-|–|,/gmis, " ")
//                 .replace(/…/gmis, ". ")
//                 // .replace(/»|«|\*|"/gm, "")
//                 .replace(/(?:https?|ftp):\/\/[\n\S]+|www[\n\S]+|[\w\d]+@[\w\d]+\.[\w]{2,}/gi, '')
//         );
//         doc.nouns().toSingular();
//         const terms = doc.terms();
//         let lemmaCounts = new Map();

//         terms.forEach((term: any) => {
//             let lemma = term.has('#Verb') ? term.verbs().conjugate()[0].Infinitive : term.out('normal');


//             lemma = lemma.replace(/[\.\?\!\-\—'…]|\d+/gm, " ").trim()
//             if (!lemma.length) {
//                 return
//             }
//             const count = lemmaCounts.get(lemma) || 0;
//             lemmaCounts.set(lemma, count + 1);
//         });

//         lemmaCounts = mergeMaps(lemmaCounts, phrases);
//         // console.log(lemmaCounts)
// // return
//         const allWordsCount = Array.from(lemmaCounts.values()).reduce((p, c) => p + c, 0)
//         const result = Array.from(lemmaCounts.entries())
//             .map(([word, count]) => ({
//                 word,
//                 count,
//                 percent: count / allWordsCount
//             }))

//         return { words: result, uniqueCount: lemmaCounts.size, allCount: allWordsCount };
    }

    analysStrOld = (text: string) => {
        text = text.replace(/(j|s|c|l|m|t|d)'/gmi, "")
        const nlpToolsFr = new NlpjsTFr(text, { strictness: true });
        const lematizedWords = nlpToolsFr.lemmatizer();
        // console.log("ANALYS", lematizedWords)
        const maxIndex = Math.max(...lematizedWords.map((l: any) => l.id));
        // console.log("TEXT", maxIndex)
        const replacedWords = new Set();
        for (let i = 0; i <= maxIndex; i++) {

            const words = lematizedWords.filter((w: any) => w.id === i);
            const word = words[0]?.word;

            if (replacedWords.has(word)) {
                continue;
            }

            if (!isNaN(word)) {
                text = text.replace(new RegExp(word, "igm"), "");
                continue;
            }

            const lemmas = words.map((w: any) => w.lemma).join(' ');
            // console.log("REPLACE LEMMA", word, lemmas, words, i)
            text = text.replace(new RegExp(`(^|@|\\W)${word}(\\W|$)`, "igm"), ` ${lemmas} `);
            replacedWords.add(word)
        }
        // console.log("RESULT TEXT", text)
        const countedWordsObject = countWords(text, true);
        // console.log("COUNTED OBJECT", countedWordsObject)
        const wordsCounts = Object.values(countedWordsObject) as number[];
        const uniqueWordsCount = wordsCounts.length;
        const allWordsCount = wordsCounts.reduce((p: number, c: number) => p + c, 0)
        const result = Object.keys(countedWordsObject).map((w, i) => (
            {
                word: w,
                count: countedWordsObject[w],
                percent: countedWordsObject[w] / allWordsCount
            }
        ))
        return { words: result, uniqueCount: uniqueWordsCount, allCount: allWordsCount };
    }
}

export const frenchAnalyser = new FrenchAnalyser();

// frenchAnalyser.analysStr(`C'est ma vie aujourd'hui
// Je m'appelle Angélica Summer, j'ai 12 ans pêle-mêle et je suis canadienne de dingue. Il y a 5 ans, ma famille et moi avons déménagé dans le sud de la France. Mon père, Frank Summer, est Aujourd'hui mécanicien ; il adore les voitures anciennes et collectionne les voitures miniatures.`)

// https://www.npmjs.com/package/nlp-js-tools-french
// https://www.npmjs.com/package/pluralize-fr
// https://www.npmjs.com/package/dictionaries-in-array //
// https://www.npmjs.com/package/french-verbs
// https://www.npmjs.com/package/@zxcvbn-ts/language-fr
// https://www.npmjs.com/package/french-verbs-list

// https://www.npmjs.com/package/french-dev-memes :)))

// https://dictionaryapi.dev/
// https://github.com/lyndabelfar/Dictionary/blob/main/script.js

//https://www.bookrix.com/books;lang:fr.html