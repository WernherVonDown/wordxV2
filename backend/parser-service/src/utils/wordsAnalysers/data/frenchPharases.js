

module.exports.frenchPhrases = [
    {
        regExp: /aujourd('|’)hui/gmi,
        phrase: "aujourd'hui",
        exclude: true,
    },
    {
        regExp: /pêle-mêle/gumi,
        phrase: "pêle-mêle",
        exclude: true,
    },
    {
        regExp: /de dingue/gumi,
        phrase: "de dingue",
        exclude: false,
    },
    {
        regExp: /au revoir/gumi,
        phrase: "au revoir",
        exclude: false,
    },
]
