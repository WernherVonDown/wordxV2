const nlp = require('fr-compromise');
const { frenchPhrases } = require("./data/frenchPharases");
const PERCENT_STEPS = 10;
const mergeMaps = (map1, map2) => {
    const mergedMap = new Map([...map1]);
  
    for (const [key, value] of map2.entries()) {
      if (mergedMap.has(key)) {
        const mergedValue = mergedMap.get(key);
        const newValue = value.map((num, index) => num + (mergedValue[index] || 0));
        mergedMap.set(key, newValue);
      } else {
        mergedMap.set(key, value);
      }
    }
  
    return mergedMap;
  };

 const getPhrases = (text) => {
    const phrases = new Map();
    const percentStep = text.length / PERCENT_STEPS;
    const percentPhrases = new Map();
    frenchPhrases.map(({ regExp, phrase, exclude }) => {
        const matchResultI = text.matchAll(regExp)
        const matchResult = [...matchResultI]
        console.log("MATCH PHRASE", regExp, phrase, matchResult)
        if (matchResult.length) {
            phrases.set(phrase, matchResult.length);
            const percentCount = matchResult.reduce((a, c) => {
                const el = c[0];
                console.log("PROC el", el, c)
                if (!el) return a;
                const currentPercent = Math.trunc(c.index / percentStep);
                const currentCount = a[currentPercent] || 0;
                a[currentPercent] = currentCount + 1;
                return a;
            }, new Array(10));
            percentPhrases.set(phrase, percentCount)
        }
    })
    console.log('EEE',phrases, frenchPhrases, percentPhrases)
     frenchPhrases
        .filter(p => phrases.has(p.phrase) && p.exclude)
        .map(p => {
            text = text.replaceAll(p.regExp, '')
        })
     return {
        text,
        phrases,
        percentPhrases,
    }
}
 const analyseFrench = (text) => {
    const {
        text: textNew,
        phrases,
        percentPhrases,
    } = getPhrases(text);
     console.log("RESULT", phrases)
    text = textNew
    const doc = nlp(
        text
            .replace(/(j|s|c|l|m|t|d|qu|n)('|’|`)/gmis, "$1e ")
            .replace(/-t-|-|–|,|»|«|\*|•|\"/gmis, " ")
            .replace(/…/gmis, ". ")
            .replace(/(?:https?|ftp):\/\/[\n\S]+|www[\n\S]+|[\w\d]+@[\w\d]+\.[\w]{2,}/gi, '')
    );
    doc.nouns().toSingular();
    const terms = doc.terms();
    let lemmaCounts = new Map();
    const percentStep = terms.length / PERCENT_STEPS;
    const lemmaPercentCounts = new Map();
    let currentPercentRound = 0;
    let currentPercent = 0;
    terms.forEach((term) => {
        let lemma = term.has('#Verb') ? term.verbs().conjugate()[0].Infinitive : term.out('normal');
         lemma = lemma.replace(/[\.\?\!\-\—'…]|\d+/gm, " ").trim()
        if (!lemma.length) {
            return
        }
        
        const count = lemmaCounts.get(lemma) || 0;
        lemmaCounts.set(lemma, count + 1);
        const percentArray = lemmaPercentCounts.get(lemma) || new Array(PERCENT_STEPS)
        const currentCount = percentArray[currentPercent] || 0;
        percentArray[currentPercent] = currentCount + 1;
        lemmaPercentCounts.set(lemma, percentArray);
        ++currentPercentRound;
        if (currentPercentRound >= percentStep) {
            currentPercentRound = 0;
            ++currentPercent;
        }
    });
     lemmaCounts = mergeMaps(lemmaCounts, phrases);
     console.log("PERCENT BEFOURE", percentPhrases)
    const allLemmasPercent = mergeMaps(percentPhrases, lemmaPercentCounts);
    console.log("MERGE", lemmaPercentCounts, percentPhrases)
     const allWordsCount = Array.from(lemmaCounts.values()).reduce((p, c) => p + c, 0)
     console.log("ALL LEMMAS EEL", allLemmasPercent)
    const result = Array.from(lemmaCounts.entries())
        .map(([word, count]) => ({
            word,
            count,
            percent: count / allWordsCount,
            percentStat: allLemmasPercent.get(word)?.join(',') || ''
        }))
        console.log("RESSSS", result, allWordsCount)
     return { words: result, uniqueCount: lemmaCounts.size, allCount: allWordsCount };
 }
 module.exports = analyseFrench;