//@ts-ignore
import yandict from "yandict";
import { vars } from "../config/vars";

yandict.key = vars.yandex.dictKey || 'dict.1.1.20220820T160535Z.488965840554260f.58efd5012a54c84276e0006ea33f0c0cc74fa192';
// yandict.reload()

export class YandexDict {
    constructor(
        private lang: string,
    ) {}
    async translate(text: string): Promise<any[]> {
        return new Promise(async res => {
            // yandict.onload = async () => {
                // console.log("T", text, this.lang)
                const t = await yandict.lookup(text, this.lang);
                // console.log("RES T", t)
                res(t?.def)
            // }
        })
        
    } 
}

