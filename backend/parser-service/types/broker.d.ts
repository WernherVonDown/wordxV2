interface IBrokerHandlerArgs {
    payload: any;
    data: ConsumeMessage;
    channel: Channel;
}

export type IBrokerHandler = (args: IBrokerHandlerArgs) => Promise<{ acked?: boolean } | void>;

export interface IQueue {
    name: string;
    binding: string;
    options: Record<string, any>;
}

export interface IExchange {
    name: string;
    type: 'direct' | 'fanout' | 'topic' | 'headers' | 'match';
    options: {
        durable: boolean;
    };
    queues: Record<string, IQueue>;
}
export interface ControllerOpt {
    payload: any;
    data: ConsumeMessage;
    channel: Channel;
}