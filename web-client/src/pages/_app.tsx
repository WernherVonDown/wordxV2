import '../styles/globals.css'
import '@fontsource/inter/300.css';
import '@fontsource/inter/400.css';
import '@fontsource/inter/500.css';
import '@fontsource/inter/700.css';
import type { AppProps } from 'next/app'
import { AuthContextProvider } from '../context/AuthContext'
import { ThemeContextProvider } from '../context/ThemeContext';


function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ThemeContextProvider>
      <AuthContextProvider>
        <Component {...pageProps} />
      </AuthContextProvider>
    </ThemeContextProvider>
  )
}

export default MyApp
