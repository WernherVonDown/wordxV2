import { CenterXY } from "../common/CenterXY/CenterXY"
import { LandingLayout } from "../components/LandingLayout/LandingLayout"
import { RemindPasswordForm } from "../components/RemindPasswordForm/RemindPasswordForm.component"

export default function RemindPasswordPage() {
    return (
        <LandingLayout>
            <CenterXY>
                <RemindPasswordForm />
            </CenterXY>
        </LandingLayout>
    )
}
