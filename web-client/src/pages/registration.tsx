import React from "react";
import { CenterXY } from "../common/CenterXY/CenterXY";
import { LandingLayout } from "../components/LandingLayout/LandingLayout";
import { MainLayout } from "../components/MainLayout/MainLayout";
import { RegistrationForm } from '../components/RegistrationForm/RegistrationForm.component';

export default function Registration () {
    return (
        <LandingLayout>
            <CenterXY>
                <RegistrationForm />
            </CenterXY>
        </LandingLayout>
    )
}
