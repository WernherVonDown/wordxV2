import React from "react";
import { ChooseLanguages } from "../../components/ChooseLanguages/ChooseLanguages";
import { Dashboard } from "../../components/Dashboard/Dashboard.component";
import { LandingLayout } from "../../components/LandingLayout/LandingLayout";
import { withAuth } from "../../hoc/withAuth";

const LanguagesPage: React.FC = () => {
    return (
        <LandingLayout>
            <ChooseLanguages />
        </LandingLayout>
    )
}

export default withAuth(LanguagesPage);