import React from "react";
import { Dashboard } from "../../components/Dashboard/Dashboard.component";
import { LandingLayout } from "../../components/LandingLayout/LandingLayout";
import { SubscriptionPlans } from "../../components/SubscriptionPlans/SubscriptionPlans";
import { useAuth } from "../../context/AuthContext";
import { withAuth } from "../../hoc/withAuth";

const PlansPage: React.FC = () => {
    return (
        <LandingLayout>
            <SubscriptionPlans />
        </LandingLayout>
    )
}

export default PlansPage;