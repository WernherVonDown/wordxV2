import { AppBar, Container, styled, Toolbar } from "@mui/material";
import React, { useContext, useEffect } from "react";
import { CenterXY } from "../common/CenterXY/CenterXY";
import { LandingLayout } from "../components/LandingLayout/LandingLayout";
import { LoginForm } from '../components/LoginForm/LoginForm.component';
import { MainLayout } from "../components/MainLayout/MainLayout";
import { AuthContext } from "../context/AuthContext";

const Offset = styled('div')(({ theme }) => theme.mixins.toolbar);
//@ts-ignore
export default function Login({ message, mems, hello }) {
    const { state: { loggedIn }} = useContext(AuthContext)
    console.log("MESSAGE", message)

    return (
        <LandingLayout>
            <CenterXY>
                {loggedIn ? <div>ПРИВЕТ!</div>
                : <LoginForm />}
                {/* {mems?.map((m: any) => <img src={m.image} />)} */}
            </CenterXY>
        </LandingLayout>
    )
}

// export async function getServerSideProps() {
//     const mems = [
//         await memes.random(),
//         await memes.fromReddit("en"),
//         await memes.fromReddit("ru"),
//         await memes.fromReddit("fr"),
//         await memes.fromReddit("de"),
//     ]
    
//     console.log("MEMES", mems)

//     const hello = () => console.log("HELLO EEE")

//     return {
//         props: {
//             message: 'Hello world!',
//             mems,
//             hello
//         }
//     }
// }
 
