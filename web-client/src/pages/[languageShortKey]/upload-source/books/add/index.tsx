import React from 'react';
import { ILangAuthProps, withLangAuth } from '../../../../../hoc/withLangAuth';
import { SourceContextProvider } from '../../../../../context/SourceContext';
import { MainLayout } from '../../../../../components/MainLayout/MainLayout';
import { UploadSourceFile } from '../../../../../components/UploadSource/UploadSourceFile';
import { SourceTypes } from '../../../../../const/source/SourceTypes';



const UploadSourceFilePage: React.FC<ILangAuthProps> = ({ languageKey }) => {
    return (
        <MainLayout languageKey={languageKey}>
            <SourceContextProvider languageKey={languageKey}>
                <UploadSourceFile sourceType={SourceTypes.film} />
            </SourceContextProvider>
        </MainLayout>
    )
}

export default withLangAuth(UploadSourceFilePage);
