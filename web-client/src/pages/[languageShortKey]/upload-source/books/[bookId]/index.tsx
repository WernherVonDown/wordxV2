import React from 'react';
import { ILangAuthProps, withLangAuth } from '../../../../../hoc/withLangAuth';
import { SourceContextProvider } from '../../../../../context/SourceContext';
import { UploadSource } from '../../../../../components/UploadSource/UploadSource.component';
import { MainLayout } from '../../../../../components/MainLayout/MainLayout';
import { UploadFilm } from '../../../../../components/UploadSource/UploadFilm';
import { UploadFilmPageItem } from '../../../../../components/UploadSource/UploadFilmPageItem';
import { UploadBookPageItem } from '../../../../../components/SearchBooks/UploadBookPageItem';

const UploadBookItemPage: React.FC<ILangAuthProps> = ({ languageKey }) => {
    return (
        <MainLayout languageKey={languageKey}>
            <SourceContextProvider languageKey={languageKey}>
                <UploadBookPageItem />
            </SourceContextProvider>
        </MainLayout>
    )
}

export default withLangAuth(UploadBookItemPage);
