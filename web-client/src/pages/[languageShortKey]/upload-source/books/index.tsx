import React from 'react';
import { ILangAuthProps, withLangAuth } from '../../../../hoc/withLangAuth';
import { SourceContextProvider } from '../../../../context/SourceContext';
import { MainLayout } from '../../../../components/MainLayout/MainLayout';
import { SearchBooks } from '../../../../components/SearchBooks/SearchBooks';

const UploadSourcePage: React.FC<ILangAuthProps> = ({ languageKey }) => {
    return (
        <MainLayout languageKey={languageKey}>
            <SourceContextProvider languageKey={languageKey}>
                <SearchBooks />
            </SourceContextProvider>
        </MainLayout>
    )
}

export default withLangAuth(UploadSourcePage);
