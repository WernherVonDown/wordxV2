import React, { useCallback, useContext, useEffect, useState } from 'react';
import { ILangAuthProps, withLangAuth } from '../../../../../hoc/withLangAuth';
import { SourceContext, SourceContextProvider } from '../../../../../context/SourceContext';
import { MainLayout } from '../../../../../components/MainLayout/MainLayout';
import { Button, Card, CardActions, CardContent, CardMedia, Grid, Typography } from '@mui/material';
import { useRouter } from 'next/router';
import { NextParsedUrlQuery } from 'next/dist/server/request-meta';
import { TextareaAutosize } from '@mui/base';
import { useBool } from '../../../../../hooks/useBool';
import { SourceTypes } from '../../../../../const/source/SourceTypes';
import { IYoutube } from '../../../../../const/source/interfaces';
import YoutubeService from '../../../../../externalServices/youtubeSertvice';
import { useLanguage } from '../../../../../hooks/useLanguage';

interface IRouterQuery extends NextParsedUrlQuery {
    youtubeId: string,
}

interface ISub {
    start: string,
    dur: string,
    text: string,
}

const Page: React.FC<any> = ({ }) => {
    const { actions: { onGetSong, onGetSongLyrics, uploadSource } } = useContext(SourceContext)
    const router = useRouter();
    const [video, setVideo] = useState<IYoutube>()
    const { youtubeId } = router.query as IRouterQuery;
    const [lyrics, setLyrics] = useState('');
    const { onTrue: onAuto, value: autoLyrics } = useBool(false)
    const {onTrue: onUploadSuccess, value: uploadSuccess} = useBool(false);
    const { languageShortKey } = useLanguage()
    const { onTrue: onNotFound, value: notFound} = useBool(false)
    const { onTrue: noAutoSubs, value: noAuto} = useBool(false)

    useEffect(() => {
        if (youtubeId) {
            YoutubeService.getVideoInfoById(youtubeId).then(v => {
                if (v) {
                    setVideo(v)
                } else {
                    onNotFound()
                }
            }).catch(e => alert("ERROR"))
        }
    }, [youtubeId])

    const onSave = useCallback(async () => {
        if (lyrics && video) {
            const formData = new FormData();
            const blob = new Blob([lyrics], {type : 'text/plain'})
            formData.append('file', blob, `${video.title}.txt'`)
            formData.append("title", video.title);
            formData.append("youtubeId", youtubeId);
            // songId,
            // author,
            formData.append("author", video.channelTitle);
            formData.append("posterUrl", video.thumbnails.standard.url);
            const res = await uploadSource(formData, SourceTypes.youtube);
            onUploadSuccess()
        }
        
    }, [lyrics, video])

    if (notFound) {
        return <Grid>
            <Typography>Видео не найдено :(</Typography>
        </Grid>
    }

    if (uploadSuccess) {
        return <Grid>
            <Typography>Успех!</Typography>
        </Grid>
    }

    return (
        <Grid container xs={12} display={'flex'} flexDirection={'row'}>
            <Grid xs={6}>
                <TextareaAutosize value={lyrics} disabled={autoLyrics} onChange={(e) => { autoLyrics && setLyrics(e.target.value) }} style={{ width: '100%', maxWidth: '100%', maxHeight: '50vh', overflow: 'auto' }} placeholder='Вставьте титры' minRows={5} />

            </Grid>
            <Grid xs={6}>
                {video ? <><Card sx={{ maxWidth: 300 }}>
                    <CardMedia
                        component="img"
                        alt={video.title}
                        height="140"
                        image={video?.thumbnails.standard.url}
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="div">
                            {video.title}
                        </Typography>
                        <Typography variant="body2" color="text.secondary">
                            {video.channelTitle}
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <Button disabled={autoLyrics || noAuto} onClick={async () => {
                            const data = await YoutubeService.getVideoCaptionsById(youtubeId, languageShortKey);
                            console.log("EE", data)
                            if (!data.subtitles?.length) {
                                return noAutoSubs()
                            }
                            setLyrics(data.subtitles.map((s: ISub) => s.text).join('\n'))
                            onAuto()
                        }}>Загрузить автоматически</Button>
                    </CardActions>
                    
                </Card> 
                {noAuto && <Typography>Титры не найдены</Typography>}
                </>
                    : <Typography>Loading..</Typography>}
            </Grid>
            <Button variant='contained' disabled={!lyrics} onClick={onSave}>Сохранить</Button>
        </Grid>
    )
}

const Index: React.FC<ILangAuthProps> = ({ languageKey }) => {
    return (
        <MainLayout languageKey={languageKey}>
            <SourceContextProvider languageKey={languageKey}>
                <Page />
            </SourceContextProvider>
        </MainLayout>
    )
}

export default withLangAuth(Index);
