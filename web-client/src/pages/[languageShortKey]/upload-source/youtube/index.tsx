import React, { useCallback, useState } from 'react';
import { ILangAuthProps, withLangAuth } from '../../../../hoc/withLangAuth';
import { SourceContextProvider } from '../../../../context/SourceContext';
import { UploadSource } from '../../../../components/UploadSource/UploadSource.component';
import { MainLayout } from '../../../../components/MainLayout/MainLayout';
import { Box, Input } from '@mui/material';
import { useInput } from '../../../../hooks/useInput';
import { useRouter } from 'next/router';
import { getUploadYoutubeItemRoute } from '../../../../const/APP_ROUTES';
import getYouTubeID from 'get-youtube-id';

interface ISub {
    start: string,
    dur: string,
    text: string,
}

const UploadYoutubePage: React.FC<ILangAuthProps> = ({ languageKey }) => {
    const youtubeUrl = useInput('')
    const [subs, setSubs] = useState<ISub[]>([])
    const router = useRouter()
    const onSearch = useCallback(async () => {
        if (!youtubeUrl.value) return;
        const videoId = getYouTubeID(youtubeUrl.value);
        if (!videoId) {
            return alert("Некорректная ссылка")
        }
        router.push(getUploadYoutubeItemRoute(languageKey, videoId))
        // const res = await YoutubeService.getVideoCaptionsByUrl(youtubeUrl.value, languageShortKey);
        // console.log("RESULT", res)
        // setSubs(res.subtitles)
    }, [youtubeUrl.value, languageKey])
    return (
        <MainLayout languageKey={languageKey}>
            <SourceContextProvider languageKey={languageKey}>
                <Box>
                    <input {...youtubeUrl} />
                    <button onClick={onSearch}>search</button>
                    {/* <Box sx={{ background: 'white' }} height={'500px'} width={'500px'} overflow={'auto'}>
                        {subs.map(s => (<div>{s.start}: {s.text}</div>))}
                    </Box> */}
                    {/* <Box maxHeight={'50%'} overflow={'hidden'}>{subs.map(s => (<div>{s.start}: {s.text}</div>))}</Box> */}

                </Box>
            </SourceContextProvider>
        </MainLayout>
    )
}

export default withLangAuth(UploadYoutubePage);
