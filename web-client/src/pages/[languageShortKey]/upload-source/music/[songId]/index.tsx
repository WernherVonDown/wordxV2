import React, { useCallback, useContext, useEffect, useState } from 'react';
import { ILangAuthProps, withLangAuth } from '../../../../../hoc/withLangAuth';
import { SourceContext, SourceContextProvider } from '../../../../../context/SourceContext';
import { MainLayout } from '../../../../../components/MainLayout/MainLayout';
import { Button, Card, CardActions, CardContent, CardMedia, Grid, Typography } from '@mui/material';
import { useRouter } from 'next/router';
import { NextParsedUrlQuery } from 'next/dist/server/request-meta';
import { getSongLyricsApiRoute } from '../../../../../const/source/SourceApiRoutes';
import { ISongSearch } from '../../../../../const/music/interfaces';
import { TextareaAutosize } from '@mui/base';
import { useBool } from '../../../../../hooks/useBool';
import { SourceTypes } from '../../../../../const/source/SourceTypes';

interface IRouterQuery extends NextParsedUrlQuery {
    songId: string,
}

const Page: React.FC<any> = ({ }) => {
    const { actions: { onGetSong, onGetSongLyrics, uploadSource } } = useContext(SourceContext)
    const router = useRouter();
    const [song, setSong] = useState<ISongSearch>()
    const { songId } = router.query as IRouterQuery;
    const [lyrics, setLyrics] = useState('');
    const { onTrue: onAuto, value: autoLyrics } = useBool(false)
    const {onTrue: onUploadSuccess, value: uploadSuccess} = useBool(false);

    useEffect(() => {
        if (songId) {
            onGetSong(Number(songId)).then((e: any) => setSong(e.song)).catch(console.log)
        }
    }, [songId])

    const onSave = useCallback(async () => {
        if (lyrics && song) {
            const formData = new FormData();
            const blob = new Blob([lyrics], {type : 'text/plain'})
            formData.append('file', blob, `${song.title || song.fullTitle}.txt'`)
            formData.append("title", song.fullTitle || song.title);
            formData.append("songId", String(song.id));
            // songId,
            // author,
            formData.append("author", song.artist_names);
            formData.append("posterUrl", song.header_image_thumbnail_url);
            const res = await uploadSource(formData, SourceTypes.music);
            onUploadSuccess()
        }
        
    }, [lyrics, song])

    if (uploadSuccess) {
        return <Grid>
            <Typography>Успех!</Typography>
        </Grid>
    }

    return (
        <Grid container xs={12} display={'flex'} flexDirection={'row'}>
            <Grid xs={6}>
                <TextareaAutosize value={lyrics} disabled={autoLyrics} onChange={(e) => { autoLyrics && setLyrics(e.target.value) }} style={{ width: '100%', maxWidth: '100%', maxHeight: '50vh', overflow: 'auto' }} placeholder='Вставьте текст песни' minRows={5} />

            </Grid>
            <Grid xs={6}>
                {song ? <Card sx={{ maxWidth: 300 }}>
                    <CardMedia
                        component="img"
                        alt={song.title}
                        height="140"
                        image={song.header_image_thumbnail_url}
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="div">
                            {song.title}
                        </Typography>
                        <Typography variant="body2" color="text.secondary">
                            {song.fullTitle}
                        </Typography>
                        <Typography variant="body2" color="text.secondary">
                            {song.artist}
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <Button disabled={autoLyrics} onClick={async () => {
                            const data = await onGetSongLyrics(song.id);
                            console.log("EE", data)
                            setLyrics(data.lyrics)
                            onAuto()
                        }}>Загрузить автоматически</Button>
                    </CardActions>
                </Card>
                    : <Typography>Loading..</Typography>}
            </Grid>
            <Button variant='contained' disabled={!lyrics} onClick={onSave}>Сохранить</Button>
        </Grid>
    )
}

const Index: React.FC<ILangAuthProps> = ({ languageKey }) => {
    return (
        <MainLayout languageKey={languageKey}>
            <SourceContextProvider languageKey={languageKey}>
                <Page />
            </SourceContextProvider>
        </MainLayout>
    )
}

export default withLangAuth(Index);
