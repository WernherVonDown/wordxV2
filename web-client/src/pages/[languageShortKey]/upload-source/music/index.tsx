import React, { useCallback, useContext, useEffect, useState } from 'react';
import { ILangAuthProps, withLangAuth } from '../../../../hoc/withLangAuth';
import { SourceContext, SourceContextProvider } from '../../../../context/SourceContext';
import { UploadSource } from '../../../../components/UploadSource/UploadSource.component';
import { MainLayout } from '../../../../components/MainLayout/MainLayout';
import { UploadFilm } from '../../../../components/UploadSource/UploadFilm';
import { SearchFilm } from '../../../../components/SearchFilm/SearchFilm';
import { Button, Card, CardActions, CardContent, CardMedia, Grid, Typography } from '@mui/material';
import { useInput } from '../../../../hooks/useInput';
import { ISongSearch } from '../../../../const/music/interfaces';
import { useRouter } from 'next/router';
import { getUploadSongItemRoute } from '../../../../const/APP_ROUTES';
import { useLanguage } from '../../../../hooks/useLanguage';

const Page: React.FC<any> = ({ }) => {
    const router = useRouter();
    const trackName = useInput(router.query.q as string || '');
    const { actions: { onSearchMusic } } = useContext(SourceContext)
    const [results, setResults] = useState<ISongSearch[]>([])
    const { languageKey } = useLanguage();

    useEffect(() => {
        if (router.query.q) {
            onSearch()
        }
    }, [router.query.q])
    
    const onSearch = useCallback(async () => {
        if (trackName.value.trim().length) {
            const { songs } = await onSearchMusic(trackName.value);
            console.log("songs", songs)
            router.query.q = trackName.value;
            router.push(router);
            if (songs) {
                setResults(songs);
            }
        }
    }, [trackName.value]);

    const renderSong = (song: ISongSearch) => {
        return <Card sx={{ maxWidth: 300 }}>
            <CardMedia
                component="img"
                alt={song.title}
                height="140"
                image={song.header_image_thumbnail_url}
            />
            <CardContent>
                <Typography gutterBottom variant="h5" component="div">
                    {song.title}
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    {song.fullTitle}
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    {song.artist}
                </Typography>
            </CardContent>
            <CardActions>
                <Button onClick={() => router.push(getUploadSongItemRoute(languageKey, song.id))}>Выбрать</Button>
            </CardActions>
        </Card>
    }

    return (
        <Grid container xs={12} display={'flex'} flexDirection={'column'}>
            <Grid xs={12}>
                Поиск музыки
                <input {...trackName} />
                <Button variant={'contained'} onClick={onSearch} >Поиск</Button>
            </Grid>
            <Grid container gap={1} justifyContent="center">
                {results.map(renderSong)}
            </Grid>
        </Grid>
    )
}

const Index: React.FC<ILangAuthProps> = ({ languageKey }) => {
    return (
        <MainLayout languageKey={languageKey}>
            <SourceContextProvider languageKey={languageKey}>
                <Page />
            </SourceContextProvider>
        </MainLayout>
    )
}

export default withLangAuth(Index);
