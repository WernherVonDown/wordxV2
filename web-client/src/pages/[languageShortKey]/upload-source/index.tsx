import React from 'react';
import { ILangAuthProps, withLangAuth } from '../../../hoc/withLangAuth';
import { SourceContextProvider } from '../../../context/SourceContext';
import { UploadSource } from '../../../components/UploadSource/UploadSource.component';
import { MainLayout } from '../../../components/MainLayout/MainLayout';

const UploadSourcePage: React.FC<ILangAuthProps> = ({ languageKey }) => {
    return (
        <MainLayout languageKey={languageKey}>
            <SourceContextProvider languageKey={languageKey}>
                <UploadSource />
            </SourceContextProvider>
        </MainLayout>
    )
}

export default withLangAuth(UploadSourcePage);
