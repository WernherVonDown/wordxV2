import React from "react";
import { ILangAuthProps, withLangAuth } from "../../hoc/withLangAuth";
import { SourceContextProvider } from "../../context/SourceContext";
import { Main } from "../../components/Main/Main.component";
import { MainLayout } from "../../components/MainLayout/MainLayout";

const LangaugePage: React.FC<ILangAuthProps> = ({ languageKey }) => {
    return (
        <MainLayout languageKey={languageKey}>
            <SourceContextProvider languageKey={languageKey}>
                <Main />
            </SourceContextProvider>
        </MainLayout>
    )
}

export default withLangAuth(LangaugePage);