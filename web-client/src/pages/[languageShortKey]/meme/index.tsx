import React, { useCallback, useEffect, useState } from "react"
import { MainLayout } from "../../../components/MainLayout/MainLayout";
import { Wallet } from "../../../components/Wallet/Wallet";
import { ILangAuthProps, withLangAuth } from "../../../hoc/withLangAuth";
import MemeService, { IMeme } from "../../../services/memeService";
import { Button } from "@mui/material";

const MemePage: React.FC<ILangAuthProps> = ({ languageKey }) => {
    const [currentMeme, setCurrentMeme] = useState<IMeme>();

    useEffect(() => {
        getMeme()
    }, [])

    const getMeme = useCallback(() => {
        MemeService.getMemeByLanguageKey(languageKey).then(m => setCurrentMeme(m.data)).catch(e => console.log("ERROR meme", e))
    }, [languageKey])

    return <MainLayout languageKey={languageKey}>
        {
            currentMeme ? <img src={currentMeme.image} style={{maxHeight: '600px'}} /> : <div>Loading...</div>
        }
        {currentMeme && <Button variant="contained" onClick={getMeme}>Дальше</Button>}
    </MainLayout>
}

export default withLangAuth(MemePage);