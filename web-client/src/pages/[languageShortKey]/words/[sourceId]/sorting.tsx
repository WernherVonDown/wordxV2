import React from "react";
import { MainLayout } from "../../../../components/MainLayout/MainLayout";
import { SourceContextProvider, useSource } from "../../../../context/SourceContext";
import { ILangAuthProps, withLangAuth } from "../../../../hoc/withLangAuth";
import { SourceWordsSourting } from "../../../../components/SourceWords/SourceWordsSorting.component";

const SourcePage: React.FC<ILangAuthProps> = ({ languageKey }) => {
    return (
        <MainLayout languageKey={languageKey}>
            <SourceContextProvider languageKey={languageKey}>
                <SourceWordsSourting />
            </SourceContextProvider>
        </MainLayout>
    )
}

export default withLangAuth(SourcePage);