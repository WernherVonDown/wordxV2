import React from "react";
import { MainLayout } from "../../../../components/MainLayout/MainLayout";
import { SourceWords } from "../../../../components/SourceWords/SourceWords.component";
import { SourceContextProvider, useSource } from "../../../../context/SourceContext";
import { ILangAuthProps, withLangAuth } from "../../../../hoc/withLangAuth";

const SourcePage: React.FC<ILangAuthProps> = ({ languageKey }) => {
    return (
        <MainLayout languageKey={languageKey}>
            <SourceContextProvider languageKey={languageKey}>
                <SourceWords />
            </SourceContextProvider>
        </MainLayout>
    )
}

export default withLangAuth(SourcePage);