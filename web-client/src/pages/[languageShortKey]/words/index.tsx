import React from "react";
import { MainLayout } from "../../../components/MainLayout/MainLayout";
import { UserWords } from "../../../components/UserWords/UserWords.component";
import { WordsContextProvider } from "../../../context/WordsContext";
import { ILangAuthProps, withLangAuth } from "../../../hoc/withLangAuth";

const WordsPage: React.FC<ILangAuthProps> = ({ languageKey }) => {
    return (
        <MainLayout languageKey={languageKey}>
            <WordsContextProvider languageKey={languageKey}>
                <UserWords />
            </WordsContextProvider>
        </MainLayout>
    )
}

export default withLangAuth(WordsPage);