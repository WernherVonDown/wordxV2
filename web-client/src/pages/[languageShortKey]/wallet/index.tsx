import React from "react"
import { MainLayout } from "../../../components/MainLayout/MainLayout";
import { Wallet } from "../../../components/Wallet/Wallet";
import { ILangAuthProps, withLangAuth } from "../../../hoc/withLangAuth";

const WalletPage: React.FC<ILangAuthProps> = ({ languageKey }) => {
    return <MainLayout languageKey={languageKey}>
        <Wallet />
    </MainLayout>
}

export default withLangAuth(WalletPage);