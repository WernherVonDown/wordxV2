import axios, { AxiosResponse } from "axios";
import getYouTubeID from 'get-youtube-id';
import * as usetube from 'usetube'
import { getSubtitles } from "youtube-caption-extractor";
import { IYoutube } from "../const/source/interfaces";
const GOOGLE_API_KEY = 'AIzaSyB9-OQmS69rnClhTanr7Ac0t73jOkmHEEY';
const GOOGLE_API_URL = 'https://www.googleapis.com/youtube/v3'
const GOOGLE_CLIENT_ID = "763233867358-6tfqb5vq31vgnkd34fthd0anfqs2e8ld.apps.googleusercontent.com"
const CLIENT_SECRET = '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';
const $youtubebeApi = axios.create({
    baseURL: GOOGLE_API_URL,

});

// `/videos?id=7lCDEYXw3mM&key=AIzaSyB9-OQmS69rnClhTanr7Ac0t73jOkmHEEY&part=snippet`

async function auth() {
    const auth_url = `https://accounts.google.com/o/oauth2/v2/auth?scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fyoutube.readonly&include_granted_scopes=true&state=state_parameter_passthrough_value&redirect_uri=https%3A//wordx.ru/local/lol&response_type=token&client_id=${GOOGLE_CLIENT_ID}`
    const res = await $youtubebeApi.get<any>(auth_url);
    console.log("AUTH", res)
}
// auth()

// const test = async () => {
//     const data = await usetube.searchVideo('hello world', 'api-key-123');

//     console.log(data);
// }
console.log("OLO", usetube)
// test()
const fetchSubtitles = async (videoID: string, lang = 'en') => {
    try {
        const response = await fetch(
            `/api/fetch-subtitles?videoID=${videoID}&lang=${lang}`
        );
        const data = await response.json();
        console.log("DATATA", data)
        return data;
    } catch (error) {
        console.error('Error fetching subtitles:', error);
    }
};


export default class YoutubeService {
    static async getVideoInfoByUrl(url: string) {
        console.log("YOUTUBE ID", getYouTubeID(url))
        try {
            const videoId = getYouTubeID(url);
            if (!videoId) {
                throw "Incorrect link"
            }
            const snippet = await this.getVideoInfoById(videoId);
            console.log("RESSS", snippet)
            const { title } = snippet;
            return { title };
        } catch (error) {
            console.log("Youtube.getVideoInfo error", error);
        }
    }
    static async getVideoInfoById(videoId: string): Promise<IYoutube> {
        const res = await $youtubebeApi.get<any>(`/videos?id=${videoId}&key=${GOOGLE_API_KEY}&part=snippet`);
        return res.data.items[0]?.snippet;
    }

    static async getVideoCaptionsById(videoId: string, lang?: string) {
        try {
            const snippet = await this.getVideoInfoById(videoId);
            const data = await fetchSubtitles(videoId, lang);
            
           //await getSubtitles({ videoID: videoId, lang: 'en' });
            console.log("SSSSS", data, snippet)
            return data;
        } catch (error) {
            console.log("Youtube.getVideoCaptionsByUrl error", error);
        }
    }

    static async getVideoCaptionsByUrl(url: string, lang?: string) {
        console.log("YOUTUBE ID", getYouTubeID(url))
        try {
            const videoId = getYouTubeID(url);
            if (!videoId) {
                throw "Incorrect link"
            }
            const snippet = await this.getVideoInfoById(videoId);
            const data = await fetchSubtitles(videoId, lang);
            
           //await getSubtitles({ videoID: videoId, lang: 'en' });
            console.log("SSSSS", data, snippet)
            return data;
            // const res = await this.getVideoCaptionsById(videoId);
            // const snippet = res.data.items;
            // console.log("RESSS CAPTIONS", snippet)
            // const ids = res.data.items.map((i: any) => i.id)
            // console.log("IDS", ids)
            // const subs = await Promise.all(ids.map(this.getVideoCaptionInfoById));
            // console.log("SUBS", subs)
            return {};
        } catch (error) {
            console.log("Youtube.getVideoCaptionsByUrl error", error);
        }
    }

    static async getVideoCaptionInfoById(captionsId: string): Promise<AxiosResponse<any>> {
        return $youtubebeApi.get<any>(`/captions/${captionsId}?key=${GOOGLE_API_KEY}`, {
            headers: {
                // Authorization: `Bearer ${GOOGLE_API_SECRET}`
            }
        });
    }

    // static async getVideoCaptionsById(videoId: string): Promise<AxiosResponse<any>> {
    //     return $youtubebeApi.get<any>(`/captions?videoId=${videoId}&key=${GOOGLE_API_KEY}&part=snippet`);
    // }
}