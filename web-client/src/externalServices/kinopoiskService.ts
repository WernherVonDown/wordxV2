import url from 'url';
const API_URL = 'https://api.kinopoisk.dev';
const TOKEN = 'C3Z9WSW-ZAVMJRK-J74EYD3-Q8H9BXC';
const getParams = (params: {[key: string]: string | number | boolean}) => 
`?token=${TOKEN}&${Object.entries(params).map(([key, value]) => `${key}=${value}`).join('&')}`
const MOVIE_ENDPOINT = `${API_URL}/movie${getParams({
    isStrict: false,
})}`;
//https://github.com/yenbekbay/movie-api
//88e5f2a327b0f0f6e945668580c3880f
//https://api.themoviedb.org/3/movie/550?api_key=88e5f2a327b0f0f6e945668580c3880f
//eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI4OGU1ZjJhMzI3YjBmMGY2ZTk0NTY2ODU4MGMzODgwZiIsInN1YiI6IjYzYmY5ZjllOGVmZTczMDA5NDg4YTdmNiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.PcAnPRzKwggPYfIEomNlWJkfAHuyZVqwa5dfs3jVvlU

export const searchFilm = async (search: string) => {
    const MOVIE_ENDPOINT_API = `${MOVIE_ENDPOINT}&${[
        ['field', 'typeNumber'], ['search', 1],
        ['field', 'name'], ['search', search]
    ].map(([key, value]) => `${key}=${value}`).join('&')}`;
    console.log("ENDPOINT", MOVIE_ENDPOINT_API)
    const res = await fetch(MOVIE_ENDPOINT_API).then(response => response.json());
    console.log("SEARCH RESULT", res)
}

export const getFilmInfo = async (kinopoiskId: string) => {
    const MOVIE_ENDPOINT_API = `${MOVIE_ENDPOINT}&${[
        ['field', 'id'],
        ['search', kinopoiskId],
    ].map(([key, value]) => `${key}=${value}`).join('&')}`;
    console.log("ENDPOINT", MOVIE_ENDPOINT_API)
    const res = await fetch(MOVIE_ENDPOINT_API)//.then(response => response.json());
    console.log("SEARCH RESULT 2", res)
    if (!res.ok) {
        throw new Error()
    }
    return res.json()
}