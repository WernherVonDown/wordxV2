import React, { ReactElement, useContext, useEffect, useLayoutEffect, useMemo } from 'react';
import { IUser } from '../const/user/types';
import { useState, useCallback } from 'react';
import AuthService from '../services/authService';
import $api, { API_URL } from '../http/index';
import axios, { AxiosError } from 'axios';
import { UserApiRoutes } from '../const/user/API_ROUTES';
import { SessionContextProvider } from './SessionContext';
import { useRouter } from 'next/router';
import { LoginRoute, PlansRoute, PUBLIC_ROUTES } from '../const/APP_ROUTES';
import { MediaSocketContextProvider } from './MediaSocketContext';


interface IState {
    loggedIn: boolean;
    user: IUser | undefined;
    isLoading: boolean;
}

interface IProps {
    state: IState,
    children: ReactElement | ReactElement[]
}

interface ContextValue {
    state: IState,
    actions: {
        [key: string]: (...args: any[]) => unknown
    }
}

const AuthContext = React.createContext({} as ContextValue);

const AuthContextProvider = (props: IProps) => {
    const { children } = props;
    const router = useRouter();
    const [loggedIn, setLoggedIn] = useState<boolean>(false);
    const [user, setUser] = useState<IUser>();
    const [isLoading, setLoading] = useState(true);

    useEffect(() => {
        if (!loggedIn && localStorage.getItem('token')) {
            checkAuth()
        } else if (!localStorage.getItem('token')) {
            setLoading(false)
        }
    }, [loggedIn])

    useEffect(() => {
        if (loggedIn) {
            setLoading(false)
        }
    }, [loggedIn])

    useEffect(() => {
        console.log("USER", user)
    }, [user])

    //   useEffect(() => {
    //     if (!loggedIn && !PUBLIC_ROUTES.includes(router.asPath)) {
    //         console.log("РЕДИРЕКТ SHIT")
    //     } else {
    //         console.log("NOT REDIRECT")
    //     }

    //   }, [loggedIn])

    const checkPathAccess = useCallback((url: string) => {
        console.log("checkPathAccess", url, loggedIn)
        // if (!loggedIn && !PUBLIC_ROUTES.includes(url)) {
        //     router.push(LoginRoute)
        // } else {
        //     console.log("NOT REDIRECT")
        // }
    }, [loggedIn])

    const login = useCallback(async (email: string, password: string) => {
        try {
            const response = await AuthService.login(email, password);
            localStorage.setItem('token', response.data.accessToken);
            setLoggedIn(true);
            setUser(response.data.user);
            router.push('/');
        } catch (error) {
            console.log('login error;', error);
        }
    }, [])

    const googleAuth = async (username: string, email: string, accessToken: string) => {
        try {
            const response = await AuthService.googleAuth(email, username, accessToken);
            localStorage.setItem('token', response.data.accessToken);
            setLoggedIn(true);
            setUser(response.data.user);
            return { success: true };
        } catch (error: any) {
            console.log('googleAuth error;', error)
            alert(error.message)
            return { success: false };
        }
    }

    const logout = useCallback(async () => {
        try {
            const response = await AuthService.logout();
            localStorage.removeItem('token');
            setLoggedIn(false);
            setUser(undefined);
        } catch (error) {
            console.log('logout error;', error)
        }
    }, [])

    const activateCode = useCallback(async (activationCode: string, email: string): Promise<{ success: boolean }> => {
        try {
            const response = await AuthService.activate(activationCode, email);
            localStorage.setItem('token', response.data.accessToken);
            setLoggedIn(true);
            setUser(response.data.user);
            router.push(PlansRoute);
            return { success: true };
        } catch (error: any) {
            console.log('activateCode error;', error);
            alert(error.message)
            return { success: false };
        }
    }, [])

    const register = useCallback(async (email: string, userName: string, password: string): Promise<{ success: boolean, activationTimeout?: number }> => {
        try {
            const response = await AuthService.registration(email, userName, password);
            // localStorage.setItem('token', response.data.accessToken);
            // setLoggedIn(true);
            // setUser(response.data.user);
            return { success: true, activationTimeout: response.data.activationTimeout };
        } catch (error: any) {
            console.log('register error;', error)
            alert(error.message)
            return { success: false };
        }
    }, []);

    const resetPasswordConfirm = useCallback(async (email: string, token: string, password: string) => {
        try {
            const response = await AuthService.resetPasswordConfrim(email, token, password);
            return { success: true };
        } catch (error: any) {
            console.log('resetPasswordConfirm error;', error);
            alert(error.message)
            return { success: false };
        }
    }, []);


    const resetPassword = useCallback(async (email: string) => {
        try {
            const response = await AuthService.resetPassword(email);
            return { success: true };
        } catch (error: any) {
            console.log('resetPassword error;', error);
            alert(error.message)
            return { success: false };
        }
    }, []);

    const checkAuth = useCallback(async () => {
        try {
            const response = await $api.get(`${API_URL}/${UserApiRoutes.CHECK_AUTH}`, { withCredentials: true });
            // localStorage.setItem('token', response.data.accessToken);
            setLoggedIn(true);
            setUser(response.data.user);
        } catch (error) {
            console.log('checkAuth error;', error)
        } finally {
            setLoading(false)
        }
    }, [loggedIn])

    const state = useMemo(() => ({
        loggedIn,
        user,
        isLoading,
    }), [
        loggedIn,
        user,
        isLoading,
    ]);

    const actions = {
        login,
        logout,
        register,
        checkAuth,
        resetPassword,
        resetPasswordConfirm,
        googleAuth,
        activateCode,
    }

    const childrenData = useMemo(() => {
        if (loggedIn && user) {
            return <MediaSocketContextProvider userId={user.id}>
                <SessionContextProvider>
                    {children}
                </SessionContextProvider>
            </MediaSocketContextProvider>
        }

        return children
    }, [loggedIn, children, user])


    return <AuthContext.Provider
        value={{
            state,
            actions
        }}
    >
        {childrenData}
    </AuthContext.Provider>
}

AuthContextProvider.defaultProps = {
    state: {
        loggedIn: false,
    }
}

export const useAuth = () => useContext(AuthContext);

export { AuthContext, AuthContextProvider };
