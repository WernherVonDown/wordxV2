import React, { ReactElement, useCallback, useContext, useEffect, useMemo, useState } from 'react';
import { UserApiRoutes } from '../const/user/API_ROUTES';
import { LanguageKeys } from '../const/language/Languagekeys';
import { NextParsedUrlQuery } from 'next/dist/server/request-meta';
import SourceService from '../services/sourceService';
import { AuthContext } from './AuthContext';
import { INotification } from '../const/notifications/interfaces';
import NotificationsService from '../services/notificationsService';
import { useLanguage } from '../hooks/useLanguage';
import { useMediaSocketSubscribe } from '../hooks/useMediaSocketSubscribe';
import { UserEvents } from '../const/user/USER_EVENTS';
import PaymentService from '../services/paymentService';
import { PaymentTypes } from '../const/payment/PaymentTypes';
import UserService from '../services/userService';
import { IPaymentMethod } from '../const/payment/types';
import { IProduct } from '../const/product/interfaces';
import DataService from '../services/dataService';


interface IState {
    notifications: INotification[],
    paymentMethods: IPaymentMethod[],
    products: IProduct[],
}

interface IProps {
    state: IState,
    children: ReactElement | ReactElement[]
}

interface ContextValue {
    state: IState,
    actions: {
        [key: string]: (...args: any[]) => unknown
    }
}

const SessionContext = React.createContext({} as ContextValue);

const READ_NOTIFICATION_TIMEOUT = 800;

const SessionContextProvider = (props: IProps) => {
    const { state: { user }, actions: { checkAuth }} = useContext(AuthContext)
    const { children } = props;
    const [_notifications, setNotifications] = useState(user?.notifications || [])
    const { languageKey } = useLanguage();
    const [paymentMethods, setPaymentMethods] = useState<IPaymentMethod[]>([]);
    const [products, setProducts] = useState<IProduct[]>([])

    useEffect(() => {
        UserService.getPaymentMethods().then(r => {
            setPaymentMethods(r.data.paymentMethods.filter(m => m.paymentMethodId))
        }).catch(e => console.log('getPaymentMethods', e))

        DataService.getProducts().then(r => {
            setProducts(r.data)
        }).catch(e => console.log('getProducts', e))
        
    }, [])

    const notifications = useMemo(() => {
        return _notifications.filter(n => !n.langKey || n.langKey === languageKey)
    }, [_notifications, languageKey])

    const readNotification = useCallback(async (notificationId: string) => {
        await NotificationsService.readNotification(notificationId);
        setTimeout(() => {
            setNotifications(p => p.map(n => {
                    if (n.id === notificationId) {
                        n.read = true;
                    }
                    return n
            }))
        }, READ_NOTIFICATION_TIMEOUT)
    }, [])

    const buyCoins = useCallback(async (amount: number, paymentMethodId?: string) => {
        try {
            const res = await PaymentService.buyCoins(PaymentTypes.wallet, amount, paymentMethodId);
            console.log("BUY REs", res)
            if (typeof res.data.redirect === 'string') {
                window.open(res.data.redirect, '_blank')?.focus()
                return;
            }
        } catch (error) {
            console.log("error", error)
        }
    }, []);



    const onGotNotification = useCallback((notification: INotification) => {
        setNotifications(p => [notification, ...p])
    }, [])

    const onRefresh = useCallback(() => {
        checkAuth()
    }, [])

    useMediaSocketSubscribe(UserEvents.NOTIFICATION, onGotNotification)
    useMediaSocketSubscribe(UserEvents.NOTIFICATION, onRefresh)

    const state = useMemo(() => ({
        notifications,
        paymentMethods,
        products,
    }), [
        notifications,
        paymentMethods,
        products,
    ]);

    const actions = {
        readNotification,
        buyCoins,
    }


    return <SessionContext.Provider
        value={{
            state,
            actions
        }}
    >
        {children}
    </SessionContext.Provider>
}

SessionContextProvider.defaultProps = {
    state: {
    }
}

export const useSession = () => useContext(SessionContext);

export { SessionContext, SessionContextProvider };
