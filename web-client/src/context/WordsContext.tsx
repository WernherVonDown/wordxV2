import React, { ReactElement, useCallback, useContext, useEffect, useMemo, useState } from 'react';
import { TRUE } from 'sass';
import { LanguageKeys } from '../const/language/Languagekeys';
import { ISortedWord } from '../const/words/interfaces';
import WordsService from '../services/wordsService';


interface IState {
}

interface IProps {
    state: IState,
    children: ReactElement | ReactElement[],
    languageKey: LanguageKeys;
}

interface ContextValue {
    state: IState,
    actions: {
        [key: string]: (...args: any[]) => any
    }
}

const WordsContext = React.createContext({} as ContextValue);

const WordsContextProvider = (props: IProps) => {
    const { children, languageKey } = props;

    const getWords = useCallback(async () => {
        try {
            console.log("GET WORDS")
            const res = await WordsService.getWords(languageKey);
            const { learningWords, knownWordsCount, learnedWordsCount, notSortedWordsCount } = res.data;
            console.log("RES", res.data)
            return {success: true, learningWords, knownWordsCount, learnedWordsCount, notSortedWordsCount }
        } catch (error: any) {
            console.log("getWords error", error)
            alert(error.message)
            return {success: false }
        }
    }, [languageKey])

    const getUserWordsSources = useCallback(async () => {
        try {
            const res = await WordsService.getUserWordsSources(languageKey);
            const { userWordsSources } = res.data;
            return {success: true, userWordsSources }
        } catch (error: any) {
            console.log("getUserWordsSources error", error)
            alert(error.message)
            return {success: false }
        }
    }, [languageKey])

    const sendLearnedWords = useCallback(async (words: string[]) => {
        try {
            const res = await WordsService.changeLearnedWords(words);
            return {success: true }
        } catch (error: any) {
            console.log("getWords error", error)
            alert(error.message)
            return {success: false }
        }
    }, [languageKey])

    

    const state = useMemo(() => ({
    }), [
    ]);

    const actions = {
        getWords,
        sendLearnedWords,
        getUserWordsSources,
    }


    return <WordsContext.Provider
        value={{
            state,
            actions
        }}
    >
        {children}
    </WordsContext.Provider>
}

WordsContextProvider.defaultProps = {
    state: {
    }
}

export const useWords = () => useContext(WordsContext)

export { WordsContext, WordsContextProvider };
