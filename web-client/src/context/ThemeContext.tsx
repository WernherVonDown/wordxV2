import React, { ReactElement, useMemo } from 'react';
import { responsiveFontSizes } from '@mui/material';
import { ThemeProvider, createTheme, Theme } from '@mui/material/styles';

interface IState {
    theme: Theme;
}

interface IProps {
    state: IState,
    children: ReactElement | ReactElement[]
}

interface ContextValue {
    state: IState,
    actions: {
        [key: string]: (...args: any[]) => unknown
    }
}

const ThemeContext = React.createContext({} as ContextValue);

const ThemeContextProvider = (props: IProps) => {
    const { children } = props;

    const theme = useMemo(() => {
        const theme = createTheme({
            palette: {
                //@ts-ignore
                type: 'light',
                primary: {
                  main: '#560DF0',
                },
                secondary: {
                  main: '#a5a1ae',
                },
                text: {
                    primary: '#212125',
                }
              },
              // shadows: [''],
              typography: {
                fontFamily: 'Inter',
                button: {
                    textTransform: 'none'
                  }
              },
              overrides: {
                MuiAppBar: {
                  colorInherit: {
                    // backgroundColor: '#680000',
                    color: '#a5a1ae',
                    shadow: 'none',
                  },
                },
              },
              props: {
                MuiAppBar: {
                  color: '#a5a1ae',
                  shadow: 'none',
                }
              },
              shape: {
                borderRadius: 8,
              },
        }
          );
        return theme;
    }, [])

    const state = useMemo(() => ({
        theme,
    }), [
        theme,
    ]);

    const actions = {
    }

   


    return <ThemeContext.Provider
        value={{
            state,
            actions
        }}
    >
        <ThemeProvider theme={theme}>
            {children}
        </ThemeProvider>
    </ThemeContext.Provider>
}

ThemeContextProvider.defaultProps = {
    state: {
    }
}


export { ThemeContext, ThemeContextProvider };
