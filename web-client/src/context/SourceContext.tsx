import React, { ReactElement, useCallback, useContext, useEffect, useMemo, useState } from 'react';
import { LanguageKeys } from '../const/language/Languagekeys';
import { ProductTypes } from '../const/product/ProductTypes';
import { ISource } from '../const/source/interfaces';
import { ISortedWord } from '../const/words/interfaces';
import SourceService from '../services/sourceService';
import WordsService from '../services/wordsService';
import { useSession } from './SessionContext';
import { SourceTypes } from '../const/source/SourceTypes';
import { ISongSearch } from '../const/music/interfaces';


interface IState {
    sources: ISource[],
    statisticsPrice: number,
    subtitlesPrice: number,
    booksPrice: number,
}

interface IProps {
    state: IState,
    children: ReactElement | ReactElement[],
    languageKey: LanguageKeys;
}

interface ContextValue {
    state: IState,
    actions: {
        [key: string]: (...args: any[]) => any
    }
}

const SourceContext = React.createContext({} as ContextValue);

const SourceContextProvider = (props: IProps) => {
    const { children, languageKey } = props;
    const [sources, setSources] = useState<ISource[]>([]);
    const { state: { products } } = useSession()

    const uploadSource = useCallback(async (formData: FormData, sourceType: SourceTypes) => {
        try {
            formData.append('type', sourceType)
            const res = await SourceService.create(formData, languageKey);
            return { success: true }
        } catch (error: any) {
            console.log('uploadSource error;', error)
            alert(error.response?.data?.message || error.message)
            return { success: false }
        }
    }, [languageKey])

    useEffect(() => {
        if (languageKey) {
            setSources([])
        }
    }, [languageKey])

    const getList = useCallback(async (page: number = 0, onEnd?: () => void) => {
        try {
            const res = await SourceService.getList(languageKey, page);
            if (!res.data.sources.length && onEnd) {
                onEnd()
            }
            setSources(p => [...p, ...res.data.sources]);
        } catch (error) {
            console.log('getList error;', error)
        }
    }, [languageKey])

    const getWords = useCallback(async (sourceId: string): Promise<{ success: boolean, words: any[] }> => {
        try {
            const res = await SourceService.getWords(sourceId);
            return { success: true, words: res.data.words }
        } catch (error: any) {
            console.log('getWords error;', error)
            alert(error.message)
            return { success: false, words: [] }
        }
    }, [])

    const statisticsProduct = useMemo(() => products.find(p => p.key === ProductTypes.statistics), [products])
    const subtitlesProduct = useMemo(() => products.find(p => p.key === ProductTypes.subtitles), [products])
    const booksProduct = useMemo(() => products.find(p => p.key === ProductTypes.books), [products])

    const getWordsStatistics = useCallback(async (sourceId: string): Promise<{ success: boolean, knownPercent?: number, notSortedPercent?: number, learningPercent?: number, allKnownWordsPercent?: number, allLearningWordsPercent?: number, allNotSortedWordsPercent?: number, allSortingPercent?: number, sortingPercent?: number }> => {
        try {
            const productId = statisticsProduct?.id;
            const res = await SourceService.getWordsStatistics(sourceId, productId || '');
            return {
                success: true,
                knownPercent: res.data.knownPercent,
                notSortedPercent: res.data.notSortedPercent,
                learningPercent: res.data.learningPercent,
                allKnownWordsPercent: res.data.allKnownWordsPercent,
                allLearningWordsPercent: res.data.allLearningWordsPercent,
                allNotSortedWordsPercent: res.data.allNotSortedWordsPercent,
                allSortingPercent: res.data.allSortingPercent,
                sortingPercent: res.data.sortingPercent,
            }
        } catch (error: any) {
            console.log('getWordsStatistics error;', error)
            alert(error.message)
            return { success: false }
        }
    }, [statisticsProduct])

    const onSearchMusic = useCallback(async (q: string): Promise<{success: boolean, songs?: ISongSearch[]}> => {
        try {
            const res = await SourceService.searchMusic(q);
            return {
                success: true,
                songs: res.data,
            }
        } catch (error: any) {
            console.log('getWordsStatistics error;', error)
            alert(error.message)
            return { success: false }
        }
    }, [])

    const onGetSongLyrics = useCallback(async (id: number): Promise<{success: boolean, lyrics?: string}> => {
        try {
            const res = await SourceService.getSongLyrics(id);
            return {
                success: true,
                lyrics: res.data,
            }
        } catch (error: any) {
            console.log('getWordsStatistics error;', error)
            alert(error.message)
            return { success: false }
        }
    }, [])

    const onGetSong = useCallback(async (id: number): Promise<{success: boolean, song?: ISongSearch}> => {
        try {
            const res = await SourceService.getSong(id);
            return {
                success: true,
                song: res.data,
            }
        } catch (error: any) {
            console.log('getWordsStatistics error;', error)
            alert(error.message)
            return { success: false }
        }
    }, [])

    const sendSortedWords = useCallback(async ({ knownWords, learningWords, sourceId,needWords }: { knownWords: ISortedWord[], learningWords: ISortedWord[], sourceId: string, needWords?: boolean }) => {
        try {
            const res = await WordsService.sendSourtedWords(learningWords, knownWords, sourceId, needWords)
            return res.data.words || []
        } catch (error) {
            console.log('sendSortedWords error;', error)
        }
    }, [])

    const getAutoSubtitiles = useCallback(async (imdbId: string) => {
        try {
            const productId = subtitlesProduct?.id;
            const res = await SourceService.getSubtitles(imdbId, languageKey, productId || '');
            return { success: true, subtitleLink: res.data.subtitleLink }
        } catch (error: any) {
            console.log('getAutoSubtitiles error;', error)
            alert(error.response?.data?.message || error.message)
            return { success: false }
        }
    }, [languageKey, subtitlesProduct])

    const getBook = useCallback(async (bookId: string) => {
        try {
            const productId = booksProduct?.id;
            console.log("book product", booksProduct)
            const res = await SourceService.getBook(bookId, languageKey, productId || '');
            return { success: true, bookLink: res.data.bookLink }
        } catch (error: any) {
            console.log('getAutoSubtitiles error;', error)
            alert(error.message)
            return { success: false }
        }
    }, [languageKey, booksProduct])

    const state = useMemo(() => ({
        sources,
        statisticsPrice: statisticsProduct?.price || 0,
        subtitlesPrice: subtitlesProduct?.price || 0,
        booksPrice: booksProduct?.price || 0,
    }), [
        sources,
        statisticsProduct,
        subtitlesProduct,
        booksProduct,
    ]);

    const actions = {
        uploadSource,
        getList,
        getWords,
        sendSortedWords,
        getWordsStatistics,
        getAutoSubtitiles,
        getBook,
        onSearchMusic,
        onGetSongLyrics,
        onGetSong,
    }


    return <SourceContext.Provider
        value={{
            state,
            actions
        }}
    >
        {children}
    </SourceContext.Provider>
}

SourceContextProvider.defaultProps = {
    state: {
    }
}

export const useSource = () => useContext(SourceContext)

export { SourceContext, SourceContextProvider };
