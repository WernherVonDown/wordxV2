import { AxiosResponse } from "axios";
import { LanguageKeys } from "../const/language/Languagekeys";
import { changeLearnedWords, getUserWordsSourcesApiRoute, getWordsApiRoute, sendSortedWords } from "../const/words/API_ROUTES";
import { ISortedWord, IUserWordsSourcesResponse, IWordsListResponse } from "../const/words/interfaces";
import $api from "../http";

export default class WordsService {
    static async sendSourtedWords(learningWords: ISortedWord[], knownWords: ISortedWord[], sourceId: string, needWords?: boolean): Promise<AxiosResponse<any>> {
        return $api.post<any>(sendSortedWords, {
            learningWords,
            knownWords,
            sourceId,
            needWords,
        });
    }
    
    static async getWords(languageKey: LanguageKeys): Promise<AxiosResponse<IWordsListResponse>> {
        return $api.get<IWordsListResponse>(getWordsApiRoute(languageKey));
    }

    static async getUserWordsSources(languageKey: LanguageKeys): Promise<AxiosResponse<IUserWordsSourcesResponse>> {
        return $api.get<IUserWordsSourcesResponse>(getUserWordsSourcesApiRoute(languageKey));
    }

    static async changeLearnedWords(words: string[]): Promise<AxiosResponse<any>> {
        return $api.post<any>(changeLearnedWords, {
            learnedWordsNew: words,
        });
    }
}