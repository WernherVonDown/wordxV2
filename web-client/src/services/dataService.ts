import { AxiosResponse } from "axios";
import { LanguageLevelsRoute, LanguagesRoute, ProductsRoute, SubscriptionPlansRoute } from "../const/API_DATA_ROUTES";
import { ILanguage, ILanguageLevel } from "../const/language/interfaces";
import { IProduct } from "../const/product/interfaces";
import { ISubscriptionPlan } from "../const/user/types";
import $api from "../http";

export default class DataService {
    static async getSubscriptionPlans(): Promise<AxiosResponse<ISubscriptionPlan[]>> {
        return $api.get<ISubscriptionPlan[]>(SubscriptionPlansRoute);
    }

    static async getLanguages(): Promise<AxiosResponse<ILanguage[]>> {
        return $api.get<ILanguage[]>(LanguagesRoute);
    }

    static async getLanugageLevels(): Promise<AxiosResponse<ILanguageLevel[]>> {
        return $api.get<ILanguageLevel[]>(LanguageLevelsRoute);
    }

    static async getProducts(): Promise<AxiosResponse<IProduct[]>> {
        return $api.get<IProduct[]>(ProductsRoute);
    }
}
