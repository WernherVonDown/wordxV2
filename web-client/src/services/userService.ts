import { AxiosResponse } from "axios";
import { PaymentTypes } from "../const/payment/PaymentTypes";
import { IPaymentMethod } from "../const/payment/types";
import { UserApiRoutes } from "../const/user/API_ROUTES";
import { ISubscriptionPlan } from "../const/user/types";
import $api from "../http";
export default class UserService {
    static async changeSubscriptionPlan(subscriptionPlanId: string, paymentType: PaymentTypes): Promise<AxiosResponse<{redirect?: string}>> {
        return $api.post<{redirect?: string}>(UserApiRoutes.CHANGE_SUBSCRIPTION_PLAN, { subscriptionPlanId, paymentType });
    }

    static async changeLanguage({ languageId, remove }: { languageId: string, remove?: boolean }): Promise<AxiosResponse> {
        return $api.post(UserApiRoutes.CHANGE_LANGUAGE, { languageId, remove });
    }

    static async getPaymentMethods(): Promise<AxiosResponse<{paymentMethods: IPaymentMethod[]}>> {
        return $api.get<{paymentMethods: IPaymentMethod[]}>(UserApiRoutes.PAYMENT_METHODS);
    }
}