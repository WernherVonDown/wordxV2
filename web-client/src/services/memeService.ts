import { AxiosResponse } from "axios";
import { LanguageKeys } from "../const/language/Languagekeys";
import $api from "../http";

export interface IMeme {
    image: string,
    category?: string,
    captions: string,
    id: string,
}

const BASE = 'meme';

const getMemeByLanguageKeyRouter = (languageKey: LanguageKeys) => `${BASE}/${languageKey}`;

export default class MemeService {
    static async getMemeByLanguageKey(languageKey: LanguageKeys): Promise<AxiosResponse<IMeme>> {
        return $api.get<IMeme>(getMemeByLanguageKeyRouter(languageKey));
    }
}
