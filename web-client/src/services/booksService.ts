import { AxiosResponse } from "axios";
import { LanguageKeys } from "../const/language/Languagekeys";
import $api from "../http";
import { IBookDto, IBooksResponse } from "../const/books/interfaces";


const BASE = 'books';

const getBooksByLanguageKeyRoute = (languageKey: LanguageKeys) => `${BASE}/${languageKey}`;
const getBooksByIdRoute = (languageKey: LanguageKeys, bookId: number | string) => `${BASE}/${languageKey}/${bookId}`;

export default class BooksService {
    static async getBooksByLanguageKey(languageKey: LanguageKeys, search: string = '', page: number = 1): Promise<AxiosResponse<IBooksResponse>> {
        return $api.get<IBooksResponse>(getBooksByLanguageKeyRoute(languageKey), {
            params: {
                search,
                page,
            }
        });
    }

    static async getBookById(languageKey: LanguageKeys, bookId: string | number): Promise<AxiosResponse<IBookDto>> {
        return $api.get<IBookDto>(getBooksByIdRoute(languageKey, bookId));
    }
}
