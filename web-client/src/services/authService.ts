import { AxiosResponse } from "axios";
import { UserApiRoutes } from "../const/user/API_ROUTES";
import { IAuthResponse, IRegisterResponse } from "../const/user/auth/types";
import $api from "../http";

export default class AuthService {
    static async login(email: string, password: string): Promise<AxiosResponse<IAuthResponse>> {
        return $api.post<IAuthResponse>(UserApiRoutes.LOGIN, { email, password });
    }

    static async activate(activationCode: string, email: string): Promise<AxiosResponse<IAuthResponse>> {
        return $api.post<IAuthResponse>(UserApiRoutes.ACTIVATE, { activationCode, email });
    }

    static async registration(email: string, userName: string, password: string): Promise<AxiosResponse<IRegisterResponse>> {
        return $api.post<IRegisterResponse>(UserApiRoutes.REGISTRATION, { email, password, userName });
    }

    static async logout(): Promise<void> {
        return $api.post(UserApiRoutes.LOGOUT);
    }

    static async resetPasswordConfrim(email: string, token: string, password: string): Promise<AxiosResponse<void>> {
        return $api.post(UserApiRoutes.RESET_PASSWORD_CONFIRM, { email, password, token });
    }

    static async resetPassword(email: string): Promise<AxiosResponse<IAuthResponse>> {
        return $api.post<IAuthResponse>(UserApiRoutes.RESET_PASSWORD, { email });
    }

    static async googleAuth(email: string, username: string, accessToken: string): Promise<AxiosResponse<IAuthResponse>> {
        return $api.post<IAuthResponse>(UserApiRoutes.GOOGLE_AUTH, { username, email, accessToken });
    }

}