import { AxiosResponse } from "axios";
import { LanguageKeys } from "../const/language/Languagekeys";
import { IGetSourceListResponse, IGetSourceSubttileResponse, IGetSourceWordsResponse, IGetSourceWordsStatisticsResponse, ISource } from "../const/source/interfaces";
import { getCreateSourceApiRoute, getListSourcesApiRoute, getMusicApiRoute, getSongApiRoute, getSongLyricsApiRoute, getSourceBookApiRoute, getSourceSubtitlesApiRoute, getSourcesWordsStatisticsApiRoute, getSourceWordsApiRoute, SourceApiRoutes } from "../const/source/SourceApiRoutes";
import $api from "../http";
import { ISongSearch } from "../const/music/interfaces";

export default class SourceService {
    static async create(formData: FormData, languageKey: LanguageKeys): Promise<AxiosResponse<any>> {
        return $api.post<any>(getCreateSourceApiRoute(languageKey), formData);
    }

    static async getList(languageKey: LanguageKeys, page: number): Promise<AxiosResponse<IGetSourceListResponse>> {
        return $api.get<IGetSourceListResponse>(getListSourcesApiRoute(languageKey), {params: {page}});
    }

    static async getWords(sourceId: string): Promise<AxiosResponse<IGetSourceWordsResponse>> {
        return $api.get<IGetSourceWordsResponse>(getSourceWordsApiRoute(sourceId));
    }

    static async getWordsStatistics(sourceId: string, productId: string): Promise<AxiosResponse<IGetSourceWordsStatisticsResponse>> {
        return $api.get<IGetSourceWordsStatisticsResponse>(getSourcesWordsStatisticsApiRoute(sourceId), { params: { productId } });
    }
    
    static async getSubtitles(imdbId: string, languageKey: LanguageKeys, productId: string): Promise<AxiosResponse<IGetSourceSubttileResponse>> {
        return $api.post<IGetSourceSubttileResponse>(getSourceSubtitlesApiRoute(languageKey), { imdbId }, { params: { productId } });
    }

    static async getBook(bookId: string, languageKey: LanguageKeys, productId: string): Promise<AxiosResponse<{bookLink: string}>> {
        return $api.post<{bookLink: string}>(getSourceBookApiRoute(languageKey), { bookId }, { params: { productId } });
    }

    static async searchMusic(q: string): Promise<AxiosResponse<ISongSearch[]>> {
        return $api.get<ISongSearch[]>(getMusicApiRoute(), { params: { q } }, );
    }
    static async getSongLyrics(id: number): Promise<AxiosResponse<string>> {
        return $api.get<string>(getSongLyricsApiRoute(id), {}, );
    }
    static async getSong(id: number): Promise<AxiosResponse<ISongSearch>> {
        return $api.get<ISongSearch>(getSongApiRoute(id), {}, );
    }
}
