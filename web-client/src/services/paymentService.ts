import { AxiosResponse } from "axios";
import { ILanguage, ILanguageLevel } from "../const/language/interfaces";
import { PaymentRoute } from "../const/payment/API_ROUTES";
import { PaymentTypes } from "../const/payment/PaymentTypes";
import { ISubscriptionPlan } from "../const/user/types";
import $api from "../http";

export default class PaymentService {
    static async buyCoins(paymentType: PaymentTypes, amount: number, paymentMethodId?: string): Promise<AxiosResponse<any>> {
        return $api.post<any>(PaymentRoute, { paymentType, amount, paymentMethodId });
    }
}