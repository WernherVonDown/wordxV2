import { AxiosResponse } from "axios";
import { LanguageLevelsRoute, LanguagesRoute, SubscriptionPlansRoute } from "../const/API_DATA_ROUTES";
import { ILanguage, ILanguageLevel } from "../const/language/interfaces";
import { getReadNotificationRoute } from "../const/notifications/API_NOTIFICATOINS_ROUTES";
import { ISubscriptionPlan } from "../const/user/types";
import $api from "../http";

export default class NotificationsService {
    static async readNotification(id: string): Promise<AxiosResponse<any>> {
        return $api.post<any>(getReadNotificationRoute(id));
    }
}