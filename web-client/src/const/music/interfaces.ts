export interface ISongSearch {
    partial: boolean;
    title: string;
    fullTitle: string;
    featuredTitle: string;
    id: number;
    thumbnail: string;
    image: string;
    url: string;
    endpoint: string;
    artist: any;
    album?: any;
    releasedAt?: Date;
    instrumental: boolean;
    header_image_thumbnail_url: string;
    artist_names: string;
}