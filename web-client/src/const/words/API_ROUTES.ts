import { LanguageKeys } from "../language/Languagekeys"

export const sendSortedWords = `/user/addWords`

export const changeLearnedWords = `/words/learnedWords`;

export const getWordsApiRoute = (languageKey: LanguageKeys) => `/words/${languageKey}`;

export const getUserWordsSourcesApiRoute = (languageKey: LanguageKeys) => `/words/sources/${languageKey}`;