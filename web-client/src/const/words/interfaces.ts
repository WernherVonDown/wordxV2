import { ISource } from "../source/interfaces";

export interface ISortedWord {
    count: number;
    percent: number;
    word: string;
}

export interface IWord {
    id: string;
    text: string;
    translation: string;
}

export interface IWordsListItem {
    count: number;
    sources: string[];
    word: IWord;
    countBySource?: {
        [key: string]: number;
    }
}

export interface IWordsListResponse {
    learningWords: IWordsListItem;
    knownWordsCount: number;
    learnedWordsCount: number;
    notSortedWordsCount: number;
}

export interface IUserWordsSources {
    id: string;
    source: ISource;
    knownCount: number;
}

export interface IUserWordsSourcesResponse {
    userWordsSources: IUserWordsSources;
}
