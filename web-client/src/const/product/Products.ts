import { IProduct } from "./interfaces";
import { ProductTypes } from "./ProductTypes";

export const Products: IProduct[] = [
    {
        title: 'Статисктика источника',
        key: ProductTypes.statistics,
        price: 5,
        id: ''
    },
    {
        title: 'Субтитры',
        key: ProductTypes.subtitles,
        price: 49,
        id: ''
    },
]
