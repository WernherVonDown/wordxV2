export enum ProductTypes {
    statistics = 'statistics',
    subtitles = 'subtitles',
    books = 'books',
}