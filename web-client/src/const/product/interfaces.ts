import { ProductTypes } from "./ProductTypes";

export interface IProduct {
    title: string,
    key: ProductTypes,
    price: number,
    id: string,
}
