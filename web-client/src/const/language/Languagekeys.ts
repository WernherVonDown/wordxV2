export enum LanguageKeys {
    english = 'english',
    german = 'german',
    french = 'french',
}

export enum LanguageShortKeys {
    english = 'en',
    german = 'de',
    french = 'fr',
}

export const LanguageRoutes = {
    [LanguageKeys.english]: LanguageShortKeys.english, 
    [LanguageKeys.german]: LanguageShortKeys.german, 
    [LanguageKeys.french]: LanguageShortKeys.french,
}

export const LanguageShortRoute = {
    [LanguageShortKeys.english]: LanguageKeys.english, 
    [LanguageShortKeys.german]: LanguageKeys.german, 
    [LanguageShortKeys.french]: LanguageKeys.french,
}

export const isLanguageKeyExists = (langKey: any) => [LanguageKeys.english, LanguageKeys.german, LanguageKeys.french].includes(langKey);
export const isLanguageRouteKeyExists = (langKey: any) => [LanguageShortKeys.english, LanguageShortKeys.german, LanguageShortKeys.french].includes(langKey);

export const languageRouteToKey = (langKey: LanguageShortKeys) =>  LanguageShortRoute[langKey];

export const langugeKeyToShortRoute = (langKey: LanguageKeys) =>  LanguageRoutes[langKey];