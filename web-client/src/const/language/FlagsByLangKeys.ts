import { LanguageKeys } from "./Languagekeys";

export const FlagsByLangKeys = {
    [LanguageKeys.english]: '/img/flags/en.png',
    [LanguageKeys.german]: '/img/flags/de.png',
    [LanguageKeys.french]: '/img/flags/fr.png',
}
