import { LanguageKeys } from "./Languagekeys";

export interface ILanguage {
    id: string;
    label: string;
    key: LanguageKeys;
}

export interface ILanguageLevel {
    label: string;
    id: string;
    key: string;
    description: string;
    levelCount: number;
}

export interface IUserLanguage {
    language: ILanguage;
    id: string;
    level: ILanguageLevel;
}
