export interface IBookDto {
    id: number,
    title: string,
    author: string,
    cover: string,
    popularity?: number,
}

export interface IBooksResponse {
    page: number,
    count: number,
    next: boolean,
    previos: boolean,
    results: IBookDto[],
}
