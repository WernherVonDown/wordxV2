const BASE = 'data'

export const SubscriptionPlansRoute = `${BASE}/subscriptionPlans`;
export const LanguagesRoute = `${BASE}/languages`;
export const LanguageLevelsRoute = `${BASE}/languageLevels`;
export const ProductsRoute = `${BASE}/products`;
