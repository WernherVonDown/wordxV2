export enum SourceCategoryKeys {
    movies = 'movies',
    series = 'series',
    books = 'books',
}
