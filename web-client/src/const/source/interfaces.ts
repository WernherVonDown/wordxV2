import { LanguageShortKeys } from "../language/Languagekeys";
import { ILanguage } from "../language/interfaces";
import { IWord } from "../words/interfaces";

export interface ISourceType {
    key: string;
    label: string;
    id: string;
}

export interface ISourcePoster {
    id: string;
    url: string;
}

export interface ISource {
    id: string;
    language: ILanguage;
    poster: ISourcePoster;
    title: string;
    totalWordsCount: number;
    uniqueWordsCount: number;
    type: ISourceType;
    posterUrl: string;
    imdbId: string;
    kinopoiskId: number;
    titleOriginal: string;
}

export interface IGetSourceListResponse {
    sources: ISource[];
}

export interface IGetSourceSubttileResponse {
    subtitleLink: string;
}

export interface ISourceWord {
    count: number;
    percent: number;
    word: IWord;
    percentStat?: string;
}

export interface IGetSourceWordsResponse {
    words: ISourceWord[];
}

interface IYThumbnail {
    url: string,
    width: number,
    height: number
}
export interface IYoutube {
    publishedAt: string,
    channelId: string,
    title: string,
    description: string,
    thumbnails: {
        default: IYThumbnail,
        medium: IYThumbnail,
        high: IYThumbnail,
        standard: IYThumbnail,
        maxres: IYThumbnail
    },
    channelTitle: string,
    tags: string[],
    categoryId: string,
    liveBroadcastContent: string,
    defaultLanguage: LanguageShortKeys,
    localized: {
        title: string,
        description: string
    },
    defaultAudioLanguage: LanguageShortKeys
}

export interface IGetSourceWordsStatisticsResponse {
    knownPercent: number;
    notSortedPercent: number;
    learningPercent: number;
    allKnownWordsPercent:number;
    allLearningWordsPercent:number;
    allNotSortedWordsPercent:number;
    allSortingPercent: number,
    sortingPercent: number,
}

export interface IKinopoiskFilm {
    nameRu: string;
    nameEn: string;
    nameOriginal: string;
    posterUrl: string;
    posterUrlPreview: string;
    ratingKinopoisk: string;
    filmId: string;
    year: string;
    imdbId: string;
    kinopoiskId: string
  }

  export interface IKinopoiskFilmDev {
    alternativeName: string,
    name: string,
    poster: {
        url: string,
        previewUrl: string,
    };
    rating: {
        imdb: number | string,
        kp: number | string,
    };
    year: number | string;
    spokenLanguages: {
        name: string,
        nameEn: string,
    }[];
    description: string,
    externalId: {
        imdb?: string,
    }
  }