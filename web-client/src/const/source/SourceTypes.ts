export enum SourceTypes {
    film = 'film',
    book = 'book',
    music = 'music',
    youtube = 'youtube',
}
