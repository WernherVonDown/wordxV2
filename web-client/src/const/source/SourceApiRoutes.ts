import { LanguageKeys } from "../language/Languagekeys"

const BASE = 'source'

export const SourceApiRoutes = {
    CREATE:`${BASE}/create`,
}

export const getCreateSourceApiRoute = (languageKey: LanguageKeys) => `${BASE}/create/${languageKey}`;

export const getListSourcesApiRoute = (languageKey: LanguageKeys) => `${BASE}/list/${languageKey}`;

export const getSourceWordsApiRoute = (sourceId: string) => `${BASE}/words/${sourceId}`;

export const getSourcesWordsStatisticsApiRoute = (sourceId: string) => `${BASE}/statistics/${sourceId}`;

export const getSourceSubtitlesApiRoute = (languageKey: LanguageKeys) => `${BASE}/subtitles/${languageKey}`;

export const getSourceBookApiRoute = (languageKey: LanguageKeys) => `${BASE}/book/${languageKey}`;

export const getMusicApiRoute = () =>  `${BASE}/music`;

export const getSongApiRoute = (id: number) =>  `${BASE}/music/${id}`;

export const getSongLyricsApiRoute = (id: number) =>  `${BASE}/music/${id}/lyrics`;
