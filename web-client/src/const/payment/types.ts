export interface IPaymentMethod {
    paymentDetails: {
        card: {
            card_type: string,
            expiry_month: string,
            expiry_year: string,
            first6: string,
            issuer_country: string,
            last4: string,
        },
        title: string,
    },
    paymentMethodId: string,
}

