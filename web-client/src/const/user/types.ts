import { IUserLanguage } from "../language/interfaces";
import { INotification } from "../notifications/interfaces";

export interface IUser {
    email: string;
    isActivated: boolean;
    id: string;
    languages: IUserLanguage[];
    subscriptionPlan: ISubscriptionPlan;
    sources: string[];
    notSourtedSources: string[]
    notifications: INotification[],
    wallet: number,
}

export interface ISubscriptionPlan {
    id: string;
    level: number;
    currency: string;
    discount: number;
    maxLangs: number;
    price: number;
    title: string;
    benifits: string[];
    bonusCoef: number;
}