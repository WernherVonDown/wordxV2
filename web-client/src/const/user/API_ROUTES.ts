const BASE = 'user'

export const UserApiRoutes = {
    LOGIN:`${BASE}/login`,
    REGISTRATION:`${BASE}/registration`,
    LOGOUT:`${BASE}/logout`,
    REFRESH:`${BASE}/refresh`,
    CHECK_AUTH: `${BASE}/checkAuth`,
    RESET_PASSWORD: `${BASE}/reset-password`,
    RESET_PASSWORD_CONFIRM: `${BASE}/reset-password-confirm`,
    GOOGLE_AUTH: `${BASE}/googleAuth`,
    ACTIVATE: `${BASE}/activate`,
    CHANGE_SUBSCRIPTION_PLAN: `${BASE}/changeSubscriptionPlan`,
    CHANGE_LANGUAGE: `${BASE}/changeLanguage`,
    PAYMENT_METHODS: `${BASE}/paymentsMethods`
}