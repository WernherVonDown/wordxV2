const BASE = 'notifications'

export const getReadNotificationRoute = (id: string) => `${BASE}/${id}/read`;