import { LanguageKeys } from "../language/Languagekeys";

export interface INotification {
    id: string,
    text: string,
    read?: boolean,
    langKey: LanguageKeys,
}