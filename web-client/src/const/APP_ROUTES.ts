import { LanguageKeys, languageRouteToKey, LanguageShortKeys, LanguageShortRoute, langugeKeyToShortRoute } from "./language/Languagekeys";

export const RemindPasswordRoute = '/remind-password';
export const LoginRoute = '/login';
export const RegistrationRoute = '/registration';
export const DashBoardRoute = '/dashboard'
export const PlansRoute = '/plans';
export const LanguagesRoute = '/languages'

export const PUBLIC_ROUTES = [RemindPasswordRoute, LoginRoute, RegistrationRoute];

export const getLanguageRoute = (language: LanguageShortKeys) => `/${language}`;

export const getLanguageRouteByKey = (language: LanguageKeys) => `/${langugeKeyToShortRoute(language)}`;

export const getUploadSourceRouteByKey = (language: LanguageKeys) => `/${langugeKeyToShortRoute(language)}/upload-source`;

export const getUploadFilmRouteByKey = (language: LanguageKeys) => `/${getUploadSourceRouteByKey(language)}/film`;

export const getUploadFilmItemRouteByKey = (language: LanguageKeys, filmId: string) => `/${getUploadFilmRouteByKey(language)}/${filmId}`;

export const getSourceWordsRoute = (language: LanguageKeys, sourceId: string) => `/${langugeKeyToShortRoute(language)}/words/${sourceId}`;

export const getSourceWordsSortingRoute = (language: LanguageKeys, sourceId: string) => `/${getSourceWordsRoute(language, sourceId)}/sorting`;

export const getMyWordsRoute = (language: LanguageKeys) => `/${langugeKeyToShortRoute(language)}/words`;

export const getWalletRoute = (language: LanguageShortKeys) => `/${language}/wallet`;

export const getUploadSourceRoute = (language: LanguageKeys, sourceType: string) => `/${getUploadSourceRouteByKey(language)}/${sourceType}/add`;

export const getUploadMusicRouteByKey = (language: LanguageKeys) => `/${getUploadSourceRouteByKey(language)}/music`;

export const getUploadSongItemRoute = (language: LanguageKeys, songId: string | number) => `/${getUploadMusicRouteByKey(language)}/${songId}`;

export const getUploadYoutubeRouteByKey = (language: LanguageKeys) => `/${getUploadSourceRouteByKey(language)}/youtube`;

export const getUploadYoutubeItemRoute = (language: LanguageKeys, videoId: string) => `/${getUploadYoutubeRouteByKey(language)}/${videoId}`;