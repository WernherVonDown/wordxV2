import React, { ComponentType } from "react";
import { useAuth } from "../context/AuthContext";

export function withAuth<T>(Component: ComponentType<T>) {
    return (props: T) => {
        const { state: { loggedIn, isLoading } } = useAuth();

        if (isLoading) {
            return <div>Loading....</div>
        }

        if (!loggedIn) {
            return <div>Пошув отседа</div>
        }
//@ts-ignore
        return <Component {...props} />
    }
}
