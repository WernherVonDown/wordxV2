import { NextParsedUrlQuery } from "next/dist/server/request-meta";
import { useRouter } from "next/router";
import React, { ComponentType } from "react";
import { isLanguageKeyExists, LanguageKeys, LanguageShortKeys } from "../const/language/Languagekeys";
import { IUser } from "../const/user/types";
import { useAuth } from "../context/AuthContext";
import { useLanguage } from "../hooks/useLanguage";
import { withAuth } from "./withAuth";

export interface ILangAuthProps {
    languageKey: LanguageKeys,
}


export function withLangAuth<T>(Component: ComponentType<T>) {
    return withAuth((props: T) => {
        const { languageKey } = useLanguage()
        const { state: { user } } = useAuth();
        
        if (!isLanguageKeyExists(languageKey)) {
            return <div>ХЗ Шо за язык такой {languageKey}</div>
        }
        const hasLanguage = user?.languages.find(l => l.language.key === languageKey);
    
        if (!hasLanguage) {
            return <div>Дядя заведи сначала себе подписку на {languageKey}</div>
        }

        return <Component {...props} languageKey={languageKey} />
    })
}
