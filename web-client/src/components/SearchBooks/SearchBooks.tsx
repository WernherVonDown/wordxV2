import { Box, Button, Container, Grid, Paper, TextField, Typography, useMediaQuery } from "@mui/material";
import React, { useCallback, useEffect, useMemo, useState } from "react";
import { useInput } from "../../hooks/useInput";
import { IBookDto } from "../../const/books/interfaces";
import { useLanguage } from "../../hooks/useLanguage";
import BooksService from "../../services/booksService";
import { SearchBookItem } from "./SearchBookItem";
import { LanguageKeys } from "../../const/language/Languagekeys";
import AddIcon from '@mui/icons-material/Add';
import { useRouter } from "next/router";
import { getUploadSourceRoute } from "../../const/APP_ROUTES";
import { SourceTypes } from "../../const/source/SourceTypes";
const MAX_BOOKS_PER_PAGE = 32;

export const SearchBooks: React.FC = () => {
    const bookName = useInput('');
    const [books, setBooks] = useState<IBookDto[]>([]);
    const { languageKey } = useLanguage();
    const [count, setCount] = useState(0);
    const [page, setPage] = useState(1);
    const [isNext, setIsNext] = useState(false);
    const [isPrevious, setIsPrevious] = useState(false);
    const [loading, setLoading] = useState(false);
    const router = useRouter()

    useEffect(() => {
        getBooks(page)
    }, []);

    const getBooks = useCallback((page: number) => {
        setLoading(true)
        BooksService.getBooksByLanguageKey(languageKey, bookName.value, page)
            .then(b => {
                setBooks(b.data.results);
                setCount(b.data.count);
                setPage(b.data.page);
                setIsNext(b.data.next);
                setIsPrevious(b.data.previos);
                setLoading(false)

            })
            .catch(e => {
                setLoading(false)
                console.log("error fetch books", e)
            })
    }, [languageKey, bookName.value]);

    const maxPage = useMemo(() => {
        return Math.ceil(count / MAX_BOOKS_PER_PAGE)
    }, [count, books.length, page])

    return <Box sx={{ minHeight: '100%' }}><Grid sx={{}} padding={1} gap={1} flex={1} container display={'flex'} flexDirection={'column'}>
        <Grid item>
            <Container >
                <Box display={'flex'} width={'100%'} gap={1}>
                    <TextField size="small" fullWidth type="search" {...bookName} />
                    <Button variant="contained" onClick={() => getBooks(1)}>Поиск</Button>
                    <Button variant="outlined" onClick={() => router.push(getUploadSourceRoute(languageKey, 'books'))}><AddIcon /></Button>
                </Box>

            </Container>
        </Grid>
        <Grid item textAlign={'center'}>
            {loading ? <Typography>Loading...</Typography>
                : <Typography>Найдено: {count}</Typography>}
        </Grid>
        {!loading && <Grid container gap={1} justifyContent="center">
            {books.map(b => <SearchBookItem key={`search_b_${b.id}`} {...b} />)}
        </Grid>}
        <Grid display={'flex'} gap={1} alignItems={'center'} justifyContent={'center'}>
            <Button
                variant="contained"
                disabled={!isPrevious}
                onClick={() => getBooks(page - 1)}
            >{'<'}</Button>
            <Typography>{page} / {maxPage}</Typography>
            <Button
                variant="contained"
                disabled={!isNext}
                onClick={() => getBooks(page + 1)}
            >{'>'}</Button>
        </Grid>
    </Grid>
    </Box>

}
