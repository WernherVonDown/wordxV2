import React from "react";
import { IBookDto } from "../../const/books/interfaces";
import { Box, Button, Card, CardActions, CardContent, CardMedia, Typography } from "@mui/material";
import { useRouter } from "next/router";

interface IProps extends IBookDto {
    
}

export const SearchBookItem: React.FC<IProps> = (props) => {
    const router = useRouter()

    return (
        <Card sx={{ display: 'flex', width: '384px' }}>
      <Box display={'flex'} flexDirection={'row'}>
        <Box display={'flex'} flexDirection={'column'} padding={1.5}>
          <CardMedia
            component="img"
            height="200"
            sx={{ maxWidth: '138px' }}
            image={props.cover}
            alt={`poster for ${props.title}`}
          />
        </Box>
        <Box display={'flex'} flexDirection={'column'}>
          <CardContent>
            <Typography gutterBottom variant="h6" component="div">
              {props.title}
            </Typography>
            <Typography gutterBottom variant="body2" component="div" color={"text.secondary"}>
              {props.author}
            </Typography>
            {/* <Box rowGap={0.5} display={'flex'} flexDirection={'column'}>
              <Typography 
              onClick={() => {
                if (!wordsStatistics) return;
                setStatsForAllWords(false)
              }}
              sx={{ background: "#eeedff", padding: '4px 8px', borderRadius: '24px', cursor: wordsStatistics ? 'pointer' : undefined, boxShadow: !statsForAllWords && wordsStatistics ? '0px 0px 0px 1px green' : 'unset' }} variant="body2" color="primary">
                Уникальных слов: {uniqueWordsCount}
              </Typography>
              <Typography 
              onClick={() => {
                if (!wordsStatistics) return;
                setStatsForAllWords(true)
              }}
              sx={{ background: "#eeedff", padding: '4px 8px', borderRadius: '24px', cursor: wordsStatistics ? 'pointer' : undefined, boxShadow: statsForAllWords && wordsStatistics ? '0px 0px 0px 1px green' : 'unset' }} variant="body2" color="primary">
                Всего слов: {totalWordsCount}
              </Typography>
            </Box> */}
          </CardContent>
        </Box>
      </Box>
      <CardActions>
        <Button onClick={() => router.push(window.location.pathname + `/${props.id}`)}>
          Выбрать
        </Button>
          {/* {CardActionsContent} */}
      </CardActions>
    </Card>
    )
} 