import { NextParsedUrlQuery } from "next/dist/server/request-meta";
import { useRouter } from "next/router";
import React, { useCallback, useEffect, useRef, useState } from "react";
import BooksService from "../../services/booksService";
import { useLanguage } from "../../hooks/useLanguage";
import { IBookDto } from "../../const/books/interfaces";
import { Button, Card, CardActions, CardContent, CardMedia, Typography } from "@mui/material";
import { useSource } from "../../context/SourceContext";
import { SourceTypes } from "../../const/source/SourceTypes";

interface IRouterQuery extends NextParsedUrlQuery {
    bookId: string,
}

export const UploadBookPageItem: React.FC = () => {
    const [book, setBook] = useState<IBookDto>();
    const router = useRouter();
    const { state: { booksPrice }, actions: { getBook, uploadSource } } = useSource();
    const { languageKey } = useLanguage()
    const { bookId } = router.query as IRouterQuery;
    const fileInputRef = useRef<HTMLInputElement>(null);
    const [file, setFile] = useState();
    const [uploadSuccess, setUploadSuccess] = useState(false);

    useEffect(() => {
        BooksService.getBookById(languageKey, bookId)
            .then(b => setBook(b.data))
            .catch(e => console.log("error book", e))
    }, [bookId]);

    const onSend = useCallback(async () => {
        console.log("aAA", file, book)
        if (file && book) {
            try {
                const formData = new FormData();
                formData.append("file", file);
                formData.append("title", book.title);
                formData.append("bookId", bookId);
                // formData.append("kinopoiskId", film.externalId?.kinopoiskId);
                formData.append("titleOriginal", book.title);
                formData.append("posterUrl", book.cover);
                // if (poster)
                //     formData.append("poster", poster)
                const res = await uploadSource(formData, SourceTypes.book);
                if (!res.success) {
                } else {
                    setUploadSuccess(true)
                }
            } catch (error) {
                console.log("error", error)
            }

        }

    }, [file, book, bookId]);

    const onGetAutoBook = useCallback(async () => {
        try {
            if (bookId) {
                const res = await getBook(bookId)
                if (res.success) {
                    const subRes = await fetch(res.bookLink);
                    const subData = await subRes.blob();
                    // console.log("subData", subData)
                    setFile(subData as any)
                    // setUploadSubSuccess(true)
                }
            }
        } catch (error) {
            alert("ERROR FETCHING")
        }

    }, [bookId, getBook])

    console.log("BOOKS", book)

    if (uploadSuccess) {
        return <div>Готово!</div>
    }

    if (!book) return <div>Loading...</div>



    return <Card sx={{ maxWidth: 345 }}>
        <CardMedia
            sx={{ height: 400 }}
            image={book.cover}
            title={book.title}
        />
        <CardContent>
            <Typography gutterBottom variant="h5" component="div">
                {book.title}
            </Typography>
            <Typography variant="body2" color="text.secondary">
                {book.author}
            </Typography>
        </CardContent>
        <CardActions>
            {
                file ? <>
                    <Button size="small" variant="contained" onClick={onSend}>Сохранить</Button>
                </> : <>
                    <Button size="small" variant="contained" onClick={onGetAutoBook}>Плоти {booksPrice} монет</Button>
                    <Button size="small" onClick={() => fileInputRef.current?.click()}>Добавляй сам</Button>
                    <input style={{ display: 'none' }} accept=".epub, .mobi, .txt" type={'file'} ref={fileInputRef} />
                </>
            }

        </CardActions>
    </Card>
}