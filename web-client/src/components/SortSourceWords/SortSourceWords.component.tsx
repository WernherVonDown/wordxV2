import * as React from 'react';
import Grid from '@mui/material/Grid';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Checkbox from '@mui/material/Checkbox';
import Button from '@mui/material/Button';
import Paper from '@mui/material/Paper';
import { ISourceWord } from '../../const/source/interfaces';
import { Divider, Slider, Typography } from '@mui/material';
import { Box } from '@mui/system';
import { InfoIcon } from '../../icons/InfoIcon';
import styles from "./SortSourceWords.module.scss"
import { ArrowsMoveIcon } from '../../icons/ArrowsMoveIcon';

function not(a: readonly string[], b: readonly string[]) {
    return a.filter((value) => b.indexOf(value) === -1);
}

function intersection(a: readonly string[], b: readonly string[]) {
    return a.filter((value) => b.indexOf(value) !== -1);
}
export interface ISourceWordWithRange extends ISourceWord {
    countRange?: number
}

interface IProps {
    words: ISourceWordWithRange[];
    onEndSorting: ({ knownWordsIds, learningWordsIds }: { knownWordsIds: string[], learningWordsIds: string[] }) => Promise<void>
    sourceId?: string;
    sendBlocked?: boolean;
    wordsRange: number[];
    setWordsRange: (...args: any) => any;
    sortByRange: boolean;
    setSortByRange: (...args: any) => any;
}

export const MIN_WORDS_RANGE = 0;
export const MAX_WORDS_RANGE = 10;

export const SortSourceWords: React.FC<IProps> = ({ 
    setSortByRange,
    sortByRange,
    words, onEndSorting, sourceId = '', sendBlocked, wordsRange, setWordsRange }) => {
    const [checked, setChecked] = React.useState<readonly string[]>([]);
    const [left, setLeft] = React.useState<string[]>(words.map(w => w.word.id));
    const [right, setRight] = React.useState<string[]>([]);
    const leftChecked = intersection(checked, left);
    const rightChecked = intersection(checked, right);
    const leftListRef = React.useRef<any>()

    const handleToggle = (value: string) => () => {
        const currentIndex = checked.indexOf(value);
        const newChecked = [...checked];

        if (currentIndex === -1) {
            newChecked.push(value);
        } else {
            newChecked.splice(currentIndex, 1);
        }

        setChecked(newChecked);
    };

    React.useEffect(() => {
        if (words.length) {
            setLeft(words.map(w => w.word.id))
            setRight([])
            leftListRef.current?.scrollTo({
                top: 0,
                behavior: 'smooth'
            });
        }

    }, [words])

    const handleAllRight = () => {
        setRight(right.concat(left));
        setLeft([]);
    };

    const handleCheckedRight = () => {
        setRight(right.concat(leftChecked));
        setLeft(not(left, leftChecked));
        setChecked(not(checked, leftChecked));
    };

    const handleCheckedLeft = () => {
        setLeft(left.concat(rightChecked));
        setRight(not(right, rightChecked));
        setChecked(not(checked, rightChecked));
    };

    const handleAllLeft = () => {
        setLeft(left.concat(right));
        setRight([]);
    };

    const handleDone = React.useCallback(() => {
        onEndSorting({ knownWordsIds: left, learningWordsIds: right });
    }, [left, right, onEndSorting])

    const customList = (items: readonly string[], listRef?: any) => (
        <Box sx={{ height: 610, overflow: 'auto', flexGrow: 1 }} ref={listRef}>
            <List dense component="div" role="list" sx={{ paddingTop: 'unset' }}>
                {items.map((value: string) => {
                    const labelId = `transfer-list-item-${value}-label`;
                    const wordItem = words.find(w => w.word.id === value);
                    const itemText = `${wordItem?.word.text} (${wordItem?.count}) ${wordItem?.word.translation ? `- ${wordItem?.word.translation}` : ''}`
                    return (
                        <ListItem
                            divider
                            key={value}
                            role="listitem"
                            button
                            sx={{ px: 1, paddingLeft: 0 }}
                            onClick={handleToggle(value)}
                        >
                            <ListItemIcon sx={{ minWidth: 'unset' }}>
                                <Checkbox
                                    checked={checked.indexOf(value) !== -1}
                                    tabIndex={-1}
                                    disableRipple
                                    sx={{ minWidth: 'unset', padding: '10px' }}
                                    inputProps={{
                                        'aria-labelledby': labelId,
                                    }}
                                />
                            </ListItemIcon>
                            <ListItemText id={labelId} primary={
                                <Box display={'flex'} justifyContent="space-between" width={'100%'}>
                                    <Typography>
                                        {wordItem?.word.text}
                                    </Typography>
                                    <Typography>
                                    {wordItem?.countRange ? `${wordItem?.countRange} / ` : ''}{wordItem?.count}
                                    </Typography>
                                </Box>
                            } secondary={<Typography color={'secondary'} fontSize={14} noWrap overflow={'hidden'}>{wordItem?.word.translation}</Typography>} />
                        </ListItem>
                    );
                })}
                <ListItem />
            </List>
        </Box>
    );

    return (
        <Grid container justifyContent={"center"} display="flex" alignItems={"center"} padding={3} direction="column">
            <Grid container spacing={2} justifyContent="center"  >

                <Grid item className={styles.wordsContainer} xs={3} height="100%" maxHeight={700} minHeight={300} overflow={'hidden'}>
                    <Box display={'flex'} width="100%" justifyContent={"space-between"} py={1}>
                        <Typography variant='h6' fontWeight={700}>Знаю</Typography>
                        <Typography variant='h6' color={'primary'} fontWeight={700}>{left.length}</Typography>
                    </Box>
                    <Box display={'flex'} width="100%" flexDirection={'column'}>
                        <Divider light variant='fullWidth' sx={{ mx: -3 }} />
                        <Box py={1} display={'flex'} width="100%" justifyContent={"space-between"}>
                            <Typography variant='body2' color={'secondary'}>Слово</Typography>
                            <Typography variant='body2' color={'secondary'}>Частота</Typography>
                        </Box>
                        <Divider light sx={{ mx: -3 }} />
                    </Box>

                    {customList(left, leftListRef)}
                </Grid>
                <Grid item paddingRight={2} alignItems="center" sx={{ display: 'flex' }}>
                    <Grid container direction="column" alignItems="center">
                        <Button
                            sx={{ my: 0.5 }}
                            variant="text"
                            size="small"
                            onClick={handleAllRight}
                            disabled={left.length === 0}
                            aria-label="move all right"
                            className={styles.moveWordsBtn}
                        >
                            <ArrowsMoveIcon double />
                        </Button>
                        <Button
                            sx={{ my: 0.5 }}
                            variant="text"
                            size="small"
                            onClick={handleCheckedRight}
                            disabled={leftChecked.length === 0}
                            aria-label="move selected right"
                            className={styles.moveWordsBtn}
                        >
                            <ArrowsMoveIcon />
                        </Button>
                        <Button
                            sx={{ my: 0.5 }}
                            variant="text"
                            size="small"
                            onClick={handleCheckedLeft}
                            disabled={rightChecked.length === 0}
                            aria-label="move selected left"
                            className={styles.moveWordsBtn}
                        >
                            <ArrowsMoveIcon flip />
                        </Button>
                        <Button
                            sx={{ my: 0.5 }}
                            variant="text"
                            size="small"
                            onClick={handleAllLeft}
                            disabled={right.length === 0}
                            aria-label="move all left"
                            className={styles.moveWordsBtn}
                        >
                            <ArrowsMoveIcon flip double />
                        </Button>
                    </Grid>
                </Grid>
                <Grid item className={styles.wordsContainer} xs={3} height="100%" maxHeight={700} minHeight={300} overflow={'hidden'}>
                    <Box display={'flex'} width="100%" justifyContent={"space-between"} py={1}>
                        <Typography variant='h6' fontWeight={700}>Буду изучать</Typography>
                        <Typography variant='h6' color={'primary'} fontWeight={700}>{right.length}</Typography>
                    </Box>
                    <Box display={'flex'} width="100%" flexDirection={'column'}>
                        <Divider light variant='fullWidth' sx={{ mx: -3 }} />
                        <Box py={1} display={'flex'} width="100%" justifyContent={"space-between"}>
                            <Typography variant='body2' color={'secondary'}>Слово</Typography>
                            <Typography variant='body2' color={'secondary'}>Частота</Typography>
                        </Box>
                        <Divider light sx={{ mx: -3 }} />
                    </Box>
                    {customList(right)}
                </Grid>
                <Grid alignSelf={'start'} item xs={2.5} spacing={2} gap={2} display={'flex'} flexDirection={'column'}>
                    <Box display={'flex'} width={1} flexDirection={'column'} sx={{ background: "#64CB13", padding: '20px', borderRadius: '12px', color: 'white' }}>
                        Слова за период
                        <Slider
                        value={wordsRange}
                        onChange={(event, newValue: any) => {
                            setWordsRange(newValue);
                          }}
                        step={1}
                        valueLabelDisplay="auto"
                        aria-labelledby="range-slider"
                        max={MAX_WORDS_RANGE}
                        min={MIN_WORDS_RANGE}
                    />
                    <Box display={'flex'} alignItems={'center'}><Checkbox checked={sortByRange} onChange={() => setSortByRange((p: any) => !p)} />Сортировать по периоду</Box>
                    </Box>
                    <Box sx={{ background: "#716FFF1F", padding: '20px', borderRadius: '12px', color: 'var(--primary)' }} gap={'10px'} display="flex">
                        <Box>
                            <InfoIcon />
                        </Box>
                        <Typography variant='body2' color={'primary'}>
                            В этом словаре не все слова из фильма. Мы исключили пересекающиеся слова из других добавленных источников.
                        </Typography>
                    </Box>
                    <Button fullWidth variant='contained' disabled={sendBlocked} onClick={handleDone}>Сохранить и загрузить следующие</Button>
                </Grid>

            </Grid>

        </Grid>
    );
}