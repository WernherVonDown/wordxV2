import React, { ReactNode, useContext } from "react";
import { AppBar, Box, Button, Container, styled, Toolbar, Typography } from "@mui/material";
import Link from "next/link";
import { AuthContext } from "../../context/AuthContext";
import { getLanguageRouteByKey, getMyWordsRoute, getUploadSourceRouteByKey, LoginRoute, RegistrationRoute } from "../../const/APP_ROUTES";
import { LanguageKeys } from "../../const/language/Languagekeys";
import { usePushShallowUrl } from "../../hooks/usePushShallowUrl";
import { LanguageEnvChange } from "../LanguageEnvChange/LanguageEnvChange";
import { NotificationButton } from "../Notifications/NotificationButton";
import { PlusIcon } from "../../icons/PlusIcon";
import { WalletButton } from "../Wallet/WalletButton";

const Offset = styled('div')(({ theme }) => theme.mixins.toolbar);

interface IProps {
    children: ReactNode | ReactNode[];
    languageKey: LanguageKeys;
}

export const MainLayout: React.FC<IProps> = ({ children, languageKey }) => {
    const { state: { loggedIn, user }, actions: { logout } } = useContext(AuthContext);
    const goToUrl = usePushShallowUrl();
    return (
        <main>
            <AppBar position="fixed" color="inherit" elevation={0}>
                <Toolbar>
                    <Box sx={{ flexGrow: 1, display: 'flex', gap: '1rem' }}>
                        <Typography variant="h6" component="div" >
                            <Link href="/">Wordx</Link>
                        </Typography>
                        <LanguageEnvChange languageKey={languageKey} />
                    </Box>
                    {!loggedIn ? <>
                        <Button href={RegistrationRoute} color="inherit" variant='text'>
                            Регистрация
                        </Button>
                        <Button href={LoginRoute} color="inherit" variant='outlined'>
                            Вход
                        </Button>
                    </> : <Box gap="0.5rem" display={'flex'}>
                        <Button variant='text' onClick={() => goToUrl(getUploadSourceRouteByKey(languageKey))}>
                            <PlusIcon /> Добавить источник
                        </Button>
                        <Button color="inherit" onClick={() => goToUrl(getMyWordsRoute(languageKey))}>
                            Мои слова
                        </Button>
                        <NotificationButton />
                        <WalletButton />
                        <Button onClick={logout} href="/" color="inherit" variant='outlined'>
                            Выйти
                        </Button>
                    </Box>
                    }

                </Toolbar>
            </AppBar>
            <Offset />

            {children}
        </main>
        // </>
    )
}