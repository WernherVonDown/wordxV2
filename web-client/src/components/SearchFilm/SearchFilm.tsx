import { Grid } from "@mui/material";
import { useRouter } from "next/router";
import React, { useCallback, useState } from "react";
import { getUploadFilmItemRouteByKey, getUploadFilmRouteByKey, getUploadSourceRouteByKey } from "../../const/APP_ROUTES";
import { languageRouteToKey } from "../../const/language/Languagekeys";
import { IKinopoiskFilm } from "../../const/source/interfaces";
import { useLanguage } from "../../hooks/useLanguage";
import { KinopoiskSearch } from "./KinopoiskSearch";
import { ResultFilmsList } from "./ResultFilmsList";

interface IProps {}

export const SearchFilm: React.FC<IProps> = () => {
    const [searchResults, setSearchResults] = useState<IKinopoiskFilm[]>([]);
    const [filmName, setFileName] = useState('')
    const [showResult, setShowResult] = useState(false);
    const { languageKey } = useLanguage()
    const onSearchResult = useCallback((result: IKinopoiskFilm[], filmName: string) => {
        setSearchResults(result);
        setFileName(filmName)
        setShowResult(true);
    }, []);

    const router = useRouter()

    const onBack = useCallback(() => {
        console.log("QUERRR", router.query)
       
        router.push({
            pathname: getUploadFilmRouteByKey(languageRouteToKey(router.query.languageShortKey as any)),
        }).then(() => {
            setShowResult(false);
            setSearchResults([]);
            setFileName('')
        })
        
    }, [])

    const onChooseFilm = useCallback((filmItem: IKinopoiskFilm) => {
        console.log("HEHHE", filmItem)
        router.push({
            pathname: getUploadFilmItemRouteByKey(languageKey, filmItem.kinopoiskId),
        })
    }, [languageKey])


    const backToCategories = useCallback(() => router.push(getUploadSourceRouteByKey(languageKey)), [languageKey])

    return (
        <Grid>
            {(!showResult || !filmName) ? <KinopoiskSearch onBack={backToCategories}  onSearchResult={onSearchResult} />
            : <ResultFilmsList onBack={onBack} onChooseFilm={onChooseFilm} filmName={filmName} films={searchResults} />}
        </Grid>
    )
}