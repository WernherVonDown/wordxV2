import { Button, Card, CardActions, CardContent, CardMedia, Grid, Typography } from "@mui/material";
import { Box } from "@mui/system";
import classNames from "classnames";
import React from "react";
import { IKinopoiskFilm } from "../../const/source/interfaces";
import styles from "./SearchFilm.module.scss";

interface IProps extends IKinopoiskFilm {
    selected?: boolean;
    onChoose?: (id: string) => void;
    noActions?: boolean;
}

export const ResultFilmsListItem: React.FC<IProps> = ({
    nameRu,
    nameEn,
    posterUrl,
    posterUrlPreview,
    ratingKinopoisk,
    filmId,
    year,
    kinopoiskId,
    selected,
    onChoose,
    noActions,
    nameOriginal
}) => {

    return (
        <Card sx={{ display: 'flex', width: '400px' }} className={styles.chooseFilmCard}>
            <Box display={'flex'} flexDirection={'row'} sx={{ width: '100%', height: '100%' }}>
                <Box display={'flex'} flexDirection={'column'} padding={2} sx={{ width: '100%' }}>
                    <CardMedia
                        component="img"
                        height="100%"
                        sx={{ maxWidth: '208px' }}
                        image={posterUrl}
                        alt={`poster for ${nameRu}`}
                    />
                </Box>
                <Box display={'flex'} flexDirection={'column'} sx={{ width: '100%' }}>
                    <CardContent sx={{ width: '100%', display: 'flex', flexDirection: 'column', position: 'relative', height: '100%' }}>
                        <Typography gutterBottom component="div">
                            {nameRu}
                        </Typography>
                        <Typography gutterBottom variant="body2" component="div" color={"text.secondary"}>
                            {nameOriginal || nameEn}
                        </Typography>
                        {year && <Typography gutterBottom variant="caption" component="div">
                            {year}
                        </Typography>}
                        {ratingKinopoisk && ratingKinopoisk !== 'null' && <Typography gutterBottom variant="caption" component="div">
                            {ratingKinopoisk}
                        </Typography>}
                        {!noActions && <CardActions sx={{ bottom: '0%' }}>
                    <Button className={styles.chooseFilmButton} variant="contained" sx={{position: 'absolute', bottom: '1rem', right: '1rem'}} onClick={() => onChoose && onChoose(kinopoiskId)}>Выбрать</Button>
                </CardActions>}
                    </CardContent>

                </Box>
                
            </Box>
        </Card>
    )

}