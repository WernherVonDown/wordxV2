import { Autocomplete, Box, Button, Grid, Paper, TextField, Typography } from "@mui/material";
import React, { useCallback, useEffect, useState } from "react";
import { useInput } from "../../hooks/useInput";
import _ from 'lodash';
import { IKinopoiskFilm } from "../../const/source/interfaces";
import { FilmCategoryItem } from "../../icons/FilmCategoryItem";
import { BackButton } from "../UploadSource/UploadSource.component";
import { searchFilm } from "../../externalServices/kinopoiskService";
import { useRouter } from "next/router";

interface IProps {
  onSearchResult: (result: IKinopoiskFilm[], filmName: string) => void;
  onBack: () => void;
}

export const KinopoiskSearch: React.FC<IProps> = ({ onSearchResult, onBack }) => {
  const router = useRouter()
  console.log('QUERY', router.query);
  const filmName = useInput(router.query.s as any);

  const getMovieKinopoisk = useCallback(async (name: string) => {
    try {
      // await searchFilm(name)

      const options = {
        method: 'GET',
        headers: {
          'X-API-KEY': '47634ec9-aa04-418a-9214-94fcb33301f9',
          // 'X-RapidAPI-Host': 'online-movie-database.p.rapidapi.com'
        }
      };

      const res = await fetch(`https://kinopoiskapiunofficial.tech/api/v2.2/films?order=RATING&type=FILM&ratingFrom=0&ratingTo=10&yearFrom=1000&yearTo=3000&keyword=${name}`, options)
        .then(response => response.json())
      console.log("RESULT", res)
      return res.items?.filter((e: IKinopoiskFilm) => e.imdbId) || [];
    } catch (error) {
      console.log("getMovie error", error)
    }

  }, [])

  // const getMovieKinopoisk2 = useCallback(async (name: string) => {
  //   try {
  //     const options = {
  //       method: 'GET',
  //       headers: {
  //         'X-API-KEY': '47634ec9-aa04-418a-9214-94fcb33301f9',
  //         // 'X-RapidAPI-Host': 'online-movie-database.p.rapidapi.com'
  //       }
  //     };

  //     const res = await fetch(`https://kinopoiskapiunofficial.tech/api/v2.1/films/search-by-keyword?keyword=${name}&type=FILM`, options)
  //       .then(response => response.json())
  //     console.log("RESULT", res)
  //     return res.films || [];
  //   } catch (error) {
  //     console.log("getMovie error", error)
  //   }

  // }, [])

  useEffect(() => {
    console.log("SEARCH", router.query.s)
    if (router.query.s) {
      onSearch(router.query.s as string)
    }
  }, [router.query.s])


  const onSearch = useCallback(async (filmName: string) => {
    if (filmName) {
      const sResult = await getMovieKinopoisk(filmName);
      onSearchResult(sResult, filmName)
    } else {
      alert('Заполните поле')
    }

  }, [])

  return <Grid marginTop={3} display="flex" container justifyContent="center">
    <BackButton onClick={onBack} />
    <Grid md={6} sm={8} xs={12} lg={5} item display="flex" direction="column" alignItems={'center'} padding={2} gap={3}
      sx={{ background: 'white', boxShadow: '0px 28px 31px rgba(109, 109, 112, 0.12)', borderRadius: '12px' }}
    >
      <Grid item><FilmCategoryItem /></Grid>
      <Grid item><Typography variant="h5">Введите название</Typography></Grid>
      <TextField size="small" fullWidth type="search" {...filmName} />
      <Grid item><Button variant="contained" onClick={() => {
        if (filmName.value) {
          router.push({
            pathname: router.pathname,
            query: { ...router.query, s: filmName.value,  },
        });
        }
        
      }}><Typography variant="button">Дальше</Typography></Button></Grid>
      
    </Grid>
    
  </Grid>
}