import { Grid, Typography } from "@mui/material";
import React, { useCallback, useState } from "react";
import { IKinopoiskFilm } from "../../const/source/interfaces";
import { BackButton } from "../UploadSource/UploadSource.component";
import { ResultFilmsListItem } from "./ResultFilmsListItem";

interface IProps {
    films: IKinopoiskFilm[]
    onChooseFilm: (item: IKinopoiskFilm) => void;
    filmName: string;
    onBack: () => void;
}

export const ResultFilmsList: React.FC<IProps> = ({ films, onChooseFilm, filmName, onBack }) => {
    const onChoose = useCallback((selectedFilmId: string) => {
        const film = films.find(f => f.kinopoiskId === selectedFilmId);
        if (film) {
            onChooseFilm(film)
        }
    }, [films])
    return (
        <Grid container justifyContent={'center'}  marginTop={2}>
            <BackButton onClick={onBack} />
            <Grid xs={11} alignItems={'center'} display="flex" direction="column" gap={2}>
                <Typography variant="h5">Результат поиска: {filmName}</Typography>
                <Grid container gap={2} justifyContent={'center'}>
                    {films.map((e) => <ResultFilmsListItem key={`result_film_${e.kinopoiskId}`} onChoose={onChoose} {...e} />)}
                </Grid>
            </Grid>
        </Grid>
    )
}