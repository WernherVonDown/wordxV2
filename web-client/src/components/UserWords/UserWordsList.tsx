import * as React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Checkbox from '@mui/material/Checkbox';
import { IWordsListItem } from '../../const/words/interfaces';
import { Box, Button, Grid } from '@mui/material';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import ContentCopyIcon from '@mui/icons-material/ContentCopy';
import { LanguageKeys } from '../../const/language/Languagekeys';
import { useLanguage } from '../../hooks/useLanguage';
import TranslateIcon from '@mui/icons-material/Translate';
import { downloadStringAsFile } from '../../utils/downloadStringAsFile';

interface IProps {
    words: IWordsListItem[];
    changeWords: (words: string[]) => Promise<void>;
    sources: string[];
}

const getReversoLink = (word: string, language: LanguageKeys) => `https://context.reverso.net/translation/${language}-russian/${word}`

interface IWordsListCountBySource extends IWordsListItem {
    countSelectedSources?: number
}

export const UserWordsList: React.FC<IProps> = ({ words, changeWords, sources }) => {
    const [checked, setChecked] = React.useState<string[]>([]);
    const [sortBySelectedSources, setSortBySelectedSources] = React.useState(false);
    const {languageKey} = useLanguage()

    const handleToggle = (value: string) => () => {
        console.log("handleToggle", value)
        const currentIndex = checked.indexOf(value);
        const newChecked = [...checked];

        if (currentIndex === -1) {
            newChecked.push(value);
        } else {
            newChecked.splice(currentIndex, 1);
        }

        setChecked(newChecked);
    };

    const onChangeWords = React.useCallback(() => {
        changeWords(checked);
    }, [checked])

    const wordsWithCountBySources = React.useMemo((): IWordsListCountBySource[] => {
        return words.map(w => {
            //@ts-ignore
            const count = w?.countBySource && sources.reduce((acc, sourceId) => (w?.countBySource[sourceId] || 0) + acc, 0)
            return {
                ...w,
                countSelectedSources: count
            }
        })
    }, [words, sources])

    const sortedWords = React.useMemo(() => {
        if (!sortBySelectedSources) {
            return wordsWithCountBySources.sort((a, b) => b.count - a.count)
        }
        return wordsWithCountBySources.sort((a, b) => (b.countSelectedSources || b.count) - (a.countSelectedSources || b.count))

    }, [wordsWithCountBySources, sortBySelectedSources])

    const onDownload = React.useCallback(() => {
        const text = sortedWords.map(w => {
            const countSelectedSources = w?.countSelectedSources;
            return w.word.text + ` (${w.count}${countSelectedSources ? `/${countSelectedSources}` : ''}) ${w.word.translation ? `- ${w.word.translation}` : ''}`
        }).join('\n');
        const fileName = window.prompt('Введите имя файла');
        
        downloadStringAsFile(text, fileName || `wordx_${new Date().toISOString()}.txt`);
    }, [sortedWords])

    return (
        <Grid>
            <Box display={'flex'} flexDirection={'column'}>
            {!!sources.length && <Box><Checkbox checked={sortBySelectedSources} onChange={() => setSortBySelectedSources(p => !p)} />Сортировать по выделенным источникам</Box>}
            </Box>
            
            <List sx={{ width: '100%', maxHeight: '500px', overflow: "auto", maxWidth: 360, bgcolor: 'background.paper' }}>
                {sortedWords.map((value, id) => {
                    const wordId = value.word.id;
                    const countSelectedSources = value?.countSelectedSources;
                    const wordText = value.word.text + ` (${value.count}${countSelectedSources ? `/${countSelectedSources}` : ''}) ${value.word.translation ? `- ${value.word.translation}` : ''}`;
                    const labelId = `checkbox-list-label-${wordId}`;
                    const reversoLink = getReversoLink(value.word.text, languageKey);
                    return (
                        <ListItem
                            key={wordId}
                            // secondaryAction={
                            //     <IconButton edge="end" aria-label="comments">
                            //         <CommentIcon />
                            //     </IconButton>
                            // }
                            disablePadding
                        >
                            <ListItemButton role={undefined} onClick={handleToggle(wordId)} dense>
                                <ListItemIcon>
                                    {id + 1}
                                    <Checkbox
                                        edge="start"
                                        checked={checked.indexOf(wordId) !== -1}
                                        tabIndex={-1}
                                        disableRipple
                                        inputProps={{ 'aria-labelledby': labelId }}
                                    />
                                </ListItemIcon>
                                <ListItemText id={labelId} primary={wordText} />
                            </ListItemButton>
                            <CopyToClipboard text={value.word.text}>
                                <ContentCopyIcon style={{cursor: 'pointer'}}/>
                            </CopyToClipboard>
                            <Button onClick={() =>  window.open(reversoLink, '_blank')}>
                                <TranslateIcon />
                            </Button>
                        </ListItem>
                    );
                })}
            </List>
            <Button disabled={!checked.length} onClick={onChangeWords} variant="contained">Уже знаю</Button>
            <Button disabled={!sortedWords.length} onClick={onDownload} variant="text">Скачать</Button>
        </Grid>
    );
}