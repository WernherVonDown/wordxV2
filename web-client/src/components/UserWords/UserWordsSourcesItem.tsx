import { Button, Card, CardActions, CardContent, CardMedia, LinearProgress, Typography } from "@mui/material";
import { Box } from "@mui/system";
import React, { useCallback, useMemo } from "react";
import { ISource } from "../../const/source/interfaces";
import { useLanguage } from "../../hooks/useLanguage";
import { getSourceWordsRoute, getSourceWordsSortingRoute } from "../../const/APP_ROUTES";
import { usePushShallowUrl } from "../../hooks/usePushShallowUrl";

interface IProps {
    source: ISource;
    knownCount: number;
    id: string;
    notFullySorted?: boolean;
}

export const UserWordsSourcesItem: React.FC<IProps> = ({ source, id, knownCount, notFullySorted }) => {
    const { posterUrl, title, titleOriginal, uniqueWordsCount, poster } = source;
    const goToUrl = usePushShallowUrl();
    const { languageKey } = useLanguage()

    const { percentRounded, percent } = useMemo(() => {
        const percent = knownCount / uniqueWordsCount * 100;
        return { percentRounded: Math.round(percent), percent }
    }, [uniqueWordsCount, knownCount])

    const selectSource = useCallback(() => {
        goToUrl(getSourceWordsSortingRoute(languageKey, source.id));
    }, [source.id, languageKey])
    return (
        <Card
            sx={{ maxWidth: 300, width: 200 }}
            style={{ cursor: 'pointer' }}
        // className={classNames({ [styles.filmSelected]: selected })}
        //  onClick={onSelect}
        >
            <CardMedia
                component="img"
                height="300"
                image={posterUrl}
                alt={`poster for ${title}`}
            />
            <CardContent>
                <Typography gutterBottom variant="body2" component="div">
                    {title}
                </Typography>
                <Typography gutterBottom variant="caption" component="div">
                    {titleOriginal || '-'}
                </Typography>
                <LinearProgress color="success" variant="determinate" value={percentRounded} />

            </CardContent>
            <CardActions>
                {notFullySorted && <Button variant="contained" onClick={selectSource}>Продолжить</Button>}
            </CardActions>
        </Card>
    )
}