import { Checkbox, Grid } from "@mui/material";
import React, { useCallback, useContext, useEffect, useMemo, useState } from "react";
import { IUserWordsSources, IWordsListItem } from "../../const/words/interfaces";
import { useWords } from "../../context/WordsContext";
import { UserWordsList } from "./UserWordsList";
import { UserWordsSourcesItem } from "./UserWordsSourcesItem";
import styles from "./UserWords.module.scss"
import classNames from "classnames";
import { Box } from "@mui/system";
import { AuthContext } from "../../context/AuthContext";
import { useBool } from "../../hooks/useBool";

export const UserWords: React.FC = () => {
    const { actions: { getWords, sendLearnedWords, getUserWordsSources } } = useWords();
    const [learningWords, setLearningWords] = useState<IWordsListItem[]>([]);
    const [knownWordsCount, setKnownWordsCount] = useState<number>(0);
    const [learnedWordsCount, setLearnedWordsCount] = useState<number>(0);
    const [notSortedWordsCount, setNotSortedWordsdCount] = useState<number>(0);
    const [isLoading, setLoading] = useState(true);
    const [userWordsSources, setUserWordsSources] = useState<IUserWordsSources[]>([])
    const [wordsSourcesFilter, setWordsSourcesFilter] = useState<string[]>([])
    const { state: { user } } = useContext(AuthContext);
    const { value: intersectWords, onToggle: toggleIntersectWords } = useBool(false);

    useEffect(() => {
        if (learningWords.length) {
            setLoading(false);
        }
    }, [learningWords.length])

    const onGetWords = useCallback(async () => {
        const res = await getWords();
        if (res.success) {
            const { learningWords, knownWordsCount, learnedWordsCount, notSortedWordsCount } = res
            setLearnedWordsCount(learnedWordsCount);
            setKnownWordsCount(knownWordsCount);
            setLearningWords(learningWords);
            setNotSortedWordsdCount(notSortedWordsCount);
            if (!learningWords.length) {
                setLoading(false);
            }
        } else {
            // alert("Ошибка")
        }
    }, [])

    const onGetUserWordsSources = useCallback(async () => {
        const res = await getUserWordsSources();
        if (res.success) {
            const { userWordsSources } = res;
            setUserWordsSources(userWordsSources)
        } else {
            // alert("Ошибка")
        }
    }, [])

    useEffect(() => {
        onGetWords();
        onGetUserWordsSources();
    }, [])

    console.log("wordsSourcesFilter", wordsSourcesFilter, userWordsSources)

    const changeLearnedWords = useCallback(async (words: string[]) => {
        if (words.length) {
            setLoading(true);
            const res = await sendLearnedWords(words);
            if (res.success) {
                onGetWords()
                onGetUserWordsSources();
            } else {
                // alert("Ошибка")
            }
        }

    }, [])

    const renderUserWordsCources = useCallback(({ id, source, knownCount }: IUserWordsSources) => {
        const selected = wordsSourcesFilter.find(wf => wf === source.id);
        const selectAction = selected ?
            () => setWordsSourcesFilter(p => p.filter(wf => wf !== source.id))
            : () => setWordsSourcesFilter(p => [...p, source.id]);
        return (
            <Grid key={`user_words_sources_${id}`}>
                <Box onClick={selectAction} className={classNames(styles.userWordsSources, { [styles.selected]: selected })}>
                    <UserWordsSourcesItem notFullySorted={!!user?.notSourtedSources?.find(sId => sId === source.id)} id={id} source={source} knownCount={knownCount} />
                </Box>
            </Grid>
        )
    }, [wordsSourcesFilter, user])

    const UserWordsSources = useMemo(() => {
        console.log("userWordsSources", userWordsSources)
        return userWordsSources.map(renderUserWordsCources)
    }, [userWordsSources, wordsSourcesFilter])

    const filteredLearningWords = useMemo(() => {
        if (!wordsSourcesFilter.length) {
            return learningWords;
        }
        if (wordsSourcesFilter.length >= 2 && intersectWords) {
            return learningWords
            .filter(w => w.sources.length >= wordsSourcesFilter.length && wordsSourcesFilter.filter(s => w.sources.includes(s)).length === wordsSourcesFilter.length)
        }
        return learningWords.filter(w => w.sources.find(s => wordsSourcesFilter.includes(s)))
    }, [learningWords, wordsSourcesFilter, intersectWords])

    if (isLoading) {
        return <div>Loading words...</div>
    }

    return (
        <Grid container>
            <Grid item width={'35%'}>
                <Grid>
                    <Grid>
                        Известные слова: {knownWordsCount}
                    </Grid>
                    <Grid>
                        Выученные: {learnedWordsCount}
                    </Grid>
                    <Grid>
                        Неотсортированные: {notSortedWordsCount}
                    </Grid>
                    <Grid>
                        Изучаемые: {filteredLearningWords.length}
                    </Grid>
                    <Grid>
                        {wordsSourcesFilter.length > 1 && <Grid>
                            <Checkbox checked={intersectWords} onChange={toggleIntersectWords} />Только общие слова выделенных источников
                        </Grid>}
                    </Grid>
                </Grid>
                <Grid>
                    <UserWordsList sources={wordsSourcesFilter} changeWords={changeLearnedWords} words={filteredLearningWords} />
                </Grid>
            </Grid>
            <Grid padding={1} item width={'65%'} display="flex" flexWrap={'wrap'} gap={1}>
                {UserWordsSources}
            </Grid>
        </Grid>
    )
}