import { Button, FormControl, FormControlLabel, FormLabel, Grid, Radio, RadioGroup, TextField } from "@mui/material";
import React, { useCallback, useContext, useState } from "react";
import { SessionContext } from "../../context/SessionContext";

const NEW_CARD_KEY = 'new_card';

export const Wallet: React.FC = () => {
    const [amount, setAmount] = useState(100);
    const [paymentMethodId, setPaymentMethodId] = useState('');
    const { actions: { buyCoins }, state: { paymentMethods } } = useContext(SessionContext);
    
    const onPay = useCallback(() => {
        if(amount > 0) {
            buyCoins(amount, paymentMethodId)
        }
        
    }, [paymentMethodId, amount])

    return (
        <Grid display={'flex'} container flexDirection={'column'}>
            <Grid>
                Сумма пополнения
            </Grid>
            <Grid>
                <TextField label={'Сумма'} value={amount} onChange={(e) => setAmount(Number(e.target.value))} type="number" />
            </Grid>
            <Grid>
                <FormControl>
                    <FormLabel id="demo-controlled-radio-buttons-group">Способ оплаты</FormLabel>
                    <RadioGroup
                        aria-labelledby="demo-controlled-radio-buttons-group"
                        name="controlled-radio-buttons-group"
                        value={paymentMethodId}
                        onChange={(e) => setPaymentMethodId(e.target.value)}
                    >
                        {paymentMethods.map(m => <FormControlLabel key={m.paymentMethodId} value={m.paymentMethodId} control={<Radio />} label={m.paymentDetails.title} />)}
                        <FormControlLabel value="" control={<Radio />} label="Новая карта" />
                    </RadioGroup>
                </FormControl>
            </Grid>
            <Grid>
                <Button variant="contained" onClick={onPay}>Пополнить</Button>
            </Grid>
        </Grid>
    )
}