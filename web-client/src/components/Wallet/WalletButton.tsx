import { Button } from "@mui/material";
import { Box } from "@mui/system";
import React, { useContext, useMemo } from "react";
import { getWalletRoute } from "../../const/APP_ROUTES";
import { AuthContext } from "../../context/AuthContext";
import { useLanguage } from "../../hooks/useLanguage";
import { CoinsIcon } from "../../icons/CoinsIcon";

export const WalletButton: React.FC = () => {
    const { state: { user } } = useContext(AuthContext);
    const { languageShortKey } = useLanguage();
    const walletRoute = useMemo(() => {
        return getWalletRoute(languageShortKey)
    }, [languageShortKey])

    return <Button
        href={walletRoute}    
    sx={{
            backgroundColor: '#716FFF1F', ':hover': {
                backgroundColor: '#716fff0d'
            }
        }}>
        <CoinsIcon />
        {user?.wallet || 0} монет
    </Button>
}