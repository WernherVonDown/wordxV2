import { Grid, Typography } from "@mui/material";
import { Box } from "@mui/system";
import Link from "next/link";
import React, { useCallback, useEffect, useMemo, useState } from "react";
import { PlansRoute } from "../../const/APP_ROUTES";
import { ILanguage, ILanguageLevel } from "../../const/language/interfaces";
import { useAuth } from "../../context/AuthContext";
import DataService from "../../services/dataService";
import UserService from "../../services/userService";
import { LanguageItem } from "./LanguageItem";

export const ChooseLanguages: React.FC = () => {
    const [languages, setLanguages] = useState<ILanguage[]>([]);
    const [languageLevels, setLanguageLevels] = useState<ILanguageLevel[]>([]);
    const { state: { user }, actions: { checkAuth } } = useAuth()
    useEffect(() => {
        getLanguageLaevels()
        getLanguages()
    }, [])

    const getLanguages = useCallback(async () => {
        try {
            const res = await DataService.getLanguages();
            console.log("RES", res.data)
            setLanguages(res.data)
        } catch (error) {
            console.log('getSubscriptionPlans error', error)
        }
    }, []);


    const getLanguageLaevels = useCallback(async () => {
        try {
            const res = await DataService.getLanugageLevels();
            setLanguageLevels(res.data)
        } catch (error) {
            console.log('getLanguageLaevels error', error)
        }
    }, []);

    const addLanguage = useCallback(async (languageId: string) => {
        try {
            const res = await UserService.changeLanguage({ languageId });
            checkAuth()
        } catch (error) {
            console.log('addLanguage error', error)
        }
    }, []);

    const removeLanguge = useCallback(async (languageId: string) => {
        try {
            const res = await UserService.changeLanguage({ languageId, remove: true });
            checkAuth()
        } catch (error) {
            console.log('removeLanguge error', error)
        }
    }, []);

    const langsNotMatchPlan = useMemo(() => {
        if (user)
            return user?.languages.length > user?.subscriptionPlan?.maxLangs;
        return true;
    }, [user?.languages, user?.subscriptionPlan])

    const maxLangsSelected = useMemo(() => {
        if (user)
            return user?.languages.length === user?.subscriptionPlan?.maxLangs;
        return true;
    }, [user?.languages, user?.subscriptionPlan])

    const renderLanguage = useCallback((lang: ILanguage) => {
        const selected = user?.languages.find(l => l.language.id === lang.id);
        const languageLevel = selected?.level?.id;
        const cannotAdd = maxLangsSelected || langsNotMatchPlan;
        const onChoose = () => selected ? removeLanguge(selected.id) : cannotAdd ? () => { } : addLanguage(lang.id);

        return <LanguageItem onChoose={onChoose} noAddBtn={!selected && cannotAdd} planMismatch={!!selected && langsNotMatchPlan} key={`language_key_${lang.id}`} selected={!!selected} languageLevel={languageLevel} language={lang} />
    }, [user?.languages, langsNotMatchPlan, maxLangsSelected])

    return <Box>
        <Grid container display="flex" gap={1} padding={1}>
            {languages.map(renderLanguage)}
        </Grid>
        {maxLangsSelected && <Box>
            <Typography>
                Чтобы добавить дополнительные языки измените текущий <a href={PlansRoute} style={{ color: 'var(--primary)', textDecoration: 'underline' }}>план</a>
            </Typography>
        </Box>}
    </Box>
}