import { Button, Grid, Paper, Typography } from "@mui/material";
import React from "react";
import { ILanguage } from "../../const/language/interfaces";
import styles from "./ChooseLanguages.module.scss";
import classNames from 'classnames';

interface IProps {
    language: ILanguage;
    languageLevel?: string;
    selected: boolean;
    onChoose: () => void;
    planMismatch: boolean;
    noAddBtn: boolean;
}

export const LanguageItem: React.FC<IProps> = ({language: { key, label, id }, languageLevel, selected, onChoose, planMismatch, noAddBtn}) => {
    return (
        <Paper className={classNames(styles.languageItem, {[styles.selected]: selected, [styles.error]: planMismatch})}>
            <Grid padding={1}>
                <Typography variant="h6">{label}</Typography>
                {!noAddBtn && <>
                {selected ? <Button variant="outlined" onClick={onChoose}>Убрать</Button> : <Button variant="contained" onClick={onChoose}>Выбрать</Button>}
                </>}
            </Grid>
        </Paper>
    )
}