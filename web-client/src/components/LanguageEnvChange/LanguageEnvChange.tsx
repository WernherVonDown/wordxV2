import { Grid } from "@mui/material";
import { Box } from "@mui/system";
import React, { useCallback } from "react";
import { useAuth } from "../../context/AuthContext";
import { IUserLanguage } from "../../const/language/interfaces";
import { usePushShallowUrl } from "../../hooks/usePushShallowUrl";
import { getLanguageRouteByKey, LanguagesRoute } from "../../const/APP_ROUTES";
import { FlagsByLangKeys } from "../../const/language/FlagsByLangKeys";
import styles from './LanguageEnvChange.module.scss';
import classNames from "classnames";
import { LanguageKeys } from "../../const/language/Languagekeys";
import { PlusIcon } from "../../icons/PlusIcon";
import { ILangAuthProps } from "../../hoc/withLangAuth";

interface IProps {
    languageKey?: LanguageKeys,
}


export const LanguageEnvChange: React.FC<IProps> = ({ languageKey }) => {
    const { state: { user } } = useAuth();
    const goToUrl = usePushShallowUrl();

    const renderLanguage = useCallback(({ language, id }: IUserLanguage) => {
        return <Grid key={`language_btn_${id}`} item>
            <Box className={classNames(styles.flagIcon, { [styles.selected]: language.key === languageKey })} onClick={() => goToUrl(getLanguageRouteByKey(language.key))}>
                <img src={FlagsByLangKeys[language.key]} alt={language.label} />
            </Box>
        </Grid>
    }, [languageKey])

    return <Grid container display={"flex"} alignItems="center" gap="0.3rem">
        {user?.languages.map(renderLanguage)}
        <Grid item>
            <Box className={classNames(styles.plusIcon)} onClick={() => goToUrl(LanguagesRoute)}>
                <PlusIcon />
            </Box>
        </Grid>
    </Grid>
}