import { Button, Grid } from "@mui/material";
import React, { useEffect } from "react";
import { getUploadSourceRouteByKey } from "../../const/APP_ROUTES";
import { useSource } from "../../context/SourceContext";
import { useLanguage } from "../../hooks/useLanguage";
import { usePushShallowUrl } from "../../hooks/usePushShallowUrl";
import SourceService from "../../services/sourceService";
import { SourcesList } from "../SourcesList/SourcesList.component";

export const Main: React.FC = () => {
    const { languageKey } = useLanguage();
    const {actions: { getList: getSourcesList }} = useSource();
    const goToShallowUrl = usePushShallowUrl();

    // useEffect(() => {
    //     getSourcesList()
    // }, [])

    
    return (
        <Grid container display="flex" direction="column" padding={2}>
            {/* <Grid item>{languageKey}</Grid>
            <Grid><Button onClick={() => goToShallowUrl(getUploadSourceRouteByKey(languageKey))}>upload</Button></Grid> */}
            {/* <Grid><Button onClick={() => SourceService.getList(languageKey)}>Load</Button></Grid> */}
            <SourcesList 
                key={languageKey} 
            />
        </Grid>
    )
}