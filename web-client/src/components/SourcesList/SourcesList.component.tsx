import { Grid } from "@mui/material";
import React, { useCallback, useEffect, useState } from "react";
import InfiniteScroll from 'react-infinite-scroller';
import { ISource } from "../../const/source/interfaces";
import { useAuth } from "../../context/AuthContext";
import { useSource } from "../../context/SourceContext";
import { SourcesListItem } from "./SourceListItem";
import { useLanguage } from "../../hooks/useLanguage";

export const SourcesList: React.FC = () => {
    const { state: { sources }, actions: {
        getList,
    } } = useSource();
    const { state: { user }, actions: { checkAuth } } = useAuth();
    const [page, setPage] = useState(0);
    const [isLoading, setIsLoading] = useState(false);
    const [noMoreSources, setNoMoreSources] = useState(false);
    const { languageKey } = useLanguage()


    useEffect(() => {
        // setIsLoading(true);
        // getList(page).finally(() => setIsLoading(false));
    }, [])

    const renderSource = useCallback(({ id, language, poster, title, totalWordsCount, uniqueWordsCount, type, posterUrl, titleOriginal }: ISource) => (
        <SourcesListItem sorted={!!user?.sources.find(sId => sId === id)} notFullySorted={!!user?.notSourtedSources?.find(sId => sId === id)} key={`source_${id}`} {...{ language, poster, title, totalWordsCount, uniqueWordsCount, type, id, posterUrl, titleOriginal }} />
    ), [user?.sources])

    return (

        <InfiniteScroll
            pageStart={0}
            loadMore={() => {
                if (!isLoading && !noMoreSources) {
                    console.log("LOAD MORE")
                    // setPage(p => p + 1)
                    setIsLoading(true);
                    getList(page, () => setNoMoreSources(true)).finally(() => setIsLoading(false));
                    setPage(p => p + 1)
                }

            }}
            hasMore={!isLoading || noMoreSources}
            loader={!noMoreSources ? <div className="loader" key={0}>Loading ...</div> : <span />}
        >
            <Grid container gap={1} justifyContent="center">
                {sources.map(renderSource)}
            </Grid>
        </InfiniteScroll>

    )
}