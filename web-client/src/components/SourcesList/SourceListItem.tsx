import { GolfCourseTwoTone } from "@mui/icons-material";
import { Button, Card, CardActions, CardContent, CardMedia, Grid, Typography } from "@mui/material";
import { Box } from "@mui/system";
import React, { useCallback, useMemo, useState } from "react";
import { getSourceWordsRoute, getSourceWordsSortingRoute } from "../../const/APP_ROUTES";
import { ILanguage } from "../../const/language/interfaces";
import { ISourcePoster, ISourceType } from "../../const/source/interfaces";
import { useSource } from "../../context/SourceContext";
import { useLanguage } from "../../hooks/useLanguage";
import { usePushShallowUrl } from "../../hooks/usePushShallowUrl";
import { CoinsIcon } from "../../icons/CoinsIcon";

interface IProps {
  language: ILanguage,
  poster: ISourcePoster,
  title: string,
  totalWordsCount: number,
  uniqueWordsCount: number,
  type: ISourceType,
  id: string,
  posterUrl: string;
  titleOriginal: string;
  sorted: boolean;
  notFullySorted: boolean;
}

export const SourcesListItem: React.FC<IProps> = (
  { language, notFullySorted, poster, title, totalWordsCount, uniqueWordsCount, type, id, posterUrl, titleOriginal, sorted }
) => {
  const { actions: { getWords, getWordsStatistics }, state: { statisticsPrice } } = useSource();
  const goToUrl = usePushShallowUrl();
  const { languageKey } = useLanguage()
  const [wordsStatistics, setWordsStatistics] = useState(false);
  const [knownWordsPercent, setkownWordsPercent] = useState(0);
  const [learningWordsPercent, setLearningWordsPercent] = useState(0);
  const [notSortedWordsPercent, setNotSortedWordsPercent] = useState(0);
  const [allKnownWordsPercent, setAllKownWordsPercent] = useState(0);
  const [allLearningWordsPercent, setAllLearningWordsPercent] = useState(0);
  const [allNotSortedWordsPercent, setAllNotSortedWordsPercent] = useState(0);
  const [sortingPercent, setSortingPercent] = useState(0);
  const [allSortingPercent, setAllSortingPercent] = useState(0);
  const [statsForAllWords, setStatsForAllWords] = useState(true);

  const selectSource = useCallback(() => {
    goToUrl(getSourceWordsRoute(languageKey, id));
  }, [id, languageKey])
  
  const selectSourceSorting = useCallback(() => {
    goToUrl(getSourceWordsSortingRoute(languageKey, id));
  }, [id, languageKey])

  const getSourceWords = useCallback(() => {
    getWords(id);
  }, [id])

  const onGetWordsStatistics = useCallback(async () => {
    const res = await getWordsStatistics(id);
    if (res.success) {
      setkownWordsPercent(res.knownPercent);
      setNotSortedWordsPercent(res.notSortedPercent);
      setLearningWordsPercent(res.learningPercent);
      setAllKownWordsPercent(res.allKnownWordsPercent);
      setAllLearningWordsPercent(res.allLearningWordsPercent);
      setAllNotSortedWordsPercent(res.allNotSortedWordsPercent);
      setSortingPercent(res.sortingPercent);
      setAllSortingPercent(res.allSortingPercent);
      console.log("RES", res)
      setWordsStatistics(true);
    } else {
      alert('Ошибка')
    }
  }, [id]);

  const allWordsAreSorted = useMemo(() => {
    return learningWordsPercent + knownWordsPercent === 1;
  }, [learningWordsPercent, knownWordsPercent])

  const Statistics = useMemo(() => {
    const knownPercent = statsForAllWords ? allKnownWordsPercent : knownWordsPercent;
    const learningPercent = statsForAllWords ? allLearningWordsPercent : learningWordsPercent;
    const notSortedPercent = statsForAllWords ? allNotSortedWordsPercent : notSortedWordsPercent;
    const sortingPercentCount = statsForAllWords ? allSortingPercent : sortingPercent

    return <div style={{ width: '100%', height: '100%', display: 'flex', borderRadius: '8px', overflow: 'hidden' }}>
      <div style={{ width: `${knownPercent * 100}%`, background: '#A9FF9B', height: '100%' }}>

      </div>
      <div style={{ width: `${learningPercent * 100}%`, background: '#FFD029', height: '100%' }}>

      </div>
      <div style={{ width: `${sortingPercentCount * 100}%`, background: '#26f9f2', height: '100%' }}>

      </div>
      <div style={{ width: `${(notSortedPercent - sortingPercentCount) * 100}%`, background: '#FF748D', height: '100%' }}>

      </div>
      
    </div>
  }, [learningWordsPercent, knownWordsPercent, notSortedWordsPercent, allKnownWordsPercent, allLearningWordsPercent, allNotSortedWordsPercent, statsForAllWords, allSortingPercent, sortingPercent])

  const CardActionsContent = useMemo(() => {
    if (sorted && !notFullySorted) {
      return <Grid item>
        <Typography>Добавлен</Typography>
      </Grid>
    }
    
    const FirstButton = allWordsAreSorted ? <Button variant="contained" onClick={() => alert('Пока не готово')} fullWidth>{notFullySorted ? 'Продолжить' : 'Добавить'}</Button> : <Button variant="contained" onClick={notFullySorted ? selectSourceSorting : selectSource} fullWidth>{notFullySorted ? 'Продолжить' : 'Выбрать'}</Button>
    const SecondButton = wordsStatistics ? Statistics : <Button variant="outlined" onClick={onGetWordsStatistics} fullWidth>Статистика {statisticsPrice} <CoinsIcon /></Button>;

    return <Grid container width={'100%'} display="flex" spacing={1}>
      <Grid item display="flex" xs={6}>
        {FirstButton}
      </Grid>
      <Grid item display="flex" xs={6}>
        {SecondButton}
      </Grid>
    </Grid>

  }, [sorted, allWordsAreSorted, wordsStatistics, Statistics, id, languageKey, statisticsPrice])

  return (
    <Card sx={{ display: 'flex', width: '384px' }}>
      <Box display={'flex'} flexDirection={'row'}>
        <Box display={'flex'} flexDirection={'column'} padding={1.5}>
          <CardMedia
            component="img"
            height="200"
            sx={{ maxWidth: '138px' }}
            image={poster?.url || posterUrl}
            alt={`poster for ${title}`}
          />
        </Box>
        <Box display={'flex'} flexDirection={'column'}>
          <CardContent>
            <Typography gutterBottom variant="h6" component="div">
              {title}
            </Typography>
            <Typography gutterBottom variant="body2" component="div" color={"text.secondary"}>
              {titleOriginal}
            </Typography>
            <Box rowGap={0.5} display={'flex'} flexDirection={'column'}>
              <Typography 
              onClick={() => {
                if (!wordsStatistics) return;
                setStatsForAllWords(false)
              }}
              sx={{ background: "#eeedff", padding: '4px 8px', borderRadius: '24px', cursor: wordsStatistics ? 'pointer' : undefined, boxShadow: !statsForAllWords && wordsStatistics ? '0px 0px 0px 1px green' : 'unset' }} variant="body2" color="primary">
                Уникальных слов: {uniqueWordsCount}
              </Typography>
              <Typography 
              onClick={() => {
                if (!wordsStatistics) return;
                setStatsForAllWords(true)
              }}
              sx={{ background: "#eeedff", padding: '4px 8px', borderRadius: '24px', cursor: wordsStatistics ? 'pointer' : undefined, boxShadow: statsForAllWords && wordsStatistics ? '0px 0px 0px 1px green' : 'unset' }} variant="body2" color="primary">
                Всего слов: {totalWordsCount}
              </Typography>
            </Box>
          </CardContent>
        </Box>
      </Box>
      <CardActions>
          {CardActionsContent}
      </CardActions>
    </Card>
  );

}