import React, { useCallback, useEffect, useMemo, useState } from "react";
import { ISourceWord } from "../../const/source/interfaces";
import { useAuth } from "../../context/AuthContext";
import { useSource } from "../../context/SourceContext";
import { useSourceId } from "../../hooks/useSourceId";
import { ISourceWordWithRange, MAX_WORDS_RANGE, MIN_WORDS_RANGE, SortSourceWords } from "../SortSourceWords/SortSourceWords.component";

const WORDS_PER_PAGE = 50;

export const SourceWordsSourting: React.FC = () => {
    const { sourceId } = useSourceId();
    const { actions: {checkAuth}} = useAuth()
    const { actions: { getWords, sendSortedWords } } = useSource();
    const [words, setWords] = useState<ISourceWord[]>([])
    const [isLoading, setIsLoading] = useState(true)
    const [processing, setProcessing] = useState(false);
    const [wordsRange, setWordsRange] = useState([MIN_WORDS_RANGE, MAX_WORDS_RANGE])
    const [sortByRange, setSortByRange] = useState(false);

    useEffect(() => {
        if (sourceId) {
            getSourceWords()
        }
    }, [sourceId]);

    useEffect(() => {
        if (words.length) {
            setIsLoading(false);
        }
    }, [words.length])

    const normolizeSortedWordIditem = useCallback((wordId: string) => {
        const word = words.find(w => w.word.id === wordId);
        return {
            word: wordId,
            count: word?.count,
            percent: word?.percent
        }
    }, [words])

    const onEndSorting = useCallback(async ({ knownWordsIds, learningWordsIds }: { knownWordsIds: string[], learningWordsIds: string[] }) => {
        const knownWords = knownWordsIds.map(normolizeSortedWordIditem);
        const learningWords = learningWordsIds.map(normolizeSortedWordIditem);
        let needWords = words.length <= knownWordsIds.concat(learningWordsIds).length
        setProcessing(true)
        setWords(words => words.filter(w => !knownWordsIds.concat(learningWordsIds).includes(w.word.id)))
        // console.log('words', knownWordsIds.concat(learningWordsIds), words,  words.filter(w => !knownWordsIds.concat(learningWordsIds).includes(w.word.id)), needWords)
        const wordsNew = await sendSortedWords({knownWords, learningWords, sourceId, needWords})
        if (needWords) {
            setWords(wordsNew);
        }
        setProcessing(false)
    }, [words, sourceId])

    const getSourceWords = useCallback(async () => {
        if (sourceId) {
            const { success, words } = await getWords(sourceId);
            if (success) {
                const sortedWords = words.sort((a: ISourceWord, b: ISourceWord) => b.count - a.count);
                setWords(sortedWords)
                if (!sortedWords.length) {
                    setIsLoading(false);
                }
            } else {
            }
        }
    }, [sourceId])

    const filteredWords: ISourceWordWithRange[] = useMemo(() => {
        if (wordsRange[0] === MIN_WORDS_RANGE && wordsRange[1] === MAX_WORDS_RANGE) return words.map(w => {
            //@ts-ignore
            delete w.countRange
            return w;
        }).sort((a, b) => b.count - a.count);
        const resWords = words
            .filter(w => !w.percentStat || w.percentStat.split(',').slice(wordsRange[0], wordsRange[1]).join('').length)
            .map(w => {
                if (!w.percentStat) return w;
                //@ts-ignore 
                w.countRange = w.percentStat.split(',').slice(wordsRange[0], wordsRange[1]).reduce((a, c) => a + Number(c),0)
                return w;
            })
            console.log("STATT", resWords[0])
            if (!sortByRange) return resWords.sort((a, b) => b.count - a.count)
            //@ts-ignore
            return resWords.sort((a, b) => b.countRange - a.countRange)
    }, [words, wordsRange, sortByRange])

    console.log("EEE", filteredWords, wordsRange)

    return <div>{isLoading ? <div>Loading...</div> : <SortSourceWords sortByRange={sortByRange} setSortByRange={setSortByRange} setWordsRange={setWordsRange} wordsRange={wordsRange} sourceId={sourceId} sendBlocked={processing} onEndSorting={onEndSorting} words={filteredWords.slice(0, WORDS_PER_PAGE)} />}</div>
}