import React, { ReactNode, useContext } from "react";
import { AppBar, Box, Button, Container, styled, Toolbar, Typography } from "@mui/material";
import Link from "next/link";
import { AuthContext } from "../../context/AuthContext";
import { LoginRoute, PlansRoute, RegistrationRoute } from "../../const/APP_ROUTES";
import { LanguageEnvChange } from "../LanguageEnvChange/LanguageEnvChange";

const Offset = styled('div')(({ theme }) => theme.mixins.toolbar);

interface IProps {
    children: ReactNode | ReactNode[]
}

export const LandingLayout: React.FC<IProps> = ({ children }) => {
    const { state: { loggedIn }, actions: { logout } } = useContext(AuthContext);
    return (
        <main>
            <AppBar position="fixed" color="inherit">
                <Toolbar>

                    <Box sx={{ flexGrow: 1, display: 'flex', gap: '1rem' }}>
                        <Typography variant="h6" component="div" >
                            <Link href="/">Wordx</Link>
                        </Typography>
                        {loggedIn && <LanguageEnvChange />}
                    </Box>
                    {!loggedIn ? <>
                        <Button href={RegistrationRoute} variant='text'>
                            Регистрация
                        </Button>
                        <Button href={LoginRoute} variant='contained'>
                            Вход
                        </Button>
                    </> : <Box gap="0.5rem" display={'flex'}>
                        <Button href={PlansRoute} color="inherit" variant='text'>
                            Plans
                        </Button>
                        <Button onClick={logout} color="inherit" variant='outlined'>
                            Выйти
                        </Button>
                    </Box>
                    }

                </Toolbar>
            </AppBar>
            <Offset />
            {children}
        </main>
    )
}