import React, { useContext } from 'react';
import styles from './LoginForm.module.scss';
import { useInput } from '../../hooks/useInput';
import { AuthContext } from '../../context/AuthContext';
import { useCallback } from 'react';
import { Box, Button, Grid, Paper, TextField, Typography } from '@mui/material';
import Link from 'next/link';
import { RegistrationRoute, RemindPasswordRoute } from '../../const/APP_ROUTES';
import GoogleAuthButton from '../GoogleAuthButton/GoogleAuthButton.component';

export const LoginForm: React.FC = () => {
    const { actions: { login } } = useContext(AuthContext);
    const email = useInput('');
    const password = useInput('');

    const onLogin = useCallback(() => {
        if (email.value.length && password.value.length) {
            login(email.value, password.value);
        } else {
            alert('Заполните все поля');
        }
    }, [email.value, password.value]);

    return <>
        <Paper>
            <div className={styles.loginForm}>
                <Typography variant='h5'>Вход</Typography>
                <TextField variant="outlined" margin='normal' fullWidth size='small' {...email} type={'text'} label='Почта' />
                <TextField variant="outlined" margin='normal' fullWidth size='small' {...password} type={'password'} label='Пароль' />
                <Box display={"flex"} flexDirection="column" gap={"3px"} alignItems="center">
                    <Button fullWidth sx={{ marginTop: 1 }} variant='contained' onClick={onLogin}>
                        вход
                    </Button>
                    <Typography>Или</Typography>
                    <GoogleAuthButton />
                </Box>
            </div>
        </Paper>
        <Grid container className={styles.bottomWrapper} direction={"column"} display={"flex"} justifyContent="center" marginTop={0.5} spacing={2} textAlign={"center"}>
            <Grid item>
                <Link href={RemindPasswordRoute}>
                    Не помню пароль
                </Link>
            </Grid>
            <Grid item>
                <Typography>Нет аккаунта? <Link href={RegistrationRoute}>
                    Регистрация
                </Link>
                </Typography>
            </Grid>
        </Grid>
    </>;
}