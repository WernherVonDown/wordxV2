import { Button, IconButton, Popover, Typography } from "@mui/material";
import PopupState, { bindPopover, bindTrigger } from "material-ui-popup-state";
import React, { useContext } from "react";
import { SessionContext } from "../../context/SessionContext";
import { NitificationIcon } from "../../icons/NitificationIcon";
import styles from "./Notifications.module.scss";
import { NotificationsContent } from "./NotificationsContent";

export const NotificationButton: React.FC = () => {
    const {state: { notifications }} = useContext(SessionContext);

    return (
        <PopupState variant="popover" popupId="demo-popup-popover">
          {(popupState) => (
            <div>
              <IconButton {...bindTrigger(popupState)}>
                <NitificationIcon active={Boolean(notifications.filter(n => !n.read).length)}/>
            </IconButton>
              <Popover
                {...bindPopover(popupState)}
                anchorOrigin={{
                  vertical: 'bottom',
                  horizontal: 'left',
                }}
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'center',
                }}
                classes={{
                    paper: styles.popoverContent
                }}
              >
                <NotificationsContent notifications={notifications} />
              </Popover>
            </div>
          )}
        </PopupState>
      );
}