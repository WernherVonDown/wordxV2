import { Button, IconButton, Popover, Typography } from "@mui/material";
import React, { useContext, useEffect, useRef } from "react";
import { INotification } from "../../const/notifications/interfaces";
import useOnScreen from "../../hooks/useOnScreen";
import styles from "./Notifications.module.scss";
import classNames from "classnames";
import { SessionContext } from "../../context/SessionContext";

interface IProps {
    notifications: INotification[]
}

const NotificationsItem: React.FC<INotification> = ({ id, text, read }) => {
    const { actions: { readNotification }} = useContext(SessionContext)
    const ref = useRef<HTMLDivElement>(null)
    const isVisible = useOnScreen(ref);

    useEffect(() => {
        if (!read && isVisible && id) {
            readNotification(id);
        }
    }, [!read && isVisible && id])

    return <div ref={ref} className={styles.item}>
        <div className={classNames(styles.unreadDot, {[styles.active]: !read})}/>
        {text}
    </div>
}

export const NotificationsContent: React.FC<IProps> = ({ notifications = [] }) => {
    return (
        <div className={styles.content}>
            <Typography sx={{ marginLeft: '8px' }} variant="h6">Уведомления ({notifications.length})</Typography>
            {notifications.length ?
                notifications.map(e => <NotificationsItem {...e} key={`notification_${e.id}`} />)
                : <div className={styles.item}>Пока нет уведомлений</div>}
        </div>
    )
}