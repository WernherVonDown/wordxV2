import { Button, Grid, Typography } from "@mui/material";
import { useRouter } from "next/router";
import React, { useCallback } from "react";
import { getLanguageRoute } from "../../const/APP_ROUTES";
import { IUserLanguage } from "../../const/language/interfaces";
import { languageRouteToKey, LanguageShortKeys, langugeKeyToShortRoute } from "../../const/language/Languagekeys";
import { useAuth } from "../../context/AuthContext";
import { usePushShallowUrl } from "../../hooks/usePushShallowUrl";

export const Dashboard: React.FC = () => {
    const { state: {user}} = useAuth()
    const router = useRouter()
    const goToShallowUrl = usePushShallowUrl();
    const renderButton = useCallback((language:IUserLanguage) => (
        <Grid key={`dashboard_lang_${language.id}`}><Button onClick={() => goToShallowUrl(getLanguageRoute(langugeKeyToShortRoute(language.language.key)))} variant="contained">{language.language.label}</Button></Grid>
    ), [])
    //https://wiki.intellectmoney.ru/pages/viewpage.action?pageId=4849803#id-%D0%9F%D1%80%D0%BE%D1%82%D0%BE%D0%BA%D0%BE%D0%BB%D0%BF%D1%80%D0%B8%D0%B5%D0%BC%D0%B0%D0%BF%D0%BB%D0%B0%D1%82%D0%B5%D0%B6%D0%B5%D0%B9Intellectmoney-4.2.1.%D0%A4%D0%BE%D1%80%D0%BC%D0%B0%D0%B7%D0%B0%D0%BF%D1%80%D0%BE%D1%81%D0%B0%D0%BF%D0%BB%D0%B0%D1%82%D0%B5%D0%B6%D0%B0
    return <Grid container direction={"column"} gap={1}>
        <Grid item><Typography>Dashboard</Typography></Grid>
        {user?.languages.map(renderButton)}

        
    </Grid>
}
//https://merchant.intellectmoney.ru/ru?eshopId=462963&orderId=12333&serviceName=buy_shit&recipientAmount=10&recipientCurrency=TST&user_email=aleekseevbursche@yandex.ru&preference=bankcard&merchantReceipt={"inn":"780531351060","group":"Main","content":{"type":1,"positions":[{"quantity":1.000,"price":10,"tax":1,"text":"Спички"}],"customerContact":"aleekseevbursche@yandex.ru"}}
{/* <form action="https://merchant.intellectmoney.ru/ru/" name="pay" method="POST">
     <input type="hidden" name="eshopId" value="462963"/>
     <input type="hidden" name="orderId" value="12312312"/>
     <input type="hidden" name="serviceName" value="Покупка подписки"/>
     <input type="hidden" name="recipientAmount" value="10"/>
     <input type="hidden" name="recipientCurrency" value="TST"/> */}
     {/* <input type="hidden" name="UserFieldName_0" value="Перевод в кошелек"/> */}
     {/* <input type="hidden" name="UserField_0" value="1374940645"/> */}
     {/* <input type="hidden" name="UserFieldName_9" value="UserPaymentFormId"/> */}
     {/* <input type="hidden" name="UserField_9" value="51"/> */}
     {/* <input type="hidden" name="UserFieldName_2" value="Param name for value_2"/> */}
    //  <input type="hidden" name="user_email" value="aleekseevbursche@yandex.ru"/>
    //  <input type="hidden" name="successUrl" value="https://google.ru/"/>
    //  <input type="hidden" name="preference" value="bankcard"/>
     {/* <input type="hidden" name="frame" value="1"/> */}
//      <input type="submit" name="button" value="оплатить"/>
// </form>
// <script async type="text/javascript">
//           document.write('<script type="text/javascript" src="https://intellectmoney.ru/payform.js?default_sum=&serviceName=&projectName=&siteURL=&comment_tip=&successUrl=&inn=780531351060&btn_name=0&eshopId=462963&construct=donate&writer=now"></script>');
// </script>