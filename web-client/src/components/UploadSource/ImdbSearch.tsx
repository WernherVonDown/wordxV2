import { Autocomplete, Box, Button, TextField } from "@mui/material";
import React, { useCallback, useState } from "react";
import { useInput } from "../../hooks/useInput";
import _ from 'lodash';

export const ImdbSearch: React.FC = () => {
  // const filmName = useInput('');
  const [filmName, setFilmName] = useState('')
  const [searchResults, setSearchResults] = useState([])

  const getMovieKinopoisk = useCallback(async (name: string) => {
    try {
      const options = {
        method: 'GET',
        headers: {
          'X-API-KEY': '47634ec9-aa04-418a-9214-94fcb33301f9',
          // 'X-RapidAPI-Host': 'online-movie-database.p.rapidapi.com'
        }
      };

      const res = await fetch(`https://kinopoiskapiunofficial.tech/api/v2.1/films/search-by-keyword?keyword=${name}&page=2`, options)
        .then(response => response.json())
      console.log("RESULT", res)
      // if (res.d) {

      // }
      return res.d || [];
    } catch (error) {
      console.log("getMovie error", error)
    }

  }, [])

  const getMovieImdb = useCallback(async (name: string) => {
    try {
      const options = {
        method: 'GET',
        headers: {
          'X-RapidAPI-Key': 'ecf6b52282msh564d044a28f6713p107cb1jsn7d8e9b4d4217',
          'X-RapidAPI-Host': 'online-movie-database.p.rapidapi.com'
        }
      };

      const res = await fetch(`https://online-movie-database.p.rapidapi.com/auto-complete?q=${name}`, options)
        .then(response => response.json())
      console.log("RESULT", res)
      // if (res.d) {

      // }
      return res.d || [];
    } catch (error) {
      console.log("getMovie error", error)
    }

  }, [])

  const onSearch = useCallback(async () => {
    if (filmName.trim().length) {
      getMovieKinopoisk(filmName)
      // const sResult = await getMovieImdb(filmName)
      // setSearchResults(sResult)
    } else {
      alert('Заполните поле')
    }

  }, [filmName])

  return (
    <>
      <Autocomplete
        id="country-select-demo"
        sx={{ width: 300 }}
        options={searchResults}
        autoHighlight
        // value={filmName}
        onInputChange={_.debounce((e: any, res: any) => {
          console.log("EEEEE", e, res)
          setFilmName(res)
        }, 500)}
        // getOptionLabel={(option: any) => option.label}
        renderOption={(props, { id, l, y, i: { imageUrl } }: { id: string, l: string, i: { imageUrl: string }, y: string }) => (
          <Box component="li" sx={{ '& > img': { mr: 2, flexShrink: 0 } }} {...props}>
            <img
              loading="lazy"
              width="20"
              src={imageUrl}
              // srcSet={`https://flagcdn.com/w40/${option.code.toLowerCase()}.png 2x`}
              alt={l}
            />
            {l} { y && <span>({y})</span>}
            {/* {option.label} ({option.code}) +{option.phone} */}
          </Box>
        )}
        renderInput={(params) => (
          <TextField
            {...params}
            label="Choose a country"
            inputProps={{
              ...params.inputProps,
              autoComplete: 'new-password', // disable autocomplete and autofill
            }}
          />
        )}
      />
      <Button onClick={onSearch}>Find</Button>
    </>
  );

  return <div>
    Search
    {/* <TextField type="search" {...filmName} /> */}
    <Button onClick={onSearch}>Найти</Button>
  </div>
}