import { Button, Grid, TextField } from "@mui/material";
import React, { useCallback, useState } from "react";
import { useInput } from "../../hooks/useInput";
import { useSource } from "../../context/SourceContext";
import { SourceTypes } from "../../const/source/SourceTypes";

interface IProps {
    sourceType: SourceTypes,
}

export const UploadSourceFile: React.FC<IProps> = ({ sourceType }) => {
    const [file, setFile] = useState();
    const [poster, setPoster] = useState()
    const sourceName = useInput('');
    const sourceNameOriginal = useInput('');
    const [fileName, setFileName] = useState('')
    const { actions: { uploadSource } } = useSource();
    const [uploadSuccess, setUploadSuccess] = useState(false)

    const saveFile = useCallback((e: any) => {
        setFile(e.target.files[0]);
        setFileName(e.target.files[0].name);
    }, []);

    const savePoster = useCallback((e: any) => {
        setPoster(e.target.files[0]);
    }, []);

    const onSend = useCallback(async () => {
        console.log("aAA", file, sourceName.value, fileName)
        if ((!sourceName.value && !sourceNameOriginal.value) || !file || !poster) return alert("Заполните все поля")
        if (file) {
            try {
                const formData = new FormData();
                formData.append("file", file);
                formData.append("title", sourceName.value);

                formData.append("titleOriginal", sourceNameOriginal.value);
                if (poster)
                    formData.append("poster", poster)
                const res = await uploadSource(formData, sourceType);
                if (!res.success) {
                    // alert("Oшибка")
                } else {
                    setUploadSuccess(true)
                }
            } catch (error) {
                console.log("error", error)
            }

        }

    }, [sourceName.value, fileName, file, poster, sourceNameOriginal.value, sourceType])

    if (uploadSuccess) {
        return <Grid container display={"flex"} flexDirection="column">
            Источник ожидает отбработки
        </Grid>
    }

    return (
        <Grid container display={"flex"} flexDirection="column">
            <Grid item>
                <TextField {...sourceName} placeholder='Название' />
            </Grid>
            <Grid item>
                <TextField {...sourceNameOriginal} placeholder='Оригинальное название' />
            </Grid>
            <Grid item>
                Постер
                <input type="file" onChange={savePoster} placeholder='Постер' accept=".jpg, .png, image/jpeg, image/png"  />
            </Grid>
            <Grid item>
                Текст в формате txt
                <input type="file" onChange={saveFile} placeholder='файл' accept=".txt" />
            </Grid>
            <Grid item>
                <Button variant='contained' onClick={onSend}>готово</Button>
            </Grid>
        </Grid>
    )
}