import { Button, Grid, Paper, TextField, Typography } from "@mui/material";
import React, { useCallback, useState } from "react";
import { useSource } from "../../context/SourceContext";
import { useInput } from "../../hooks/useInput";
import { ImdbSearch } from "./ImdbSearch";
import { KinopoiskSearch } from "../SearchFilm/KinopoiskSearch";
import { SearchFilm } from "../SearchFilm/SearchFilm";
import { UploadFilm } from "./UploadFilm";
import { FilmCategoryItem } from "../../icons/FilmCategoryItem";
import styles from "./UloadSource.module.scss";
import { SourceCategoryKeys } from "../../const/source/SourcesCategorys";
import { CloseIcon } from "../../icons/CloseIcon";
import { ArrowLeftIcon } from "../../icons/ArrowLeftIcon";
import classNames from "classnames";
import { useRouter } from "next/router";
import { BookCategoryItem } from "../../icons/BookCategoryItem";
import { YoutubeCategoryIcon } from "../../icons/YoutubeCategoryIcon";
import { MusicCategoryIcon } from "../../icons/MusicCategoryIcon";

const CATEGORYS = [
    {
        label: 'Фильм',
        key: 'film'
    }
]

interface IBackButtonProps {
    className?: string;
    onClick?: () => void;
}

export const BackButton: React.FC<IBackButtonProps> = ({ className = '', onClick }) => {
    return (
        <div className={classNames(styles.backBtn, className)}
            onClick={onClick}
        ><ArrowLeftIcon /></div>
    )
}

export const UploadSource: React.FC = () => {
    const router = useRouter();
    // const [file, setFile] = useState();
    // const [poster, setPoster] = useState()
    // const sourceName = useInput('');
    // const [fileName, setFileName] = useState('')
    // const { actions: { uploadSource } } = useSource();
    // const [category, setCategory] = useState<SourceCategoryKeys>();

    // const saveFile = useCallback((e: any) => {
    //     setFile(e.target.files[0]);
    //     setFileName(e.target.files[0].name);
    // }, []);

    // const savePoster = useCallback((e: any) => {
    //     setPoster(e.target.files[0]);
    // }, []);

    // const onSend = useCallback(async () => {
    //     console.log("aAA", file, sourceName.value, fileName)
    //     if (file) {
    //         try {
    //             const formData = new FormData();
    //             formData.append("file", file);
    //             formData.append("fileName", sourceName.value);
    //             if (poster)
    //                 formData.append("poster", poster)
    //             const res = await uploadSource(formData);
    //             if (!res.success) {
    //                 alert("Oшибка")
    //             }
    //         } catch (error) {
    //             console.log("error", error)
    //         }

    //     }

    // }, [sourceName.value, fileName, file, poster])

    // const backToCategories = useCallback(() => {
    //     setCategory(undefined);
    // }, [])

    // if (category === SourceCategoryKeys.movies) {
    //     //@ts-ignore
    //     return <UploadFilm backToCategories={backToCategories} />
    // }

    return (
        <Grid item xs={12} display="flex" justifyContent={'center'} paddingTop={2}>
            <Grid item flexDirection={'column'} display={'flex'} sm={12} md={10} lg={8} xs={12} alignSelf="center" gap={'3rem'}>
                <Grid item xs={12} justifyContent={'center'} textAlign={'center'}> <Typography variant={'h5'}>Загрузить новый ресурс</Typography></Grid>
                <Grid container justifyContent={'center'} textAlign={'center'} gap={2}>
                    <Paper elevation={4} className={styles.sourceCategory}>
                        <Grid onClick={() => router.push({
                            pathname: window.location.href + '/books',
                        })} className={styles.categoryWrapper} item width={256} height={174} alignItems="center" justifyContent="space-evenly" display={'flex'} flexDirection="column">
                            <BookCategoryItem />
                            <Typography variant="h6">Книга</Typography>
                        </Grid>
                    </Paper>
                    <Paper elevation={4} className={styles.sourceCategory}>
                        <Grid onClick={() => router.push({
                            pathname: window.location.href + '/film',
                        })} className={styles.categoryWrapper} item width={256} height={174} alignItems="center" justifyContent="space-evenly" display={'flex'} flexDirection="column">
                            <FilmCategoryItem />
                            <Typography variant="h6">Фильм</Typography>
                        </Grid>
                    </Paper>
                    <Paper elevation={4} className={styles.sourceCategory}>
                        <Grid onClick={() => router.push({
                            pathname: window.location.href + '/youtube',
                        })} className={styles.categoryWrapper} item width={256} height={174} alignItems="center" justifyContent="space-evenly" display={'flex'} flexDirection="column">
                            <YoutubeCategoryIcon />
                            <Typography variant="h6">Youtube video</Typography>
                        </Grid>
                    </Paper>
                    <Paper elevation={4} className={styles.sourceCategory}>
                        <Grid onClick={() => router.push({
                            pathname: window.location.href + '/music',
                        })} className={styles.categoryWrapper} item width={256} height={174} alignItems="center" justifyContent="space-evenly" display={'flex'} flexDirection="column">
                            <MusicCategoryIcon />
                            <Typography variant="h6">Музыка</Typography>
                        </Grid>
                    </Paper>
                </Grid>
            </Grid>
        </Grid>
    )

}