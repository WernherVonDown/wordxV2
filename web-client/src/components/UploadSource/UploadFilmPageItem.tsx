import { Box, Button, ButtonBase, Grid, Input, Paper, TextField, Typography } from "@mui/material";
import React, { KeyboardEvent, useCallback, useEffect, useMemo, useRef, useState } from "react";
import { useSource } from "../../context/SourceContext";
import { useInput } from "../../hooks/useInput";
import { ImdbSearch } from "./ImdbSearch";
import { KinopoiskSearch } from "../SearchFilm/KinopoiskSearch";
import { SearchFilm } from "../SearchFilm/SearchFilm";
import { IKinopoiskFilm, IKinopoiskFilmDev } from "../../const/source/interfaces";
import { ResultFilmsListItem } from "../SearchFilm/ResultFilmsListItem";
import Link from "next/link";
import { UploadFilmItem } from "./UploadFilmItem";
import { CenterXY } from "../../common/CenterXY/CenterXY";
import { FilmCategoryItem } from "../../icons/FilmCategoryItem";
import styles from "./UloadSource.module.scss";
import AddIcon from '@mui/icons-material/Add';
import CloseIcon from '@mui/icons-material/Close';
import { FileIcon } from "../../icons/FileIcon";
import { CoinsIcon } from "../../icons/CoinsIcon";
import classNames from "classnames";
import { BackButton } from "./UploadSource.component";
import { useRouter } from "next/router";
import { SourceApiRoutes } from "../../const/source/SourceApiRoutes";
import { ILangAuthProps, withLangAuth } from "../../hoc/withLangAuth";
import { getLanguageRouteByKey, getUploadFilmRouteByKey, getUploadSourceRouteByKey } from "../../const/APP_ROUTES";
import { NextParsedUrlQuery } from "next/dist/server/request-meta";
import { getFilmInfo } from "../../externalServices/kinopoiskService";
import { SourceTypes } from "../../const/source/SourceTypes";

interface IProps extends ILangAuthProps {
    langugeKey?:any;
}

interface IRouterQuery extends NextParsedUrlQuery {
    filmId: string,
}

const KpToKpDev = (film: IKinopoiskFilm, externalId: {imdbId?: string} = {}): IKinopoiskFilmDev => {
    return {
        alternativeName: film.nameEn,
        name: film.nameRu,
        poster: {
            url: film.posterUrl,
            previewUrl: film.posterUrlPreview,
        },
        rating: {
            imdb: 0,
            kp: film.ratingKinopoisk,
        },
        year: film.year,
        spokenLanguages: [],
        description: '',
        externalId: {
            imdb: film.imdbId || externalId?.imdbId,
        }
      }
}

export const UploadFilmPageItem: React.FC<IProps> = ({ languageKey }) => {
    const [file, setFile] = useState<File>();
    const [poster, setPoster] = useState()
    const sourceName = useInput('');
    const [fileName, setFileName] = useState('')
    const { actions: { uploadSource, getAutoSubtitiles }, state: { subtitlesPrice } } = useSource();
    const [film, setFilm] = useState<IKinopoiskFilmDev | undefined>()
    const [uploadSubSuccess, setUploadSubSuccess] = useState(false);
    const [uploadSuccess, setUploadSuccess] = useState(false);
    const uploadIntputRef = useRef<HTMLInputElement>(null);
    const router = useRouter();
    const { filmId } = router.query as IRouterQuery;

    useEffect(() => {
        if (filmId) {
            getFilmInfo(filmId).then(film => setFilm(film)).catch(async () => {
                const options = {
                    method: 'GET',
                    headers: {
                      'X-API-KEY': '47634ec9-aa04-418a-9214-94fcb33301f9',
                    }
                  };
            
                  const res = await fetch(`https://kinopoiskapiunofficial.tech/api/v2.1/films/${filmId}`, options)
                    .then(response => response.json())
                    if (res.data) {
                        const film = KpToKpDev(res.data, res.externalId);
                        console.log("RESULT EE", res, film)
                        setFilm(film)
                    }
                    
                  
            });
        }
    }, [filmId])

    const saveFile = useCallback((e: any) => {
        console.log("FILE", e.target.files[0])
        setFile(e.target.files[0]);
        setFileName(e.target.files[0].name);
        setUploadSubSuccess(true)
    }, []);

    const clearFileData = useCallback(() => {
        setFile(undefined);
        setFileName('');
        setUploadSubSuccess(false)
    }, [])

    const savePoster = useCallback((e: any) => {
        setPoster(e.target.files[0]);
    }, []);

    const onSend = useCallback(async () => {
        console.log("aAA", file, film, sourceName.value, fileName)
        if (file && film && film.externalId?.imdb) {
            try {
                const formData = new FormData();
                formData.append("file", file);
                formData.append("title", film.name);
                formData.append("imdbId", film.externalId?.imdb);
                // formData.append("kinopoiskId", film.externalId?.kinopoiskId);
                formData.append("titleOriginal", film.alternativeName || film.name);
                formData.append("posterUrl", film.poster?.url);
                if (poster)
                    formData.append("poster", poster)
                const res = await uploadSource(formData, SourceTypes.film);
                if (!res.success) {
                    // alert("Oшибка")
                } else {
                    setUploadSuccess(true)
                }
            } catch (error) {
                console.log("error", error)
            }

        }

    }, [sourceName.value, fileName, file, poster, film]);


    const onGetAutoSubtitiles = useCallback(async () => {
        try {
            console.log("film", film)
            if (film?.externalId.imdb) {
                const res = await getAutoSubtitiles(film?.externalId.imdb)
                if (res.success) {
                    const subRes = await fetch(res.subtitleLink);
                    const subData = await subRes.blob();
                    setFile(subData as any)
                    setUploadSubSuccess(true)
                }
            }
        } catch (error) {
            alert("ERROR FETCHING")
        }

    }, [film?.externalId?.imdb])

    const AddSubtitles = useMemo(() => {
        return (
            <Box justifyContent={'space-between'} width="100%" display={"flex"} gap="24px">
                <Box display={"flex"} style={{ background: 'var(--primary)' }} color="white" gap="34px" padding="20px" width="100%" flexDirection="column" justifyContent={'space-between'} borderRadius={"10px"}>
                    <Box>
                        <Box display={"flex"}>
                            <CoinsIcon />
                            <Typography>Купить за {subtitlesPrice} монет</Typography>
                        </Box>

                        <Box display={'flex'} flexDirection="column">
                            <Typography variant="caption">
                                У нас уже есть субтитры к этом фильму.
                            </Typography>
                            <Typography variant="caption">
                                Добавить их в два клика 😉
                            </Typography>
                        </Box>
                    </Box>
                    <Button
                        className={classNames(styles.uploadBtn, styles.auto)}
                        variant={'contained'}
                        sx={{ paddingX: '57px', paddingY: '12px' }}
                        onClick={onGetAutoSubtitiles}

                    >
                        <Typography>Купить</Typography>
                    </Button>
                </Box>
                <Box display={"flex"} style={{ background: '#F5F6F8' }} gap="34px" padding="20px" width="100%" justifyContent={'space-between'} flexDirection="column" borderRadius={"10px"}>
                    <Box>
                        <Typography>Загрузить самостоятельно</Typography>
                        <Box display={'flex'} gap="0.5rem">
                            <Typography variant="body1" color={'secondary'}>В формате SRT</Typography>
                            <a className="defaultLink" href={'https://www.opensubtitles.org/ru/search/subs'}>Где взять субтитры?</a>
                        </Box>
                    </Box>
                    <Button
                        className={styles.uploadBtn}
                        variant={'contained'}
                        sx={{ paddingX: '57px', paddingY: '12px', color: 'var(--primary)' }}
                        onClick={() => uploadIntputRef.current?.click()}
                        onKeyDown={(e: KeyboardEvent<any>) => e.keyCode === 32 && uploadIntputRef.current?.click()}
                    ><AddIcon /> <Typography>Загрузить </Typography>
                        <input accept="application/x-subrip" ref={uploadIntputRef} hidden type="file" onChange={saveFile} placeholder='файл' />
                    </Button>
                </Box>
            </Box>

        )
    }, [uploadIntputRef, saveFile, onGetAutoSubtitiles])

    const AddedSubtitlesPreview = useMemo(() => {
        if (!file) return null;
        return (
            <Box gap={3} className={styles.filePreview} color="var(--primary)">
                <Box gap={1} className={styles.filePreviewName}>
                    <FileIcon /><Typography color={'black'}>{file.name || 'subtitles'}</Typography>
                </Box>
                <Box onClick={clearFileData} className={styles.filePreviewClose}>
                    <CloseIcon sx={{ width: '17px', height: '17px' }} />
                </Box>
            </Box>
        )
    }, [file])

    const onBack = useCallback(() => {
        router.back()
        // router.push({
        //     pathname: getUploadFilmRouteByKey(languageKey)
        // })
        // setFilm(undefined)
    }, [languageKey])

    if (uploadSuccess) {
        return <CenterXY>
            <Box display={'flex'} flexDirection="column" gap="1rem" alignItems="center">
                <FilmCategoryItem />
                <Typography variant="h5">Ресурс добавлен!</Typography>
                <Button href={getLanguageRouteByKey(languageKey)} variant="contained">В список ресурсов</Button>
            </Box>
        </CenterXY>
    }

    if (!film) {
        return <div>Loading...</div>
    }

    return (
        <Grid container display={"flex"} flexDirection="column" marginTop={2} alignItems="center">
            {<Grid xs={6} display="flex" container flexDirection="column" gap={2} position='relative'>
            {/* <div style={{display: 'flex', position: 'absolute', left: '-3rem', width: '36px', height: '36px', background: '#FFFFFF', boxShadow: '0px 28px 31px rgba(109, 109, 112, 0.1)', borderRadius: '50%'}}>x</div> */}
            
                <UploadFilmItem onBack={onBack} {...film} />
                <Paper>
                    <Box padding={2} width={'100%'}>
                        <Box display={'flex'} paddingBottom={2} width={'100%'} justifyContent="space-between">
                            <Box >
                                <Typography variant="h6">Добавить субтитры</Typography>
                            </Box>
                        </Box>

                        <Grid container display={"flex"} alignItems="center">
                            {!file ? AddSubtitles
                                : AddedSubtitlesPreview}
                        </Grid>
                    </Box>
                </Paper>
                {uploadSubSuccess && <Grid display={'flex'} item justifyContent={'center'}>
                    <Button variant='contained' onClick={onSend}>Сохранить и опубликовать</Button>
                </Grid>}
            </Grid>

            }
        </Grid>
    )
}