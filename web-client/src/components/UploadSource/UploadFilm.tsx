import { Box, Button, ButtonBase, Grid, Input, Paper, TextField, Typography } from "@mui/material";
import React, { KeyboardEvent, useCallback, useMemo, useRef, useState } from "react";
import { useSource } from "../../context/SourceContext";
import { useInput } from "../../hooks/useInput";
import { ImdbSearch } from "./ImdbSearch";
import { KinopoiskSearch } from "../SearchFilm/KinopoiskSearch";
import { SearchFilm } from "../SearchFilm/SearchFilm";
import { IKinopoiskFilm } from "../../const/source/interfaces";
import { ResultFilmsListItem } from "../SearchFilm/ResultFilmsListItem";
import Link from "next/link";
import { UploadFilmItem } from "./UploadFilmItem";
import { CenterXY } from "../../common/CenterXY/CenterXY";
import { FilmCategoryItem } from "../../icons/FilmCategoryItem";
import styles from "./UloadSource.module.scss";
import AddIcon from '@mui/icons-material/Add';
import CloseIcon from '@mui/icons-material/Close';
import { FileIcon } from "../../icons/FileIcon";
import { CoinsIcon } from "../../icons/CoinsIcon";
import classNames from "classnames";
import { BackButton } from "./UploadSource.component";
import { useRouter } from "next/router";
import { SourceApiRoutes } from "../../const/source/SourceApiRoutes";
import { ILangAuthProps, withLangAuth } from "../../hoc/withLangAuth";
import { getLanguageRouteByKey, getUploadFilmItemRouteByKey, getUploadFilmRouteByKey, getUploadSourceRouteByKey } from "../../const/APP_ROUTES";

interface IProps extends ILangAuthProps {
    langugeKey?: any;
}

export const UploadFilm: React.FC<IProps> = ({ languageKey }) => {

    return <SearchFilm
    />
}