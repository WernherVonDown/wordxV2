import { Box, Card, CardContent, CardMedia, Grid, Typography } from "@mui/material";
import Image from "next/image";
import React from "react";
import { IKinopoiskFilm, IKinopoiskFilmDev } from "../../const/source/interfaces";
import { BackButton } from "./UploadSource.component";
import styles from "./UloadSource.module.scss";

interface IProps extends IKinopoiskFilmDev {
    onBack: () => void;
}

export const UploadFilmItem: React.FC<IProps> = ({
    alternativeName,
    name,
    poster,
    rating,
    year,
    spokenLanguages,
    description,
    onBack
}) => {
    return<Card sx={{ display: 'flex', width: '100%' }}>
        <BackButton className={styles.absolutePos} onClick={onBack}  />
    <Box display={'flex'} flexDirection={'row'} sx={{ width: '100%', height: '100%' }}>
        <Box display={'flex'} flexDirection={'column'} padding={2} >
            <CardMedia
                component="img"
                height="100%"
                sx={{ maxWidth: '208px' }}
                image={poster?.url}
                alt={`poster for ${name}`}
            />
        </Box>
        <Box display={'flex'} flexDirection={'column'} sx={{ width: '100%' }}>
            <CardContent sx={{ width: '100%', display: 'flex', flexDirection: 'column', position: 'relative', height: '100%' }}>
                <Typography gutterBottom component="div">
                    {name}
                </Typography>
                <Typography gutterBottom variant="body2" component="div" color={"text.secondary"}>
                    {alternativeName}
                </Typography>
                {year && <Typography gutterBottom variant="caption" component="div">
                    {year}
                </Typography>}
                {rating && rating.kp  && <Typography gutterBottom variant="caption" component="div">
                    {rating.kp}
                </Typography>}
            </CardContent>

        </Box>
        
    </Box>
</Card>
}
