import { Box, Button, Grid, Paper, TextField, Typography } from '@mui/material';
import Link from 'next/link';
import React, { useCallback, useContext, useEffect, useState } from 'react';
import { LoginRoute } from '../../const/APP_ROUTES';
import { AuthContext } from '../../context/AuthContext';
import { useInput } from '../../hooks/useInput';
import { useTimer } from '../../hooks/useTimer';
import GoogleAuthButton from '../GoogleAuthButton/GoogleAuthButton.component';
import styles from './RegistrationForm.module.scss';

export const RegistrationForm: React.FC = () => {
    const { actions: { register, activateCode } } = useContext(AuthContext);
    const [registerDone, setRegiserDone] = useState(false);
    const username = useInput('')
    const email = useInput('');
    const password1 = useInput('');
    const password2 = useInput('');
    const activationCode = useInput('');
    const activationCodeTimer = useTimer();

    const onRegister = useCallback(async () => {
        if (email.value.trim().length && password1.value.trim().length && password2.value.trim().length && username.value.trim().length) {
            if (password1.value === password2.value) {
                const res: any = await register(email.value, username.value, password1.value);
                if (res.success) {
                    setRegiserDone(true);
                    activationCodeTimer.startTimer(res.activationTimeout, () => {
                        // alert("Ну ты лох")
                        // document.location.reload();
                    })
                }

                // res.activationTimeout
            } else {
                alert('пароли не совпадают')
            }
        } else {
            alert('Заполните все поля');
        }
    }, [email.value, password1.value, password2.value, username.value]);

    useEffect(() => {
        if (activationCode.value.trim().length === 4 && email.value) {
            onActivate(activationCode.value, email.value);
        }
    }, [activationCode.value, email.value])

    const onActivate = useCallback(async (activationCode: string, email: string) => {
        const res: any = await activateCode(activationCode, email);
        if (!res.success) {
            alert('Неверный код')
        }
    }, [])

    if (registerDone) {
        return<Paper>
            <div className={styles.registerForm}>
                <Typography textAlign={"center"} variant="h5">Подтвердите вашу почту. Введите код</Typography>
                <Typography textAlign={"center"} variant="h5">{activationCodeTimer.timerValue}</Typography>
                <TextField variant="outlined" margin='normal' fullWidth size='small' {...activationCode} type={'text'} label='Код активации' />
            </div>
        </Paper>
    }

    return <>
        <Paper>
            <div className={styles.registerForm}>
                <Typography variant='h5'>Регистрация</Typography>
                <TextField variant="outlined" margin='normal' fullWidth size='small' {...email} type={'email'} label='Почта' />
                <TextField variant="outlined" margin='normal' fullWidth size='small' {...username} type={'text'} label='Имя пользователя' />
                <TextField variant="outlined" margin='normal' fullWidth size='small' {...password1} type={'password'} label='Пароль' />
                <TextField variant="outlined" margin='normal' fullWidth size='small' {...password2} type={'password'} label='Повторите пароль' />
                <Box display={"flex"} flexDirection="column" gap={"3px"} alignItems="center">
                    <Button fullWidth sx={{ marginTop: 1 }} variant='contained' onClick={onRegister}>
                    Регистрация
                    </Button>
                    <Typography>Или</Typography>
                    <GoogleAuthButton />
                </Box>
            </div>
        </Paper>
        <Grid container className={styles.bottomWrapper} direction={"column"} display={"flex"} justifyContent="center" marginTop={0.5} spacing={2} textAlign={"center"}>
            <Grid item>
                <Typography>Есть аккаунт? <Link href={LoginRoute}>
                    Войти
                </Link>
                </Typography>
            </Grid>
        </Grid>
    </>;
}