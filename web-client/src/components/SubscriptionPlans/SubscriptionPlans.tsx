import { Button, Grid, Paper, Typography } from "@mui/material";
import classNames from "classnames";
import { useRouter } from "next/router";
import React, { useCallback, useEffect, useState } from "react"
import { LanguagesRoute } from "../../const/APP_ROUTES";
import { PaymentTypes } from "../../const/payment/PaymentTypes";
import { ISubscriptionPlan } from "../../const/user/types";
import { useAuth } from "../../context/AuthContext";
import DataService from "../../services/dataService";
import UserService from "../../services/userService";
import styles from './SubscriptionPlans.module.scss';

export const SubscriptionPlans: React.FC = () => {
    const [subscriptionPlans, setSubscriptionPlans] = useState<ISubscriptionPlan[]>([])
    const { state: { loggedIn, isLoading, user }, actions: { checkAuth } } = useAuth();
    const router = useRouter()

    useEffect(() => {
        getSubscriptionPlans()
    }, [])

    const getSubscriptionPlans = useCallback(async () => {
        try {
            const res = await DataService.getSubscriptionPlans();
            setSubscriptionPlans(res.data)
        } catch (error) {
            console.log('getSubscriptionPlans error', error)
        }
    }, []);

    const changeSubscriptionPlan = useCallback(async (planId: string) => {
        try {
            const res = await UserService.changeSubscriptionPlan(planId, PaymentTypes.subscription);
            //ts-ignore
            if (typeof res.data.redirect === 'string') {
                window.open(res.data.redirect, '_blank')?.focus()
                return;
            }
            await checkAuth();
            router.push(LanguagesRoute);
        } catch (error) {
            alert('Ошибка');
        }
    }, [])

    const renderSubscriptionPlan = useCallback(({ title, currency, benifits, id, price, discount }: ISubscriptionPlan) => {
        const userSubscriptonId = user?.subscriptionPlan?.id;
        const subscriptionSelected = userSubscriptonId === id;
        const changeSub = () => user ? changeSubscriptionPlan(id) : alert("Войди или зарегайся");

        return (
            <Paper key={`subscription_plan_${id}`} className={classNames({[styles.selected]: subscriptionSelected})}>
                <Grid container flexDirection={'column'} padding={1} justifyContent="space-between" height={'100%'} gap={2}>
                    <Grid item>
                        <Typography variant="h6">
                            {title}
                        </Typography>
                        {benifits.map((b, i) => <Grid key={`subscription_plan_benifit_${id}_${i}`}><Typography variant="caption">- {b}</Typography></Grid>)}
                    </Grid>
                    <Grid item>
                        {/*@ts-ignore*/}
                        <Typography variant="body1">Цена: <strike>{price}</strike>(-{discount * 100}%){price - price * discount}{currency}/месяц</Typography>
                        {subscriptionSelected ? <Typography color="primary">Выбран</Typography> : <Button variant="contained" onClick={changeSub} fullWidth>Выбрать</Button>}
                    </Grid>
                </Grid>
            </Paper>
        )
    }, [user])

    return <Grid container display="flex" gap={1} padding={1}>
        {subscriptionPlans.map(renderSubscriptionPlan)}
    </Grid>
}