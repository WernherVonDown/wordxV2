import { useRouter } from "next/router"
import { useCallback } from "react"

export const usePushShallowUrl = () => {
    const router = useRouter();

    return useCallback((url: string) => router.push(url, undefined, { shallow: true }), []);
}
