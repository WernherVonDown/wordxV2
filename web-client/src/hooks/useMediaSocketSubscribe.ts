import { useContext, useEffect } from 'react';
import { MediaSocketContext } from '../context/MediaSocketContext';

export const useMediaSocketSubscribe = (event: string, handler: (...args: any[]) => void) => {
    const {
        state: socketState,
        actions: socketActions,
    } = useContext(MediaSocketContext);

    useEffect(() => {
        if (socketState?.isConnected) {
            socketActions?.subscribe(event, handler);
        }

        return () => {
            socketActions?.unsubscribe(event, handler);
        };
    }, [socketState?.isConnected, event, handler]);
};
