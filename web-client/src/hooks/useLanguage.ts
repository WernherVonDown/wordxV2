import { NextParsedUrlQuery } from "next/dist/server/request-meta";
import { useRouter } from "next/router";
import { LanguageKeys, languageRouteToKey, LanguageShortKeys } from "../const/language/Languagekeys";

interface IRouterQuery extends NextParsedUrlQuery {
    languageShortKey: LanguageShortKeys;
}

interface IHook {
    languageKey: LanguageKeys;
    languageShortKey: LanguageShortKeys,
}

export const useLanguage = (): IHook => {
    const router = useRouter();
    const { languageShortKey } = router.query as IRouterQuery;
    return { languageKey: languageRouteToKey(languageShortKey), languageShortKey};
}