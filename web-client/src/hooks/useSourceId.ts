import { NextParsedUrlQuery } from "next/dist/server/request-meta";
import { useRouter } from "next/router";

interface IRouterQuery extends NextParsedUrlQuery {
    sourceId: string;
}

interface IHook {
    sourceId: string;
}

export const useSourceId = (): IHook => {
    const router = useRouter();
    const { sourceId } = router.query as IRouterQuery;
    return { sourceId };
}