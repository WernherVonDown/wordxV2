import { useCallback, useEffect, useRef, useState } from "react"

interface IHook {
    active: boolean,
    timerValue?: number,
    startTimer: (timerLimit: number, endTimerCallback?: () => void) => void,
    stopTimer: () => void,
}

const TIMER_STEP = 1000;

export const useTimer = (): IHook => {
    const [timerValue, setTimerValue] = useState<number | undefined>(undefined);
    const [active, setActive] = useState(false);
    const timerRef = useRef(0);

    useEffect(() => {
        return () => {
            stopTimer()
        }
    }, [])

    const startTimer = useCallback((timerLimit: number, endTimerCallback?: () => void) => {
        setActive(true);
        let seconds = 0;
        setTimerValue(timerLimit / 1000)
        timerRef.current = window.setInterval(() => {
            seconds += TIMER_STEP;
            const newValue = timerLimit - seconds;
            if (newValue > 0) {
                setTimerValue(newValue / 1000);
            } else {
                stopTimer()
                if (endTimerCallback) {
                    endTimerCallback()
                }
            }
        }, TIMER_STEP)
    }, []);

    const stopTimer = useCallback(() => {
        setActive(false);
        clearInterval(timerRef.current)
        setTimerValue(undefined)
    }, [])
    return {
        active,
        timerValue,
        startTimer,
        stopTimer,
    }
}