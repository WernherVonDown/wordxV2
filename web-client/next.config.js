/** @type {import('next').NextConfig} */

require('dotenv').config();

const publicRuntimeConfig = {
  apiUrl: process.env.NEXT_PUBLIC_API_URL || "http://localhost:8000/api",
  googleAuthClientId: process.env.REACT_APP_GOOGLE_AUTH_CLIENT_ID || '',
  socketUrl: process.env.REACT_APP_SOCKET_URL || '',
  tanalyticsToken: process.env.TANALYTICS_TOKEN || '18efaa9c-c71b-4b48-9677-d3c009e472b8',
}


const nextConfig = {
  publicRuntimeConfig,
  reactStrictMode: false,
}

module.exports = nextConfig
